//globals.js
module.exports = {
	variables : {

	//id globals
	skipLoginButtin_id: 'SkipEventRectangle',
	usrnmInputSelect_id: 'userLoginIcon',
	pswrdInputSelect_id: 'passwordIcon',
	usrnm_id: 'username',
	pswrd_id: 'password',

	//CSS globals
	startLogin_css: '#loginButton+text',

	//Send Key globals
	usrnm_keys: 'augustine',
	pswrd_keys: ':Am24iMc#!',
	usrnm_neg: 'garbage',
	pswrd_neg: 'garbage',

	bowers_usrnm_keys: 'bowers',
	bowers_pswrd_keys: 'Bf6Ppg#xFd',
	mccarthy_usrnm_keys: 'mccarthy',
	mccarthy_pswrd_keys: ':C5W_LskPN',

	//Sleep Delay globals
	deviceLoadDelay: 10000
 	}
};