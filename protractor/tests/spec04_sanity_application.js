//test05_sanity_application.js

describe('Application Tab Sanity Suite', function() {
	//login with admin username and password, click Application Tab
	it('Application', function() {
		console.log('\n\nApplication Sanity: Check if Graph is Presented.');
		loginUser(usrnm_keys, pswrd_keys);
		expect(browser.getCurrentUrl()).toMatch('systemsummary');
		//make sure numbers load
		browser.sleep(6000);
		element(by.css('#gnTopMenu > rect:nth-child(26)')).click();
		browser.sleep(6000);
		scrollToView(element(by.id('AppSunburstDiagram')));
		expect(element(by.id('AppSunburstDiagram')).isPresent()).toBe(true);
	});
});