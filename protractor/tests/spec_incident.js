//test_menu_incident.js
describe('Incident Menu Suite', function() {

	//Incident Page
	it('Check Incident Card values are present', function() {
		console.log('\n\nIncident Menu: Check if all incident summary sections have a number; greater than or equal to 0.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(14)')).click();
		browser.sleep(2000);
		element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
		browser.sleep(2000);
		//element.all(by.css('#sideMenu~g>text~rect')).get(2).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('Incident Summary', sideMenuClicks, sideMenuTexts);
		browser.sleep(2000);

		let numberTexts = element.all(by.css('text[font-size="60px"]'))
		//make sure numbers load
		browser.sleep(6000);
		var sessionsExportingData = numberTexts.get(0).getText().then(function(num) {
			console.log("Session Exporting found: "+parseInt(num, 10));
			return parseInt(num, 10);
		});
		expect(sessionsExportingData).toBeGreaterThan(-1);

		var openExternalChannels = numberTexts.get(1).getText().then(function(num) {
			console.log("Open External Channels found: "+parseInt(num, 10));
			return parseInt(num, 10);
		});
		expect(openExternalChannels).toBeGreaterThan(-1);

		var externalCommunications = numberTexts.get(2).getText().then(function(num) {
			console.log("External Communications found: "+parseInt(num, 10));
			return parseInt(num, 10);
		});
		expect(externalCommunications).toBeGreaterThan(-1);

		var internalCommunications = numberTexts.get(3).getText().then(function(num) {
			console.log("Internal Communications found: "+parseInt(num, 10));
			return parseInt(num, 10);
		});
		expect(internalCommunications).toBeGreaterThan(-1);

		var infections = numberTexts.get(4).getText().then(function(num) {
			console.log("Infections found: "+parseInt(num, 10));
			return parseInt(num, 10);
		});
		expect(infections).toBeGreaterThan(-1);
	});

	it('Clicking on Incident Cards lead to Threat List on same page', function() {
		console.log('\nIncident Menu: Clicking on Main Number of Incident Cards.')
		let incidentCardNum = element.all(by.css('[id*="Incident Summary"] text:nth-child(8)'));
		let incidentCardClick = element.all(by.css('[id*="Incident Summary"] > g > rect:nth-child(4)'));
		let incidentCardTitle = element.all(by.css('[id*="Incident Summary"] > g > text:nth-child(6)'));

		//Sessions Exporting Data
		incidentCardNum.get(0).getText().then(function(num) {
			if (parseInt(num, 10) > 0) {
				browser.sleep(2000);
				incidentCardTitle.get(0).getText().then(function(title) {
					scrollToView(incidentCardNum.get(0));
					browser.sleep(2000);
					browser.actions().mouseMove(incidentCardNum.get(0)).click().perform();
					browser.sleep(4000);
					console.log("Expect for "+title+" Section to have "+num+" alarms in Threat List");
					expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"]')).isPresent()).toBe(true);
					expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"] > g:nth-child(4) > g:nth-child(5) > text')).getText()).toContain(num);
				});
			}
			else {
				incidentCardTitle.get(0).getText().then(function(title) {
					console.log("There are 0 alarms for the "+title+" Section")
				});
			}
		});

		//Open External Channels
		incidentCardNum.get(1).getText().then(function(num) {
			if (parseInt(num, 10) > 0) {
				browser.sleep(2000);
				incidentCardTitle.get(1).getText().then(function(title) {
					scrollToView(incidentCardNum.get(1));
					browser.sleep(2000);
					browser.actions().mouseMove(incidentCardNum.get(1)).click().perform();
					browser.sleep(4000);
					console.log("Expect for "+title+" Section to have "+num+" alarms in Threat List");
					expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"]')).isPresent()).toBe(true);
					expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"] > g:nth-child(4) > g:nth-child(5) > text')).getText()).toContain(num);
				});
			}
			else {
				incidentCardTitle.get(1).getText().then(function(title) {
					console.log("There are 0 alarms for the "+title+" Section")
				});
			}
		});

		//External Communications
		incidentCardNum.get(2).getText().then(function(num) {
			if (parseInt(num, 10) > 0) {
				browser.sleep(2000);
				incidentCardTitle.get(2).getText().then(function(title) {
					scrollToView(incidentCardNum.get(2));
					browser.sleep(2000);
					browser.actions().mouseMove(incidentCardNum.get(2)).click().perform();
					browser.sleep(4000);
					console.log("Expect for "+title+" Section to have "+num+" alarms in Threat List");
					expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"]')).isPresent()).toBe(true);
					expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"] > g:nth-child(4) > g:nth-child(5) > text')).getText()).toContain(num);
				});
			}
			else {
				incidentCardTitle.get(2).getText().then(function(title) {
					console.log("There are 0 alarms for the "+title+" Section")
				});
			}
		});

		//Internal Communications
		incidentCardNum.get(3).getText().then(function(num) {
			if (parseInt(num, 10) > 0) {
				browser.sleep(2000);
				incidentCardTitle.get(3).getText().then(function(title) {
					scrollToView(incidentCardNum.get(3));
					browser.sleep(2000);
					browser.actions().mouseMove(incidentCardNum.get(3)).click().perform();
					browser.sleep(4000);
					console.log("Expect for "+title+" Section to have "+num+" alarms in Threat List");
					expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"]')).isPresent()).toBe(true);
					expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"] > g:nth-child(4) > g:nth-child(5) > text')).getText()).toContain(num);
				});
			}
			else {
				incidentCardTitle.get(3).getText().then(function(title) {
					console.log("There are 0 alarms for the "+title+" Section")
				});
			}
		});

		//Infections & Variabilities
		incidentCardNum.get(4).getText().then(function(num) {
			if (parseInt(num, 10) > 0) {
				browser.sleep(2000);
				incidentCardTitle.get(4).getText().then(function(title) {
					scrollToView(incidentCardNum.get(4));
					browser.sleep(2000);
					browser.actions().mouseMove(incidentCardNum.get(4)).click().perform();
					browser.sleep(4000);
					console.log("Expect for "+title+" Section to have "+num+" alarms in Threat List");
					expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"]')).isPresent()).toBe(true);
					expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"] > g:nth-child(4) > g:nth-child(5) > text')).getText()).toContain(num);
					presentElementClick(element(by.css('[id*="ThreatListPanel"][id$="0:0"] g:nth-child(6) > rect:nth-child(7)')), 'Close Panel button not there', function() {});
					browser.sleep(2000);
				});
			}
			else {
				incidentCardTitle.get(4).getText().then(function(title) {
					console.log("There are 0 alarms for the "+title+" Section")
				});
			}
		});
	});

	it('Check if Individual Card values of Sessions Exporting Data are present', function() {
		console.log('\nIncident Menu: Clicking all individual portions of the \"Sessions Exporting Data\" Card, if available.');
		browser.sleep(2000);
		
		var sessionsExportingNum = element.all(by.css('[id^="ReportCardPanel(Incident Summary)"] > g:nth-child(5) > g > text:nth-child(2)'));
		var sessionsExportingTitle = element.all(by.css('[id^="ReportCardPanel(Incident Summary)"] > g:nth-child(5) > g > text:nth-child(3)'));

		sessionsExportingTitle.each(function(elem, index) {
			sessionsExportingNum.get(index).getText().then(function(num) {
				sessionsExportingTitle.get(index).getText().then(function(title) {
					presentElementText(sessionsExportingTitle.get(index), '', function() {
						presentElementText(sessionsExportingNum.get(index), ('No Number present for '+title+' Section'), function() {
							if(parseInt(num, 10) > 0) {
								console.log('Expect '+num+' alarms for '+title+' Section');
								scrollToView(sessionsExportingNum.get(index));
								browser.sleep(2000);
								browser.actions().mouseMove(sessionsExportingNum.get(index)).click().perform();
								browser.sleep(4000);
								expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"]')).isPresent()).toBe(true);
								expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"] > g:nth-child(4) > g:nth-child(5) > text')).getText()).toContain(num);
							}
							else {
								console.log('Number is less than or equal to 0 for '+title+' Section');
							}
						});
					});
				});
			});
		});
	});

	it('Check if Individual Card values of Open External Channels are present', function() {
		console.log('\nIncident Menu: Clicking all individual portions of the \"Open External Channels\" Card, if available.');
		browser.sleep(2000);
		
		var openExternalNum = element.all(by.css('[id^="ReportCardPanel(Incident Summary)"] > g:nth-child(6) > g > text:nth-child(2)'));
		var openExternalTitle = element.all(by.css('[id^="ReportCardPanel(Incident Summary)"] > g:nth-child(6) > g > text:nth-child(3)'));

		openExternalTitle.each(function(elem, index) {
			openExternalNum.get(index).getText().then(function(num) {
				openExternalTitle.get(index).getText().then(function(title) {
					presentElementText(openExternalTitle.get(index), '', function() {
						presentElementText(openExternalNum.get(index), ('No Number present for '+title+' Section'), function() {
							if(parseInt(num, 10) > 0) {
								console.log('Expect '+num+' alarms for '+title+' Section');
								scrollToView(openExternalNum.get(index));
								browser.sleep(2000);
								browser.actions().mouseMove(openExternalNum.get(index)).click().perform();
								browser.sleep(4000);
								expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"]')).isPresent()).toBe(true);
								expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"] > g:nth-child(4) > g:nth-child(5) > text')).getText()).toContain(num);
							}
							else {
								console.log('Number is less than or equal to 0 for '+title+' Section');
							}
						});
					});
				});
			});
		});
	});

	it('Check if Individual Card values of External Communications are present', function() {
		console.log('\nIncident Menu: Clicking all individual portions of the \"External Communications\" Card, if available.');
		browser.sleep(2000);

		var externalCommunicationsNum = element.all(by.css('[id^="ReportCardPanel(Incident Summary)"] > g:nth-child(7) > g > text:nth-child(2)'));
		var externalCommunicationsTitle = element.all(by.css('[id^="ReportCardPanel(Incident Summary)"] > g:nth-child(7) > g > text:nth-child(3)'));

		externalCommunicationsTitle.each(function(elem, index) {
			externalCommunicationsNum.get(index).getText().then(function(num) {
				externalCommunicationsTitle.get(index).getText().then(function(title) {
					presentElementText(externalCommunicationsTitle.get(index), '', function() {
						presentElementText(externalCommunicationsNum.get(index), ('No Number present for '+title+' Section'), function() {
							if(parseInt(num, 10) > 0) {
								console.log('Expect '+num+' alarms for '+title+' Section');
								scrollToView(externalCommunicationsNum.get(index));
								browser.sleep(2000);
								browser.actions().mouseMove(externalCommunicationsNum.get(index)).click().perform();
								browser.sleep(4000);
								expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"]')).isPresent()).toBe(true);
								expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"] > g:nth-child(4) > g:nth-child(5) > text')).getText()).toContain(num);
							}
							else {
								console.log('Number is less than or equal to 0 for '+title+' Section');
							}
						});
					});
				});
			});
		});
	});

	it('Check if Individual Card values of Internal Communications are present', function() {
		console.log('\nIncident Menu: Clicking all individual portions of the \"Internal Communications\" Card, if available.');
		browser.sleep(2000);

		var internalCommunicationsNum = element.all(by.css('[id^="ReportCardPanel(Incident Summary)"] > g:nth-child(8) > g > text:nth-child(2)'));
		var internalCommunicationsTitle = element.all(by.css('[id^="ReportCardPanel(Incident Summary)"] > g:nth-child(8) > g > text:nth-child(3)'));

		internalCommunicationsTitle.each(function(elem, index) {
			internalCommunicationsNum.get(index).getText().then(function(num) {
				internalCommunicationsTitle.get(index).getText().then(function(title) {
					presentElementText(internalCommunicationsTitle.get(index), '', function() {
						presentElementText(internalCommunicationsNum.get(index), ('No Number present for '+title+' Section'), function() {
							if(parseInt(num, 10) > 0) {
								console.log('Expect '+num+' alarms for '+title+' Section');
								scrollToView(internalCommunicationsNum.get(index));
								browser.sleep(2000);
								browser.actions().mouseMove(internalCommunicationsNum.get(index)).click().perform();
								browser.sleep(4000);
								expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"]')).isPresent()).toBe(true);
								expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"] > g:nth-child(4) > g:nth-child(5) > text')).getText()).toContain(num);
							}
							else {
								console.log('Number is less than or equal to 0 for '+title+' Section');
							}
						});
					});
				});
			});
		});
	});

	it('Check if Individual Card values of Infections & Vulnerabilities are present', function() {
		console.log('\nIncident Menu: Clicking all individual portions of the \"Infections & Vulnerabilities\" Card, if available.');
		browser.sleep(2000);

		var infectionsVulnerabilitiesNum = element.all(by.css('[id^="ReportCardPanel(Incident Summary)"] > g:nth-child(9) > g > text:nth-child(2)'));
		var infectionsVulnerabilitiesTitle = element.all(by.css('[id^="ReportCardPanel(Incident Summary)"] > g:nth-child(9) > g > text:nth-child(3)'));

		infectionsVulnerabilitiesTitle.each(function(elem, index) {
			infectionsVulnerabilitiesNum.get(index).getText().then(function(num) {
				infectionsVulnerabilitiesTitle.get(index).getText().then(function(title) {
					presentElementText(infectionsVulnerabilitiesTitle.get(index), '', function() {
						presentElementText(infectionsVulnerabilitiesNum.get(index), ('No Number present for '+title+' Section'), function() {
							if(parseInt(num, 10) > 0) {
								console.log('Expect '+num+' alarms for '+title+' Section');
								scrollToView(infectionsVulnerabilitiesNum.get(index));
								browser.sleep(2000);
								browser.actions().mouseMove(infectionsVulnerabilitiesNum.get(index)).click().perform();
								browser.sleep(4000);
								expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"]')).isPresent()).toBe(true);
								expect(element(by.css('[id*="ThreatListPanel"][id$="0:0"] > g:nth-child(4) > g:nth-child(5) > text')).getText()).toContain(num);
							}
							else {
								console.log('Number is less than or equal to 0 for '+title+' Section');
							}
						});
					});
				});
			});
		});
	});

	it('Check if Device Risk Cards under Incident Summary leads to Device Page', function() {
		console.log('\nIncident Menu: Clicking Device Risk Summary Cards under Incident Cards.');
		browser.sleep(2000);
		var deviceRiskNum = element.all(by.css('[id^="ReportCardPanel(Device Risk Summary)"] > g > text:nth-child(8)'));
		var deviceRiskType = element.all(by.css('[id^="ReportCardPanel(Device Risk Summary)"] > g > text:nth-child(6)'))
		var deviceRiskClick = element.all(by.css('[id^="ReportCardPanel(Device Risk Summary)"] > g > rect:nth-child(4)'));

		deviceRiskClick.each(function(elem, index) {
			deviceRiskType.get(index).getText().then(function(type) {
				deviceRiskNum.get(index).getText().then(function(num) {
					if(parseInt(num, 10) > 0) {
						scrollToView(deviceRiskClick.get(index));
						browser.sleep(2000);
						deviceRiskClick.get(index).click();
						browser.sleep(4000);
						expect(browser.getCurrentUrl()).toMatch('devicepage');
						scrollToView(element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')));
						browser.sleep(2000);
						element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
							console.log(type+' Card: expect to contain \"'+num+'\" Devices');
							expect(devices).toContain((num+' out of'));
							element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
							browser.sleep(3000);
						});
					}
					else{
						console.log('There are 0 devices for the '+type+' Section')
					}
				});
			});
		});
	});
});