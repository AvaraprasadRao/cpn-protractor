//test04_systemPageIntegration.js

describe('System Tab Integration Suite', function() {

	function dropdownReset(dropdown, optionBoxes, optionTexts) {
		dropdown.all(by.css('text')).get(0).getText().then(function(selection) {
			if(selection !== 'None') {
				dropdown.click();
				browser.sleep(3000);
				optionBoxes.filter(function(elem, index) {
					return optionTexts.get(index).getText().then(function(text) {
						return text === 'None';
					});
				}).first().click();
				browser.sleep(3000);
				expect(dropdown.all(by.css('text')).get(0).getText()).toBe('None');
			}
		});
	}

	function clickService(service) {
		let integrationTabs = element.all(by.css('[id^="ServicesBrowserPanel"] > g > g > rect:nth-child(7)'));
		integrationTabs.get(0).click();
		browser.sleep(3000);
		expect(element(by.id('ExtServiceBrowserPanel(External Services)0:0')).isPresent()).toBe(true);

		let externalServices = element.all(by.css('#topCards svg text'));
		let externalServicesClick = element.all(by.css('#topCards svg > rect:last-of-type'));
		presentElementClick(externalServicesClick.filter(function(elem, index) {
			return externalServices.get(index).getText().then(function(text) {
				return stringContains(text, service);
			});
		}).first(), '', function() {}, function() {
			integrationTabs.get(1).click();
			browser.sleep(3000);
			expect(element(by.id('IntServiceBrowserPanel(Internal Services)0:0')).isPresent()).toBe(true);

			let internalServices = element.all(by.css('#topCards svg text'));
			let internalServicesClick = element.all(by.css('#topCards svg > rect:last-of-type'));
			presentElementClick(internalServicesClick.filter(function(elem, index) {
				return internalServices.get(index).getText().then(function(text) {
					return stringContains(text, service);
				});
			}).first(), '', function() {});
		});
	}
	
	//Active Directory
	it('Setting up Active Directory', function() {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		expect(browser.getCurrentUrl()).toMatch('systempage');
		expect(element(by.id('NetworkConfigPanel(Equipment Configuration)0:0')).isPresent()).toBe(true);
		element.all(by.css('#sideMenu~g>text~rect')).get(4).click();
		browser.sleep(4000).then(function() {
			console.log('\n\nSystem Integration: Configuring Active Directory Service');
		});
		expect(element(by.id('ServicesBrowserPanel(Service Integration)0:0')).isPresent()).toBe(true);

		let dropdown = element(by.css('*[id*=pollFrequency]'));
		let optionBoxes = element.all(by.css('#infoLayerGElement > g > rect'));
		let optionTexts = element.all(by.css('#infoLayerGElement > g > text'));
		clickService('Active Directory');
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(2).click();
		
		browser.sleep(2000);
		
		element(by.id('DeleteEventRectangle')).isPresent().then(function(bool) {
			if (bool == true) {
				element(by.id('DeleteEventRectangle')).click();
				browser.sleep(2000);
			}
		});

		element(by.id('ad-servername')).clear();
		element(by.id('ad-servername')).sendKeys('ad-server-2');
		element(by.id('ad-domainname')).clear();
		element(by.id('ad-domainname')).sendKeys('ad2.cpn.lan');
		element(by.id('ad-serverip')).clear();
		element(by.id('ad-serverip')).sendKeys('192.168.103.11');
		element(by.id('ad-username')).clear();
		element(by.id('ad-username')).sendKeys('cpnadmin');
		element(by.id('ad-password')).clear();
		element(by.id('ad-password')).sendKeys('ironfist2!');
		//element(by.id('ad-servername')).sendKeys('ad-server-1');
		//element(by.id('ad-domainname')).sendKeys('ad1.cpn.lan');
		//element(by.id('ad-serverip')).sendKeys('192.168.103.89');
		//element(by.id('ad-username')).sendKeys('cpnadmin');
		//element(by.id('ad-password')).sendKeys('ironfist2!');

		dropdown.click();
		browser.sleep(2000);		
		optionBoxes.filter(function(elem, index) {
			return optionTexts.get(index).getText().then(function(text) {
				return text === '1 hour';
			});
		}).first().click();
		browser.sleep(1000);
		expect(dropdown.all(by.css('text')).get(0).getText()).toEqual('1 hour');

		browser.sleep(2000);
		element(by.id('SaveEventRectangle')).click();
		browser.sleep(2000);
		element(by.id('SaveEventRectangle')).click();
		browser.sleep(2000);
		ignoreSynchronization(function() {
			expect(element(by.css('text[fill="#4bb3ad"]')).isPresent()).toBe(true);
			expect(element(by.css('text[fill="#4bb3ad"]')).getText()).toMatch("Successfully saved changes.");
		});

		browser.sleep(2000);
	});

	//Email
	it('Setting up Internal Email Service', function() {
		console.log('\nSystem Integration: Configuring Email Service');
		let dropdown = element(by.css('*[id*=securitymode]'));
		let optionBoxes = element.all(by.css('#infoLayerGElement g rect'));
		let optionTexts = element.all(by.css('#infoLayerGElement g text'));
		clickService('Email|SMTP');

		//element.all(by.css('#topCards image ~ rect ~ rect')).get(0).click();
		browser.sleep(2000);

		element(by.id('DeleteEventRectangle')).isPresent().then(function(bool) {
			if (bool == true) {
				element(by.id('DeleteEventRectangle')).click();
				browser.sleep(2000);
			}
		});

		element(by.id('smtp-server')).clear();
		element(by.id('smtp-server')).sendKeys('smtp.office365.com');
		element(by.id('smtp-port')).clear();
		element(by.id('smtp-port')).sendKeys('587');
		element(by.id('smtp-username')).clear();
		element(by.id('smtp-username')).sendKeys('support@cloudpostnetworks.com');
		element(by.id('smtp-password')).clear();
		element(by.id('smtp-password')).sendKeys('sP0bS3lN!');
		
		dropdown.click();
		browser.sleep(3000);
		optionBoxes.filter(function(elem, index) {
			return optionTexts.get(index).getText().then(function(text) {
				return text === 'STARTTLS';
			});
		}).first().click();
		browser.sleep(1000);
		expect(dropdown.all(by.css('text')).get(0).getText()).toEqual('STARTTLS');

		browser.sleep(1000);
		element(by.id('SaveEventRectangle')).click();
		ignoreSynchronization(function() {
			expect(element(by.css('text[fill="#4bb3ad"]')).isPresent()).toBe(true);
			//expect(element(by.css('text[fill="#4bb3ad"]')).getText()).toMatch("Successfully saved changes.");
		});
		browser.sleep(2000);
	});

	//Alarms Notification
	it('Setting up Internal Alarms Notification Service', function() {
		console.log('\nSystem Integration: Configuring Alarms Notification Service');
		let emailElements = element.all(by.css('*[id^=email]'));
		let dropdown = element(by.css('*[id*=frequencyInput-alarms-notification]'));
		let timeInputs = element.all(by.css('div input'));
		let optionBoxes = element.all(by.css('#infoLayerGElement g rect'));
		let optionTexts = element.all(by.css('#infoLayerGElement g text'));
		
		clickService('Alarms Notification');
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(1).click();		
		browser.sleep(2000);

		element(by.id('DeleteEventRectangle')).isPresent().then(function(bool) {
			if (bool == true) {
				element(by.id('DeleteEventRectangle')).click();
				browser.sleep(2000);
			}
		});

		var url = require('url');
		var host = url.parse(browser.params.url, true).host;
		emailElements.get(0).clear();
		emailElements.get(0).sendKeys(browser.params.email);
		emailElements.get(1).clear()
		emailElements.get(1).sendKeys(host+'_Alarm_Alert');

		dropdownReset(dropdown, optionBoxes, optionTexts);

		dropdown.click();
		browser.sleep(3000);
		optionBoxes.filter(function(elem, index) {
			return optionTexts.get(index).getText().then(function(text) {
				return text === 'Daily';
			});
		}).first().click();
		browser.sleep(3000);
		expect(dropdown.all(by.css('text')).get(0).getText()).toEqual('Daily');

		timeInputs.get(2).clear();
		timeInputs.get(2).sendKeys('11');
		timeInputs.get(3).clear();
		timeInputs.get(3).sendKeys('0');
		browser.sleep(2000);
		element(by.id('SaveEventRectangle')).click();
		browser.sleep(2000);

		element(by.id('SaveEventRectangle')).click();
		ignoreSynchronization(function() {
			expect(element(by.css('text[fill="#4bb3ad"]')).isPresent()).toBe(true);
			//expect(element(by.css('text[fill="#4bb3ad"]')).getText()).toMatch("Successfully saved changes.");
		});
		browser.sleep(3000);
	});

	//Internal Services: Devices Notification
	it('Setting up Internal Devices Notification Service', function() {
		console.log('\nSystem Integration: Configuring Devices Notification Service');
		let emailElements = element.all(by.css('*[id^=email]'));
		let dropdown = element(by.css('*[id*=frequencyInput-devicegroups-notification]'));
		let timeInputs = element.all(by.css('div input'));
		let optionBoxes = element.all(by.css('#infoLayerGElement g rect'));
		let optionTexts = element.all(by.css('#infoLayerGElement g text'));
		clickService('Devices Notification');
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(2).click();
		browser.sleep(2000);

		element(by.id('DeleteEventRectangle')).isPresent().then(function(bool) {
			if (bool == true) {
				element(by.id('DeleteEventRectangle')).click();
				browser.sleep(2000);
			}
		});

		var url = require('url');
		var host = url.parse(browser.params.url, true).host;
		emailElements.get(0).clear();
		emailElements.get(0).sendKeys(browser.params.email);
		emailElements.get(1).clear()
		emailElements.get(1).sendKeys(host+'_Devices_Alert');

		dropdownReset(dropdown, optionBoxes, optionTexts);

		dropdown.click();
		browser.sleep(3000);
		optionBoxes.filter(function(elem, index) {
			return optionTexts.get(index).getText().then(function(text) {
				return text === 'Daily';
			});
		}).first().click();
		browser.sleep(3000);
		expect(dropdown.all(by.css('text')).get(0).getText()).toEqual('Daily');

		timeInputs.get(2).clear();
		timeInputs.get(2).sendKeys('11');
		timeInputs.get(3).clear();
		timeInputs.get(3).sendKeys('0');
		browser.sleep(2000);
		element(by.id('SaveEventRectangle')).click();
		browser.sleep(2000);

		element(by.id('SaveEventRectangle')).click();
		ignoreSynchronization(function() {
			expect(element(by.css('text[fill="#4bb3ad"]')).isPresent()).toBe(true);
			//expect(element(by.css('text[fill="#4bb3ad"]')).getText()).toMatch("Successfully saved changes.");
		});
		browser.sleep(3000);
	});
});