//test_fullConfig.js

describe('Full Config Suite', function() {

	function dropdownReset(dropdown, optionBoxes, optionTexts) {
		dropdown.all(by.css('text')).get(0).getText().then(function(selection) {
			if(selection !== 'None') {
				dropdown.click();
				browser.sleep(3000);
				optionBoxes.filter(function(elem, index) {
					return optionTexts.get(index).getText().then(function(text) {
						return text === 'None';
					});
				}).first().click();
				browser.sleep(3000);
				expect(dropdown.all(by.css('text')).get(0).getText()).toBe('None');
			}
		});
	}

	function clickService(service) {
		let integrationTabs = element.all(by.css('[id^="ServicesBrowserPanel"] > g > g > rect:nth-child(7)'));
		integrationTabs.get(0).click();
		browser.sleep(3000);
		expect(element(by.id('ExtServiceBrowserPanel(External Services)0:0')).isPresent()).toBe(true);

		let externalServices = element.all(by.css('#topCards svg text'));
		let externalServicesClick = element.all(by.css('#topCards svg > rect:last-of-type'));
		presentElementClick(externalServicesClick.filter(function(elem, index) {
			return externalServices.get(index).getText().then(function(text) {
				return stringContains(text, service);
			});
		}).first(), '', function() {}, function() {
			integrationTabs.get(1).click();
			browser.sleep(3000);
			expect(element(by.id('IntServiceBrowserPanel(Internal Services)0:0')).isPresent()).toBe(true);

			let internalServices = element.all(by.css('#topCards svg text'));
			let internalServicesClick = element.all(by.css('#topCards svg > rect:last-of-type'));
			presentElementClick(internalServicesClick.filter(function(elem, index) {
				return internalServices.get(index).getText().then(function(text) {
					return stringContains(text, service);
				});
			}).first(), '', function() {});
		});
	}

	function deleteConfig() {
		element(by.id('DeleteEventRectangle')).isPresent().then(function(bool) {
			if (bool == true) {
				element(by.id('DeleteEventRectangle')).click();
				browser.sleep(2000);
			}
		});
	}

	function clickSave() {
		element(by.id('SaveEventRectangle')).click();
		ignoreSynchronization(function() {
			expect(element(by.css('text[fill="#4bb3ad"]')).isPresent()).toBe(true);
		});
	}

	//Equipment Configuration
	it('1. Config: Network Equipment IP Adresseses', function () {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(5000).then(function() {
			console.log('\n\nSystem Config: Inputting Config Information:');
			console.log('\nSystem Config: Network Equipment IP Adresseses.');
		});
		expect(browser.getCurrentUrl()).toMatch('systempage');
		expect(element(by.id('NetworkConfigPanel(Equipment Configuration)0:0')).isPresent()).toBe(true);

		let ipAddresses = element.all(by.css('*[id^=ipaddress]'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));

		//1. Network Equipment IP Adresseses
		browser.sleep(3000);
		ipAddresses.get(0).clear();
		// ipAddresses.get(0).getAttribute('value').then(function(text) {
		// 	if(text.length != 0) {
		// 		clearElem(ipAddresses.get(0), text.length);
		// 	}
		// });
		ipAddresses.get(0).sendKeys('10.200.201.2');
		browser.sleep(1000);
		scrollAndClick(nextButtons.get(0));
	});

	it('2. Network Equipment CLI Credentials', function () {
		let usernames = element.all(by.css('*[id^=username]'));
		let passwords = element.all(by.css('*[id^="password"]'));
		let enpasswords = element.all(by.css('*[id^=enpassword]'));
		let plusButtons = element.all(by.css('path[fill="#4bb3ad"] ~ rect'));
		let minusButtons = element.all(by.css('path[fill="#bd3e3e"] ~ rect'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		console.log('\nSystem Config: Network Equipment CLI Credentials.');

		//2. Network Equipment CLI Credentials
		minusButtons.count().then(function(num) {
			if (num > 0){
				browser.sleep(2000);
				for(i = 0; i < (num - 1); i++) {
					minusButtons.get(0).click();
					browser.sleep(1000);
				}
				browser.sleep(1000);
				plusButtons.get(1).click();
				browser.sleep(1000);
				minusButtons.get(0).click();;
			}
		});
		browser.sleep(2000);
		usernames.get(0).sendKeys('cisco');
		passwords.get(0).sendKeys('cisco');
		enpasswords.get(0).sendKeys('cisco');
		browser.sleep(1000);
		plusButtons.get(1).click();
		
		usernames.get(1).sendKeys('admin');
		passwords.get(1).sendKeys('admins');
		enpasswords.get(1).sendKeys('enable');
		browser.sleep(1000);
		plusButtons.get(1).click();

		usernames.get(2).sendKeys('manager');
		passwords.get(2).sendKeys('cloudpost');
		enpasswords.get(2).sendKeys('cloudpost');
		browser.sleep(1000);
		scrollAndClick(nextButtons.get(1));
	});

	it('3. Network Equipment SNMP Credentials', function () {
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		console.log('\nSystem Config: Network Equipment SNMP Credentials');

		//3. Network Equipment SNMP Credentials
		browser.sleep(2000);
		scrollAndClick(nextButtons.get(2));
	});

	it('4. Configure Vulnerability Scan', function () {
		let ipAddresses = element.all(by.css('*[id^=ipaddress]'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		let dropdowns = element.all(by.css('*[id*=frequencyInput]'));
		let optionBoxes = element.all(by.css('#infoLayerGElement g rect'));
		let optionTexts = element.all(by.css('#infoLayerGElement g text'));
		console.log('\nSystem Config: Configure Vulnerability Scan.');

		//4. Configure Vulnerability Scan
		browser.sleep(2000);
		//ipAddresses.get(1).clear();
		ipAddresses.get(1).getAttribute('value').then(function(text) {
			if(text.length != 0) {
				clearElem(ipAddresses.get(1), text.length);
			}
		});
		ipAddresses.get(1).sendKeys('10.200.205.0/24');
		dropdowns.get(0).click();
		browser.sleep(1000);
		dropdowns.all(by.css('text')).get(0).getText().then(function(scanText) {
			if(scanText != "Now") {
				optionBoxes.filter(function(elem, index) {
					return optionTexts.get(index).getText().then(function(text) {
						return text === 'Now';
					});
				}).first().click();
			}
		});
		browser.sleep(1000);
		expect(dropdowns.all(by.css('text')).get(0).getText()).toEqual('Now');
		browser.sleep(1000);
		scrollAndClick(nextButtons.get(3));
	});

	it('5. Configure Password Scan.', function () {
		let ipAddresses = element.all(by.css('*[id^=ipaddress]'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		let dropdowns = element.all(by.css('*[id*=frequencyInput]'));
		let optionBoxes = element.all(by.css('#infoLayerGElement g rect'));
		let optionTexts = element.all(by.css('#infoLayerGElement g text'));
		console.log('\nSystem Config: Configure Password Scan.');

		//5. Configure Password Scan
		browser.sleep(2000);
		//ipAddresses.get(3).clear();
		ipAddresses.get(3).getAttribute('value').then(function(text) {
			if(text.length != 0) {
				clearElem(ipAddresses.get(3), text.length);
			}
		});
		ipAddresses.get(3).sendKeys('10.200.205.0/24');
		dropdowns.get(1).click();
		browser.sleep(1000);
		dropdowns.all(by.css('text')).get(1).getText().then(function(scanText) {
			if(scanText != 'Now') {
				optionBoxes.filter(function(elem, index) {
					return optionTexts.get(index).getText().then(function(text) {
						return text === 'Now';
					});
				}).first().click();
			}
		});
		browser.sleep(1000);
		expect(dropdowns.all(by.css('text')).get(1).getText()).toEqual('Now');
		scrollAndClick(nextButtons.get(4));
	});

	it('6. Enable or Disable Probes', function () {
		let checkBoxes = element.all(by.css('g:nth-child(6) > g:nth-child(3) > svg > g > svg > g > g > rect:nth-child(3)'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		console.log('\nSystem Config: Enable or Disable Probes.');

		//6. Enable or Disable Probes
		browser.sleep(2000);
		element.all(by.css('g:nth-child(6) > g:nth-child(3) > svg > g > svg > g > g > rect:nth-child(1)[fill="#4bb3ad"]')).count().then(function(num) {
			if(num == 0) {
				checkBoxes.get(0).click();
				checkBoxes.get(1).click();
				checkBoxes.get(2).click();
				checkBoxes.get(3).click();
			}
		});
		browser.sleep(1000);
		scrollAndClick(nextButtons.get(5));
	});

	it('7. Configure CloudPost Equipment', function () {
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		console.log('\nSystem Config: Configure CloudPost Equipment');
 
		//7. Configure CloudPost Equipment
		browser.sleep(2000);
		scrollAndClick(nextButtons.get(6));
		browser.sleep(2000);
		nextButtons.get(6).click();
		browser.sleep(2000);
		ignoreSynchronization(function() {
			expect(element(by.css('text[fill="#4bb3ad"]')).isPresent()).toBe(true);
			//expect(element(by.css('text[fill="#4bb3ad"]')).getText()).toMatch("Successfully saved Configuration.");
		});
		browser.sleep(3000);
	});

	//Active Directory
	it('Setting up Active Directory', function() {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		element.all(by.css('#sideMenu~g>text~rect')).get(4).click();

		browser.sleep(4000).then(function() {console.log('\n\nSystem Integration: Setting up Active Directory Service');});
		expect(element(by.id('ServicesBrowserPanel(Service Integration)0:0')).isPresent()).toBe(true);

		let dropdown = element(by.css('*[id*=pollFrequency]'));
		let optionBoxes = element.all(by.css('#infoLayerGElement > g > rect'));
		let optionTexts = element.all(by.css('#infoLayerGElement > g > text'));
		clickService('Active Directory');
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(2).click();
		browser.sleep(2000);

		deleteConfig();

		element(by.id('ad-servername')).clear();
		element(by.id('ad-servername')).sendKeys('ad-server-2');
		element(by.id('ad-domainname')).clear();
		element(by.id('ad-domainname')).sendKeys('ad2.cpn.lan');
		element(by.id('ad-serverip')).clear();
		element(by.id('ad-serverip')).sendKeys('192.168.103.11');
		element(by.id('ad-username')).clear();
		element(by.id('ad-username')).sendKeys('cpnadmin');
		element(by.id('ad-password')).clear();
		element(by.id('ad-password')).sendKeys('ironfist2!');

		dropdown.click();
		browser.sleep(2000);		
		optionBoxes.filter(function(elem, index) {
			return optionTexts.get(index).getText().then(function(text) {
				return text === '1 hour';
			});
		}).first().click();
		browser.sleep(1000);
		expect(dropdown.all(by.css('text')).get(0).getText()).toEqual('1 hour');

		// browser.sleep(2000);
		// element(by.id('SaveEventRectangle')).click();
		browser.sleep(3000);
		clickSave();
		browser.sleep(3000);
	});

	//Email & Alarms Notification
	it('Setting up Internal Alarms Notification Service', function() {
		let emailDropdown = element(by.css('*[id*=securitymode]'));
		let emailOptionBoxes = element.all(by.css('#infoLayerGElement g rect'));
		let emailOptionTexts = element.all(by.css('#infoLayerGElement g text'));
		clickService('Email|SMTP');

		//element.all(by.css('#topCards image ~ rect ~ rect')).get(0).click();
		browser.sleep(2000).then(function(){console.log('\nSystem Integration: Setting up Email Service');});

		deleteConfig();

		element(by.id('smtp-server')).clear();
		element(by.id('smtp-server')).sendKeys('smtp.office365.com');
		element(by.id('smtp-port')).clear();
		element(by.id('smtp-port')).sendKeys('587');
		element(by.id('smtp-username')).clear();
		element(by.id('smtp-username')).sendKeys('support@cloudpostnetworks.com');
		element(by.id('smtp-password')).clear();
		element(by.id('smtp-password')).sendKeys('sP0bS3lN!');

		emailDropdown.click();
		browser.sleep(3000);
		emailOptionBoxes.filter(function(elem, index) {
			return emailOptionTexts.get(index).getText().then(function(text) {
				return text === 'STARTTLS';
			});
		}).first().click();
		browser.sleep(1000);
		expect(emailDropdown.all(by.css('text')).get(0).getText()).toEqual('STARTTLS');

		browser.sleep(3000);
		clickSave();
		browser.sleep(3000);

		let alarmEmailElements = element.all(by.css('*[id^=email]'));
		let alarmDropdown = element(by.css('*[id*=frequencyInput-alarms-notification]'));
		let alarmTimeInputs = element.all(by.css('div input'));
		let alarmOptionBoxes = element.all(by.css('#infoLayerGElement g rect'));
		let alarmOptionTexts = element.all(by.css('#infoLayerGElement g text'));
		
		clickService('Alarms Notification');
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(1).click();		
		browser.sleep(2000).then(function(){console.log('\nSystem Integration: Setting up Alarms Notification Service');});

		deleteConfig();

		var url = require('url');
		var host = url.parse(browser.params.url, true).host;
		alarmEmailElements.get(0).clear();
		alarmEmailElements.get(0).sendKeys(browser.params.email);
		alarmEmailElements.get(1).clear()
		alarmEmailElements.get(1).sendKeys(host+'_Alarm_Alert');

		dropdownReset(alarmDropdown, alarmOptionBoxes, alarmOptionTexts);

		alarmDropdown.click();
		browser.sleep(3000);
		alarmOptionBoxes.filter(function(elem, index) {
			return alarmOptionTexts.get(index).getText().then(function(text) {
				return text === 'Daily';
			});
		}).first().click();
		browser.sleep(3000);
		expect(alarmDropdown.all(by.css('text')).get(0).getText()).toEqual('Daily');

		alarmTimeInputs.get(2).clear();
		alarmTimeInputs.get(2).sendKeys('11');
		alarmTimeInputs.get(3).clear();
		alarmTimeInputs.get(3).sendKeys('0');
		// browser.sleep(2000);
		// element(by.id('SaveEventRectangle')).click();

		browser.sleep(3000);
		clickSave();
		browser.sleep(3000);
	});

	//Devices Notification
	it('Setting up Internal Devices Notification Service', function() {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		element.all(by.css('#sideMenu~g>text~rect')).get(4).click();

		console.log('\nSystem Integration: Setting up Devices Notification Service');
		let deviceEmailElements = element.all(by.css('*[id^=email]'));
		let deviceDropdown = element(by.css('*[id*=frequencyInput-devicegroups-notification]'));
		let deviceTimeInputs = element.all(by.css('div input'));
		let deviceOptionBoxes = element.all(by.css('#infoLayerGElement g rect'));
		let deviceOptionTexts = element.all(by.css('#infoLayerGElement g text'));
		clickService('Devices Notification');
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(2).click();
		browser.sleep(2000);

		deleteConfig();

		var url = require('url');
		var host = url.parse(browser.params.url, true).host;
		deviceEmailElements.get(0).clear();
		deviceEmailElements.get(0).sendKeys(browser.params.email);
		deviceEmailElements.get(1).clear()
		deviceEmailElements.get(1).sendKeys(host+'_Devices_Alert');

		dropdownReset(deviceDropdown, deviceOptionBoxes, deviceOptionTexts);

		deviceDropdown.click();
		browser.sleep(3000);
		deviceOptionBoxes.filter(function(elem, index) {
			return deviceOptionTexts.get(index).getText().then(function(text) {
				return text === 'Daily';
			});
		}).first().click();
		browser.sleep(3000);
		expect(deviceDropdown.all(by.css('text')).get(0).getText()).toEqual('Daily');

		deviceTimeInputs.get(2).clear();
		deviceTimeInputs.get(2).sendKeys('11');
		deviceTimeInputs.get(3).clear();
		deviceTimeInputs.get(3).sendKeys('0');

		browser.sleep(3000);
		clickSave();
		browser.sleep(3000);
	});

	//Cisco ISE Service
	it('Setting up Cisco ISE Service', function() {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		element.all(by.css('#sideMenu~g>text~rect')).get(4).click();

		console.log('\nService Integration: Setting up Cisco ISE Service');
		let ciscoPrimaryIp = element(by.id('ise-primaryip'));
		let ciscoMntUsername = element(by.id('ise-mnt-username'));
		let ciscoMntPassword = element(by.id('ise-mnt-password'));
		let ciscoErsUsername = element(by.id('ise-ers-username'));
		let ciscoErsPassword = element(by.id('ise-ers-password'));
		clickService('Cisco|ISE');
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(2).click();
		browser.sleep(2000);

		deleteConfig();

		ciscoPrimaryIp.clear();
		ciscoPrimaryIp.sendKeys('192.168.104.245');
		ciscoMntUsername.clear();
		ciscoMntUsername.sendKeys('augustine');
		ciscoMntPassword.clear();
		ciscoMntPassword.sendKeys('Duff135!');
		ciscoErsUsername.clear();
		ciscoErsUsername.sendKeys('augustine');
		ciscoErsPassword.clear();
		ciscoErsPassword.sendKeys('Duff135!');

		browser.sleep(3000);
		clickSave();
		browser.sleep(3000);
	});

	//SNMP Agent Service
	it('Setting up SNMP Agent Service', function() {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		element.all(by.css('#sideMenu~g>text~rect')).get(4).click();

		console.log('\nService Integration: Setting up SNMP Agent Service');
		let snmpV2 = element(by.css('[id^="snmpV2"]'));
		let snmpReceiverIP = element(by.css('[id^="receiverIP"]'));
		let snmpReceiverCommStr = element(by.css('[id^="receiverCommStr"]'));
		clickService('SNMP');
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(2).click();
		browser.sleep(2000);

		deleteConfig();

		snmpV2.clear();
		snmpV2.sendKeys('public');
		snmpReceiverIP.clear();
		snmpReceiverIP.sendKeys('192.168.102.59');
		snmpReceiverCommStr.clear();
		snmpReceiverCommStr.sendKeys('public');

		browser.sleep(3000);
		clickSave();
		browser.sleep(3000);
	});

	//Servicenow Service
	it('Setting up Servicenow Service', function() {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		element.all(by.css('#sideMenu~g>text~rect')).get(4).click();

		console.log('\nService Integration: Setting up Servicenow Service');
		let servicenowUsername = element(by.id('servicenow-username'));
		let servicenowPassword = element(by.id('servicenow-password'));
		let servicenowUiUrl = element(by.id('servicenow-url'));
		let servicenowApiUrl = element(by.id('servicenow-apiurl'));
		clickService('Servicenow');
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(2).click();
		browser.sleep(2000);

		deleteConfig();

		servicenowUsername.clear();
		servicenowUsername.sendKeys('admin');
		servicenowPassword.clear();
		servicenowPassword.sendKeys('Vara@123')
		servicenowUiUrl.clear();
		servicenowUiUrl.sendKeys('https://dev50451.service-now.com/')
		servicenowApiUrl.clear();
		servicenowApiUrl.sendKeys('https://dev50451.service-now.com/api/now/');

		browser.sleep(3000);
		clickSave();
		browser.sleep(3000);
	});

	//Nuvolo Service
	it('Setting up Nuvolo Service', function() {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		element.all(by.css('#sideMenu~g>text~rect')).get(4).click();

		console.log('\nService Integration: Setting up Nuvolo Service');
		let nuvoloUsername = element(by.id('nuvolo-username'));
		let nuvoloPassword = element(by.id('nuvolo-password'));
		let nuvoloUiUrl = element(by.id('nuvolo-url'));
		let nuvoloApiUrl = element(by.id('nuvolo-apiurl'));
		let nuvoloSourceKey = element(by.id('nuvolo-sourcekey'));
		clickService('Nuvolo');
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(2).click();
		browser.sleep(2000);

		deleteConfig();

		nuvoloUsername.clear();
		nuvoloUsername.sendKeys('cloudpost');
		nuvoloPassword.clear();
		nuvoloPassword.sendKeys('Jx848593p%dK9D4d')
		nuvoloUiUrl.clear();
		nuvoloUiUrl.sendKeys('https://ven02434.service-now.com/')
		nuvoloApiUrl.clear();
		nuvoloApiUrl.sendKeys('https://ven02434.service-now.com/api/x_nuvo_eam/eam/');
		nuvoloSourceKey.clear();
		nuvoloSourceKey.sendKeys('38ba7360db3f0300e319f91ebf9619a8');

		browser.sleep(3000);
		clickSave();
		browser.sleep(3000);
	});

	//Splunk Service
	it('Setting up Splunk Service', function() {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		element.all(by.css('#sideMenu~g>text~rect')).get(4).click();

		console.log('\nSystem Integration: Setting up Splunk Service');
		let serverIp = element(by.id('splunk-serverip'));
		let serverTcpPort = element(by.id('splunk-tcp-port'));
		let serverUiPort = element(by.id('splunk-ui-port'));
		clickService('Splunk');
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(2).click();
		browser.sleep(2000);

		deleteConfig();

		serverIp.clear();
		serverIp.sendKeys('192.168.104.75');
		serverTcpPort.clear();
		serverTcpPort.sendKeys('3004');
		serverUiPort.clear();
		serverUiPort.sendKeys('8000');

		browser.sleep(3000);
		clickSave();
		browser.sleep(3000);
	});

	//External Authentication LDAP
	it('Setting up External Authentication LDAP Service', function() {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		element.all(by.css('#sideMenu~g>text~rect')).get(4).click();

		console.log('\nSystem Integration: Setting up External Authentication LDAP Service');
		let ldapServerName = element(by.id('radius-serverip'));
		let ldapIdentity = element(by.id('radius-ldapIdentity'));
		let ldapBaseDn = element(by.id('radius-ldapBaseDn'));
		let ldapPassword = element(by.id('pan-ldapPassword'));
		clickService('External Authentication');
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(2).click();
		browser.sleep(2000);

		deleteConfig();

		ldapServerName.clear();
		ldapServerName.sendKeys('192.168.104.181');
		ldapIdentity.clear();
		ldapIdentity.sendKeys('dc=mycpnetwork,dc=com');
		ldapBaseDn.clear();
		ldapBaseDn.sendKeys('cn=administrator,cn=Users,dc=mycpnetwork,dc=com');
		ldapPassword.clear();
		ldapPassword.sendKeys('CPN@123456');

		browser.sleep(3000);
		clickSave();
		browser.sleep(3000);
	});

	//External Authentication AD
	it('Setting up External Authentication AD Service', function() {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		element.all(by.css('#sideMenu~g>text~rect')).get(4).click();

		console.log('\nSystem Integration: Setting up External Authentication AD Service');
		let adHostName = element(by.id('ad-servername'));
		let adDomainName = element(by.id('ad-domainname'));
		let adServerIp = element(by.id('ad-serverip'));
		let adLoginName = element(by.id('ad-username'));
		let adLoginPassword = element(by.id('ad-password'));
		clickService('External Authentication');
		browser.sleep(2000);
		element(by.css('[id^="ServiceDetailPanel"] g:nth-child(5) > rect:nth-child(7)')).click();
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(2).click();
		browser.sleep(2000);

		deleteConfig();
		
		adHostName.clear();
		adHostName.sendKeys('WIN-7408VMD8DGH.mycpnetwork.com');
		adDomainName.clear();
		adDomainName.sendKeys('mycpnetwork.com');
		adServerIp.clear();
		adServerIp.sendKeys('192.168.104.181');
		adLoginName.clear();
		adLoginName.sendKeys('administrator');
		adLoginPassword.clear();
		adLoginPassword.sendKeys('CPN@123456');

		browser.sleep(3000);
		clickSave();
		browser.sleep(3000);
	});

	//Fortinet Dervice
	it('Setting up Fortinet Service', function() {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		element.all(by.css('#sideMenu~g>text~rect')).get(4).click();

		console.log('\nService Integration: Setting up Fortinet Service');
		let fortinetServerName = element(by.id('fortinet-serverip'));
		let fortinetUsername = element(by.id('fortinet-username'));
		let fortinetPassword = element(by.id('fortinet-password'));
		clickService('Fortinet');
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(2).click();
		browser.sleep(2000);

		deleteConfig();

		fortinetServerName.clear();
		fortinetServerName.sendKeys('10.200.201.164');
		fortinetUsername.clear();
		fortinetUsername.sendKeys('admin');
		fortinetPassword.clear();
		fortinetPassword.sendKeys('ironfist2!');

		browser.sleep(3000);
		clickSave();
		browser.sleep(20000);
	});
});