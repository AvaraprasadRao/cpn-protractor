//test_05_group.js

describe('Group Tab Sanity Suite', function() {

	it('Traffic Analysis, Alarm Test', function() {
		loginUser(usrnm_keys, pswrd_keys);
		
		let underCircleTextsFirst = element.all(by.css('#underNodeLayer > g > g > text:nth-child(1)'));
		let underCircleClick = element.all(by.css('#underNodeLayer > g > circle:nth-child(1)'));
		let overCircleTextsFirst = element.all(by.css('#overNodeLayer > g > g > text:nth-child(1)'));
		let overCircleClick = element.all(by.css('#overNodeLayer > g > circle:nth-child(1)'));
		let flowAnalysisText = element.all(by.css('#SankeyDiagram > text'));
		let flowAnalysisIcon = element.all(by.css('#SankeyDiagram > g > rect'));
		let communicationTraffic = element.all(by.css('#CSTDiagramCtrl > g > svg > g > g:nth-child(4) > svg > g > g > rect:nth-child(7)'));
		let communicationText = element.all(by.css('#CSTDiagramCtrl > g > svg > g > g:nth-child(4) > svg > g > g > text:nth-child(4)'));
		
		console.log('\nGroup Sanity: Check if Traffic Analysis Flow is correct.');
		element(by.css('#gnTopMenu > rect:nth-child(14)')).click();
		browser.sleep(7000);
		expect(browser.getCurrentUrl()).toMatch('grouppage');
		element.all(by.css('#sideMenu~g>text~rect')).get(3).click();
		
		browser.sleep(3000);
		expect(element(by.id('TrafficAnalysisPanel(Classification Group Traffic Analysis)0:0')).isPresent()).toBe(true);
		var workstations = underCircleClick.filter(function(elem, index) {
			return underCircleTextsFirst.get(index).getText().then(function(text) {
				return text === 'Workstations';
			});
		}).first();
		scrollAndClick(workstations);
		browser.sleep(3000);

		var badURL = overCircleClick.filter(function(elem, index) {
			return overCircleTextsFirst.get(index).getText().then(function(text) {
				return text === 'BadUrl';
			});
		}).last();
		scrollAndClick(badURL);
		browser.sleep(3000);

		
		var badURLxWorkstations = communicationTraffic.filter(function(elem, index) {
			return communicationText.get(index).getText().then(function(text) {
				return text === 'W: Workstations';
			});
		}).first();
		scrollAndClick(badURLxWorkstations)
		browser.sleep(3000);

		expect(element(by.id('G2GTrafficPanel(Classification Group Flow Analysis)0:0')).isPresent()).toBe(true);
		browser.sleep(3000);

		flowAnalysisIcon.filter(function(elem, index) {
			return flowAnalysisText.get(index).getText().then(function(text) {
				return text === 'iMacR5.cpn.lan';
			});
		}).first().click();
		browser.sleep(3000);

		expect(browser.getCurrentUrl()).toMatch('devicepage');
		element(by.css('#DeviceDetailPanel\\28 88\\3a 63\\3a DF\\3a CA\\3a 40\\3a C7\\29 0\\3a 0 > g:nth-child(4) > g:nth-child(6) > rect:nth-child(7)')).click();
		browser.sleep(10000)
		//element(by.css('#DeviceDetailPanel\\28 Device\\20 Detail\\29 BPCTxtField')).sendKeys('104.192.143.2');
		browser.sleep(10000);
		expect(element(by.css('g:nth-child(3) > svg > g:nth-child(4) > g > g:nth-child(5) > svg > g:nth-child(1) > g > rect:nth-child(3)')).getAttribute('fill')).toEqual('#c67734');
		element.all(by.css('g:nth-child(4) > g:nth-child(5) > rect:nth-child(7)')).get(0).click();
		browser.sleep(6000);
		//expect(element(by.css('#DeviceAlarmSection\\28 Malware\\20 Site\\20 Access\\29 > svg > text')).getText()).toMatch('104.192.143.2');
		expect(element(by.css('#DeviceAlarmSection\\28 Malware\\20 Site\\20 Access\\29 > circle')).getAttribute('fill')).toEqual('#c67734')
	});
});