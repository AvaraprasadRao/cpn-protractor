//test_account_creation.js

describe('Account Creation Suite', function() {

	function createUser(username, password, role) {
		element.all(by.css('#topCards > svg > g > g')).count().then(function(initial){
			console.log('Initial Accounts Number: '+initial);
			element(by.css('[id^="LocalAdminsBrowserPanel"] > g:nth-child(4) > g:nth-child(5) > rect:nth-child(7)')).click();

			element(by.id('username')).sendKeys(username);
			browser.sleep(2000);
			element(by.id('password')).sendKeys(password);
			browser.sleep(2000);
			//turn off 2FA Auth Button
			presentElement(element(by.css('[id^="AdminAccountDetail"] g > g:nth-child(10) > rect:nth-child(2)')), '', function() {
				element(by.css('[id^="AdminAccountDetail"] g > g:nth-child(10) > rect:nth-child(2)')).getAttribute('fill').then(function(color) {
					if (color == '#4bb3ad') {
						element(by.css('[id^="AdminAccountDetail"] g > g:nth-child(10) > rect:nth-child(4)')).click();
						browser.sleep(1000);
						expect(element(by.css('[id^="AdminAccountDetail"] > g > svg > g > g:nth-child(10) > rect:nth-child(2)')).getAttribute('fill')).toBe('transparent');
					}
				});
			});
			element(by.id('firstname')).sendKeys('John');
			browser.sleep(1000);
			element(by.id('lastname')).sendKeys('Doe');
			browser.sleep(1000);
			//Check Role
			presentElement(element(by.css('#dropDownContentG-user-roles > text')), '', function() {
				element(by.css('#dropDownContentG-user-roles > text')).getText().then(function(text) {
					if(!(text.includes(role))) {
						element(by.css('#dropDownContentG-user-roles > rect:nth-child(8)')).click();
						browser.sleep(2000);
						let dropdownOptions = element.all(by.css('#infoLayerGElement > g > text'));
						let dropdownClick = element.all(by.css('#infoLayerGElement > g > rect'));
						dropdownClick.filter(function(elem, index) {
							return dropdownOptions.get(index).getText().then(function(opt) {
								return opt.includes(role);
							});
						}).first().click();
						browser.sleep(2000);
						expect(element(by.css('#dropDownContentG-user-roles > text')).getText()).toMatch('User');
					}
				})
			});
			//turn off 2FA Dropdown
			presentElement(element(by.css('#dropDownContentG-mfa-choices > text')), '', function() {
				element(by.css('#dropDownContentG-mfa-choices > text')).getText().then(function(text) {
					if(!(text.includes('None'))) {
						element(by.css('#dropDownContentG-mfa-choices > rect:nth-child(8)')).click();
						browser.sleep(2000);
						let dropdownOptions = element.all(by.css('#infoLayerGElement > g > text'));
						let dropdownClick = element.all(by.css('#infoLayerGElement > g > rect'));
						dropdownClick.filter(function(elem, index) {
							return dropdownOptions.get(index).getText().then(function(opt) {
								return opt.includes('None');
							});
						}).first().click();
						browser.sleep(2000);
						expect(element(by.css('#dropDownContentG-mfa-choices > text')).getText()).toMatch('None');
					}
				})
			});
			element(by.id('email')).sendKeys('placeholder@cloudpostnetworks.com');
			browser.sleep(4000);
			element(by.id('SaveEventRectangle')).click();
			browser.sleep(4000);
			element.all(by.css('#topCards > svg > g > g')).count().then(function(final) {
				console.log('Current Accounts Number: '+final);
				expect(final).toBe(initial+1);
				browser.sleep(4000);
			});
		});
	}

	function deleteUser(name) {
		element.all(by.css('#topCards > svg > g > g')).count().then(function(initial){
			console.log('Initial Accounts Number: '+initial);
			browser.sleep(2000);
			let usernames = element.all(by.css('#topCards > svg  svg:nth-child(5) > text'));
			let deleteButtons = element.all(by.css('#topCards > svg > g rect:last-child'));
			//let deleteButtonsOld = element.all(by.css('#topCards > svg > g > g > svg > rect:nth-child(10)'));

			deleteButtons.filter(function(elem, index) {
				return usernames.get(index).getText().then(function(text) {
					return text === name;
				});
			}).first().click();
			browser.sleep(2000);

			expect(element(by.css('#modalLayerGElement g:nth-child(5) text:nth-child(2)')).getText()).toMatch(name);

			element(by.css('#DeleteEventRectangle')).click().then(function() {
				browser.sleep(5000);
				element.all(by.css('#topCards > svg > g > g')).count().then(function(final) {
					console.log('Current Accounts Number: '+final);
					expect(final).toBe(initial-1);
				});
			});
		});
	}

	//login with admin username and password, click Group Tab and check if at least one card is viewable
	it('Short Password User Creation', function() {
		console.log('\n\nAccount Creation: Create User with Short Password (8 characters).');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		//element(by.css('g > g:nth-child(9) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('User Accounts', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);
		createUser('short_user', 'shortpas', 'User');
	});

	it('Login as Short Password User', function() {
		console.log('\nAccount Creation: Check if able to login using Short Password User credentials.');
		loginUser('short_user', 'shortpas');
		browser.sleep(deviceLoadDelay);
		expect(browser.getCurrentUrl()).toMatch('systemsummary');
	});

	it('Delete Short Password User', function() {
		console.log('\nAccount Creation: Check if able to delete Short Password User.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		//element(by.css('g > g:nth-child(9) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('User Accounts', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);
		deleteUser('short_user');
	});

	it('Medium Password User Creation', function() {
		console.log('\n\nAccount Creation: Create User with Medium Password (25 characters).');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		//element(by.css('g > g:nth-child(9) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('User Accounts', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);
		createUser('medium_user', '0123456789012345678901234', 'User');
	});

	it('Login as Medium Password User', function() {
		console.log('\nAccount Creation: Check if able to login using Medium Password User credentials.');
		loginUser('medium_user', '0123456789012345678901234');
		browser.sleep(deviceLoadDelay);
		expect(browser.getCurrentUrl()).toMatch('systemsummary');
	});

	it('Delete Medium Password User', function() {
		console.log('\nAccount Creation: Check if able to delete Medium Password User.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		//element(by.css('g > g:nth-child(9) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('User Accounts', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);
		deleteUser('medium_user');
	});

	it('Long Password User Creation', function() {
		console.log('\n\nAccount Creation: Create User with Long Password (40 characters).');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		//element(by.css('g > g:nth-child(9) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('User Accounts', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);
		createUser('long_user', '0123456789012345678901234567890123456789', 'User');
	});

	it('Login as Long Password User', function() {
		console.log('\nAccount Creation: Check if able to login using Long Password User credentials.');
		loginUser('long_user', '0123456789012345678901234567890123456789');
		browser.sleep(deviceLoadDelay);
		expect(browser.getCurrentUrl()).toMatch('systemsummary');
	});

	it('Delete Long Password User', function() {
		console.log('\nAccount Creation: Check if able to delete Long Password User.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		//element(by.css('g > g:nth-child(9) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('User Accounts', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);
		deleteUser('long_user');
	});
});