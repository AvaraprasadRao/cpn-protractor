//test_menu_group.js
describe('Group Menu Suite', function() {

	it('Groups Table is Populated', function() {
		console.log('\n\nGroup Menu: Check if Groups Table is populated.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(14)')).click();
		browser.sleep(3000);
		expect(element.all(by.css('#topCards [id]')).count()).toBeGreaterThan(0);
	});

	it('Click on few groups', function() {
		console.log('\nGroup Menu: Check if Groups Table is clickable.');
		element.all(by.css('#topCards [id]')).count().then(function(count) {
			if(count > 3) {
				var groupName1 = element.all(by.css('#topCards [id] > svg > svg > text')).get(0).getText().then(function(text) {
					console.log("\nRow 1 Group Name: "+text)
					return text;
				});
				var groupType1 = element.all(by.css('#topCards [id] > svg > text:nth-child(6)')).get(0).getText().then(function(text) {
					console.log("Row 1 Group Type: "+text);
					return text;
				});
				element.all(by.css('#topCards [id] > svg > path+rect')).get(0).click().then(function() {
					browser.sleep(2000);
					expect(element(by.css('[id^="GroupDetailPanel"]')).isPresent()).toBe(true);
					expect(element(by.css('[id^="GroupDetailPanel"] > text.dashboardTextSlimForTitle')).getText()).toContain(groupName1);
					expect(element(by.css('[id^="GroupDetailPanel"] > g:nth-child(3) > svg > g:nth-child(3) > svg > text:nth-child(3)')).getText()).toBe(groupType1);
					element(by.css('[id^="GroupDetailPanel"] > g:nth-child(4) > g:nth-child(5) > rect:nth-child(7)')).click();
					browser.sleep(2000);
				});
				
				var groupName2 = element.all(by.css('#topCards [id] > svg > svg > text')).get(1).getText().then(function(text) {
					console.log("\nRow 2 Group Name: "+text)
					return text;
				});
				var groupType2 = element.all(by.css('#topCards [id] > svg > text:nth-child(6)')).get(1).getText().then(function(text) {
					console.log("Row 2 Group Type: "+text)
					return text;
				});
				element.all(by.css('#topCards [id] > svg > path+rect')).get(1).click().then(function() {
					browser.sleep(2000);
					expect(element(by.css('[id^="GroupDetailPanel"]')).isPresent()).toBe(true);
					expect(element(by.css('[id^="GroupDetailPanel"] > text.dashboardTextSlimForTitle')).getText()).toContain(groupName2);
					expect(element(by.css('[id^="GroupDetailPanel"] > g:nth-child(3) > svg > g:nth-child(3) > svg > text:nth-child(3)')).getText()).toBe(groupType2);
					element(by.css('[id^="GroupDetailPanel"] > g:nth-child(4) > g:nth-child(5) > rect:nth-child(7)')).click();
					browser.sleep(2000);
				});

				var groupName3 = element.all(by.css('#topCards [id] > svg > svg > text')).get(2).getText().then(function(text) {
					console.log("\nRow 3 Group Name: "+text)
					return text;
				});
				var groupType3 = element.all(by.css('#topCards [id] > svg > text:nth-child(6)')).get(2).getText().then(function(text) {
					console.log("Row 3 Group Type: "+text)
					return text;
				});
				element.all(by.css('#topCards [id] > svg > path+rect')).get(2).click().then(function() {
					browser.sleep(2000);
					expect(element(by.css('[id^="GroupDetailPanel"]')).isPresent()).toBe(true);
					expect(element(by.css('[id^="GroupDetailPanel"] > text.dashboardTextSlimForTitle')).getText()).toContain(groupName3);
					expect(element(by.css('[id^="GroupDetailPanel"] > g:nth-child(3) > svg > g:nth-child(3) > svg > text:nth-child(3)')).getText()).toBe(groupType3);
					element(by.css('[id^="GroupDetailPanel"] > g:nth-child(4) > g:nth-child(5) > rect:nth-child(7)')).click();
					browser.sleep(2000);
				});
			}
			else {
				console.log("Expected at least 3 groups")
			}
		});
	});

	it('Click on Info Section', function() {
		console.log('\nGroup Menu: Click info sections from table.');
		//Clicking Device Members Icon
		browser.sleep(2000).then(function() {console.log('Clicking Device Members Icon, if present');});
		element.all(by.css('svg > rect:nth-child(18)[fill]')).filter(function(icon) {
			return icon.isPresent();
		}).first().click();
		browser.sleep(2000);
		expect(browser.getCurrentUrl()).toMatch('devicepage');
		//element(by.css('g:nth-child(4) > g:nth-child(16) > rect:nth-child(7)')).click();
		browser.sleep(2000);
		element(by.css('#gnTopMenu > rect:nth-child(13)')).click();

		//Clicking Profile Members Icon
		browser.sleep(2000).then(function() {console.log('Clicking Profile Members Icon, if present');});
		element.all(by.css('svg > rect:nth-child(19)[fill]')).filter(function(icon) {
			return icon.isPresent();
		}).first().click();
		browser.sleep(2000);
		expect(browser.getCurrentUrl()).toMatch('profilepage');
		//element(by.css('g:nth-child(4) > g:nth-child(11) > rect:nth-child(7)')).click();
		browser.sleep(2000);
		element(by.css('#gnTopMenu > rect:nth-child(13)')).click();

		// //Clicking Applications Usage Icon
		browser.sleep(2000).then(function() {console.log('Clicking Applications Usage Icon, if present');});
		element.all(by.css('svg > rect:nth-child(20)[fill]')).filter(function(icon) {
			return icon.isPresent();
		}).first().click();
		browser.sleep(2000);
		expect(browser.getCurrentUrl()).toMatch('apppage');
		//element(by.css('g:nth-child(4) > g:nth-child(8) > rect:nth-child(7)')).click();
		browser.sleep(2000);
		element(by.css('#gnTopMenu > rect:nth-child(13)')).click();
	});

	it('Group Table Sorting Check', function() {
		console.log('\nGroup Menu: Sorting Functionality Check.');
		browser.sleep(3000);
		element.all(by.css('#topCards > g svg > g > path+rect')).get(0).click();
		browser.sleep(3000);
		var firstGroupName = element.all(by.css('#topCards [id] > svg > svg > text')).get(0).getText().then(function(text) {
			console.log("Clicking Up Arrow Filter - Top Row Group Name: "+text)
			return text;
		});

		element.all(by.css('#topCards > g svg > g > path+rect')).get(1).click();
		browser.sleep(3000);

		var lastGroupName = element.all(by.css('#topCards [id] > svg > svg > text')).get(0).getText().then(function(text) {
			console.log("Clicking Down Arrow Filter - Top Row Group Name: "+text);
			return text;
		});

		expect(firstGroupName).not.toBe(lastGroupName);
	});

	it('Group Table Search Bar Check', function() {
		console.log('\nGroup Menu: Search Bar Functionality Check.');
		browser.sleep(2000);
		element(by.css('[id^="GroupBrowserPanel"] [id$="BPCTxtField"]')).sendKeys('Misc Devices');
		browser.sleep(2000);
		var groupNum = element(by.css('[id^="GroupBrowserPanel"] > g:nth-child(4) > g:nth-child(5) > text')).getText().then(function(text) {
			var num = text.match(/\d/g).join("");
			return parseInt(num, 10);
		});
		expect(groupNum).toBe(1);
		expect(element.all(by.css('#topCards [id] > svg > svg > text')).get(0).getText()).toEqual('Misc Devices');
	});

	it('Group Table Undo Filter Check', function() {
		console.log('\nGroup Menu: Clear All Filters Check.');
		var initialNum = element(by.css('[id^="GroupBrowserPanel"] > g:nth-child(4) > g:nth-child(5) > text')).getText().then(function(text) {
			var num = text.match(/\d/g).join("");
			return parseInt(num, 10);
		});

		element(by.css('[id^="GroupBrowserPanel"] > g:nth-child(4) > g:nth-child(9) > rect:nth-child(7)')).click();
		browser.sleep(4000);

		var afterClearNum = element(by.css('[id^="GroupBrowserPanel"] > g:nth-child(4) > g:nth-child(5) > text')).getText().then(function(text) {
			var num = text.match(/\d/g).join("");
			return parseInt(num, 10);
		});

		expect(afterClearNum).toBeGreaterThan(initialNum);
	});

	// it('Risk Analysis Click on some bubbles', function() {
	// 	console.log('\nGroup Menu: Risk Analysis Bubble Clicking.');

	//	element.all(by.css('#sideMenu~g>text~rect')).get(2).click();
	// 	browser.sleep(5000);
	// 	presentElementClick(element(by.css('[id$="Network Devices"] > circle:nth-child(3)')), 'Network Devices Bubble', function() {
	// 		browser.sleep(3000);
	// 		expect(element(by.css('[id*="Flow View"]')).isPresent()).toBe(true);
	// 		expect(element(by.css('[id*="Flow View"] > g:nth-child(4) > g:nth-child(4) > text')).getText()).toContain("Network Devices");
	// 	});
	// });

	// it('Risk Analysis Check for traffic flow', function() {
	// 	console.log('\nGroup Menu: Risk Analysis check Traffic Flow.');
	// });

	it('Traffic Analysis Check if Bubbles Present', function() {
		console.log('\nGroup Traffic Analysis: Checking if Bubbles are present.');

		element.all(by.css('#sideMenu~g>text~rect')).get(3).click();
		browser.sleep(4000);
		expect(element(by.id('TrafficAnalysisPanel(Classification Group Traffic Analysis)0:0')).isPresent()).toBe(true);

		let internetBubbles = element.all(by.css('circle[fill="#272f3e"]+circle[stroke="#979cd1"]'));
		let classificationBubbles = element.all(by.css('circle[fill="#272f3e"]+circle[stroke="#dcc482"]'));
		let intranetBubbles = element.all(by.css('circle[fill="#272f3e"]+circle[stroke="#999999"]'));
		expect(internetBubbles.count()).toBeGreaterThan(0);
		expect(classificationBubbles.count()).toBeGreaterThan(0);
		expect(intranetBubbles.count()).toBeGreaterThan(0);
	});

	it('Traffic Analysis Click all bubbles', function() {
		console.log('\nGroup Traffic Analysis: Clicking all Bubbles.');

		let bubbleGroups = element.all(by.css('#underNodeLayer circle[fill="#272f3e"]'));
		bubbleGroups.each(function(elem, index) {
			scrollToView(bubbleGroups.get(index));
			browser.actions().mouseMove(bubbleGroups.get(index)).click().perform();
			browser.sleep(2000);
			browser.actions().mouseMove({x: 50, y: 0}).perform();
			expect(element.all(by.css('#linkLayer > path')).count()).toBeGreaterThan(0);
			browser.sleep(2000);
			browser.actions().mouseMove({x: -50, y: 0}).click().mouseUp().perform();
			browser.sleep(2000);
			browser.navigate().back();
			browser.sleep(2000);
			browser.navigate().forward();
			browser.sleep(3000);
		});
		expect(bubbleGroups.count()).toBeGreaterThan(0);
	});

	it('Traffic Analysis Filter Check', function() {
		console.log('\nGroup Traffic Analysis: Filter Functionality Check.');

		element.all(by.css('#sideMenu~g>text~rect')).get(3).click();
		browser.sleep(3000);

		let bubbleGroups = element.all(by.css('#underNodeLayer circle[fill="#272f3e"]'));
		let bubbleFilters = element.all(by.css('.panelSlideSwitchRect~rect'));

		bubbleGroups.count().then(function(initialCount) {
			bubbleFilters.each(function(elem, index) {
				elem.click();
				browser.sleep(2000);
			});
			browser.sleep(3000).then(function() {
				bubbleGroups.count().then(function(currentCount) {
					expect(currentCount).toBeLessThan(initialCount);
				});
			})
		});
	});

	it('Traffic Analysis Undo Filter Check', function() {
		console.log('\nGroup Traffic Analysis: Clear All Filters Check.');

		element.all(by.css('#sideMenu~g>text~rect')).get(3).click();
		browser.sleep(3000);

		let bubbleGroups = element.all(by.css('#underNodeLayer circle[fill="#272f3e"]'));
		let bubbleFilters = element.all(by.css('.panelSlideSwitchRect~rect'));

		bubbleGroups.count().then(function(initialCount) {
			bubbleFilters.each(function(elem, index) {
				elem.click();
				browser.sleep(2000);
			});
			browser.sleep(3000).then(function() {
				bubbleGroups.count().then(function(currentCount) {
					expect(currentCount).toBeGreaterThan(initialCount);
				});
			})
		});
	});

	it('Traffic Analysis Access to Shanky Diagram', function() {
		console.log('\nGroup Traffic Analysis: Check if able to access Shanky Diagram.');

		let bubbleGroups = element.all(by.css('#underNodeLayer circle[fill="#272f3e"]'));
		let controlDiagramButtons = element.all(by.css('#CSTDiagramCtrl g > g > rect:nth-child(7)'));

		bubbleGroups.get(0).click();
		browser.sleep(4000);
		controlDiagramButtons.get(0).click();
		browser.sleep(6000);

		expect(element(by.id('SankeyDiagram')).isPresent()).toBe(true);
	});

	it('Traffic Analysis from Shanky Diagram Access to Flow Vector Table', function() {
		console.log('\nGroup Traffic Analysis: Check if able to access Flow Vector Table.');

		let shankyButtons = element.all(by.css('#SankeyDiagram > text+rect'));

		shankyButtons.get(0).click();
		browser.sleep(6000);
		expect(element(by.id('VectorsListPanel(Classification Group Flow List)0:0')).isPresent()).toBe(true);
		element(by.css('[id^="VectorsListPanel"] g:nth-child(5) > text')).getText().then(function(text) {
			console.log('Expect there to be more than 0 flows');
			expect(parseInt(text, 10)).toBeGreaterThan(0);
		});
	});
});