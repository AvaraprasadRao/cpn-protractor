//test_menu_deviceAdList.js

describe('Device AD List Menu Suite', function() {

	//Device AD List Page
	it('Check if there are AD Users present', function() {
		console.log('\n\nDevice AD List Menu: Check if there are Users present.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(22)')).click();
		browser.sleep(2000);

		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('AD User List', sideMenuClicks, sideMenuTexts);

		browser.sleep(8000);
		var adUsers = element(by.css('[id^="UserBrowserPanel"] g:nth-child(5) > text')).getText().then(function(text) {
			var num = text.match(/\d/g).join("");
			console.log('Number of AD Users found: '+num);
			return parseInt(num, 10);
		});
		expect(adUsers).toBeGreaterThan(0);
	});

	it('Check a few cards if information is correct', function() {
		console.log('\nDevice AD List Menu: Click a few cards and match key informtation.')
		let cards = element.all(by.css('#topCards g[id]'));
		cards.each(function(card, index) {
			if(index < 8) {
				presentElementText(cards.get(index).all(by.css('text')).get(0), '', function() {
					cards.get(index).all(by.css('text')).get(0).getText().then(function(user) {
						cards.get(index).all(by.css('text')).get(1).getText().then(function(job) {
							cards.get(index).all(by.css('image')).getAttribute('href').then(function(ref) {
								console.log('\nClicking AD User #'+(index+1));
								cards.get(index).all(by.css('g text~rect:last-of-type')).get(0).click();
								browser.sleep(8000);

								let userInformation = element.all(by.css('[id^=UserDetailPanel] g:nth-child(2) > svg > text'));
								findDetail(userInformation, user, 'Name');
								findDetail(userInformation, job, 'Job Title');
								expect(element(by.css('[id^=UserDetailPanel] image')).getAttribute('href')).toContain(ref);

								element(by.css('[id^=UserDetailPanel] g:nth-child(6) > rect:nth-child(7)')).click();
								browser.sleep(3000);
							});
						});
					});
				});
			}
		})
	});
});