//test_04_systemPageConfigCheck.js

describe('System Tab Config Check Suite', function() {
	
	it('1. Config: Network Equipment IP Adresseses', function () {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(5000).then(function() {
			console.log('\n\nSystem Config Check: Checking Config Information:');
			console.log('\nSystem Config Check: Network Equipment IP Adresseses.');
		});
		expect(browser.getCurrentUrl()).toMatch('systempage');
		expect(element(by.id('NetworkConfigPanel(Equipment Configuration)0:0')).isPresent()).toBe(true);

		let ipAddresses = element.all(by.css('*[id^=ipaddress]'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));

		//1. Network Equipment IP Adresseses
		browser.sleep(3000);
		expect(ipAddresses.get(0).getAttribute('value')).toBe('10.200.201.2');
		browser.sleep(1000);
		scrollAndClick(nextButtons.get(0));
	});

	it('2. Network Equipment CLI Credentials', function () {
		let usernames = element.all(by.css('*[id^=username]'));
		let passwords = element.all(by.css('*[id^="password"]'));
		let enpasswords = element.all(by.css('*[id^=enpassword]'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		console.log('\nSystem Config Check: Network Equipment CLI Credentials.');

		//2. Network Equipment CLI Credentials
		expect(usernames.get(0).getAttribute('value')).toBe('cisco');
		expect(passwords.get(0).getAttribute('value')).toBe('cisco');
		expect(enpasswords.get(0).getAttribute('value')).toBe('cisco');
		browser.sleep(1000);
		
		expect(usernames.get(1).getAttribute('value')).toBe('admin');
		expect(passwords.get(1).getAttribute('value')).toBe('admins');
		expect(enpasswords.get(1).getAttribute('value')).toBe('enable');
		browser.sleep(1000);

		expect(usernames.get(2).getAttribute('value')).toBe('manager');
		expect(passwords.get(2).getAttribute('value')).toBe('cloudpost');
		expect(enpasswords.get(2).getAttribute('value')).toBe('cloudpost');
		browser.sleep(1000);
		scrollAndClick(nextButtons.get(1));
	});

	it('3. Network Equipment SNMP Credentials', function () {
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		console.log('\nSystem Config Check: Network Equipment SNMP Credentials');

		//3. Network Equipment SNMP Credentials
		browser.sleep(2000);
		nextButtons.get(2).click();
	});

	it('4. Configure Vulnerability Scan', function () {
		let ipAddresses = element.all(by.css('*[id^=ipaddress]'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		let dropdowns = element.all(by.css('*[id*=frequencyInput]'));
		console.log('\nSystem Config Check: Configure Vulnerability Scan.');

		//4. Configure Vulnerability Scan
		browser.sleep(2000);
		expect(ipAddresses.get(1).getAttribute('value')).toBe('10.200.205.0/24');
		browser.sleep(1000);
		expect(dropdowns.all(by.css('text')).get(0).getText()).toEqual('None');
		browser.sleep(1000);
		scrollAndClick(nextButtons.get(3));
	});

	it('5. Configure Password Scan.', function () {
		let ipAddresses = element.all(by.css('*[id^=ipaddress]'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		let dropdowns = element.all(by.css('*[id*=frequencyInput]'));
		console.log('\nSystem Config Check: Configure Password Scan.');

		//5. Configure Password Scan
		browser.sleep(2000);
		expect(ipAddresses.get(3).getAttribute('value')).toBe('10.200.205.0/24');
		browser.sleep(1000);
		expect(dropdowns.all(by.css('text')).get(1).getText()).toEqual('None');
		scrollAndClick(nextButtons.get(4));
	});

	it('6. Enable or Disable Probes', function () {
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		console.log('\nSystem Config Check: Enable or Disable Probes.');

		//6. Enable or Disable Probes
		browser.sleep(2000);
		expect(element.all(by.css('g:nth-child(6) > g:nth-child(3) > svg > g > svg > g > g > rect:nth-child(1)[fill="#4bb3ad"]')).count()).toBe(4);
		scrollAndClick(nextButtons.get(5));
	});

	it('7. Configure CloudPost Equipment', function () {
		console.log('\nSystem Config Check: Configure CloudPost Equipment');
 
		//7. Configure CloudPost Equipment
		browser.sleep(2000);
	});
});