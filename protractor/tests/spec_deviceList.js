//test_menu_deviceList.js

describe('Device List Menu Suite', function() {

	//Device List Page
	it('Check Total Devices are available in Device List', function() {
		console.log('\n\nDevice List Menu: Check Total Devices are Present in Table.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#devicesInfoSection text[fill="#f65139"]')).getText().then(function(total) {
			console.log("Total Devices Number should be: "+total);
			element(by.css('#gnTopMenu > rect:nth-child(22)')).click();
			browser.sleep(2000);
			let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
			let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
			selectSideMenu('Device List', sideMenuClicks, sideMenuTexts);
			browser.sleep(2000);
			element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
				expect(devices).toContain(total);
				expect(devices).toContain('Total');
			});
		});
	});

	it('Check Device List Sorting', function() {
		console.log('\nDevice List Menu: Sorting Functionality Check.');
		browser.sleep(2000);

		element.all(by.css('#topCards > g svg > g > path+rect')).get(0).click();
		browser.sleep(2000);
		var firstMacAddress = element.all(by.css('#topCards svg > path~rect:nth-child(3)[x="0"]~svg:nth-of-type(2) > text')).get(0).getText().then(function(mac) {
			console.log("Sorting from First Mac Address - Top Row Address: "+mac)
			return mac;
		});

		element.all(by.css('#topCards > g svg > g > path+rect')).get(1).click();
		browser.sleep(2000);
		var lastMacAddress = element.all(by.css('#topCards svg > path~rect:nth-child(3)[x="0"]~svg:nth-of-type(2) > text')).get(0).getText().then(function(mac) {
			console.log("Sorting from Last Mac Address - Top Row Address: "+mac);
			return mac;
		});

		expect(firstMacAddress).not.toBe(lastMacAddress);
	});

	it('Check Device List Search Bar', function() {
		console.log('\nDevice List Menu: Search Bar Functionality Check.');
		browser.sleep(2000);
		element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).sendKeys('AXIS');
		browser.sleep(2000);
		element.all(by.css('#topCards svg > path~rect:nth-child(3)[x="0"]')).count().then(function(count) {
			console.log('Total AXIS Device Number found: '+count);
			expect(count).toBeGreaterThan(0);
			if(count < 0) {
				console.log('There are No AXIS Devices!');
			}
			else {	
				let deviceNames = element.all(by.css('#topCards svg > path~rect:nth-child(3)[x="0"]~svg:nth-of-type(4) > text'));
				deviceNames.each(function(device, index) {
					device.getText().then(function(name) {
						console.log('Checking Device No. '+(index+1)+' device name \"'+name+'\" is AXIS');
						expect(name).toMatch(/axis-[\w\d]+/i);
					});
				});
				//element(by.css('[id^="DeviceBrowserPanel"] g:nth-child(16) > rect:nth-child(7)')).click();
				browser.sleep(2000);
			}
		});
	});

	it('Check Device List Info Clicking, Security Panel Icon', function() {
		console.log('\nDevice List Menu: Click info sections from table.');
		browser.sleep(2000);
		//Clicking Security Panel Icon

		presentElementClick(element.all(by.css('#topCards svg > rect:nth-child(15)[fill="white"]')).first(),
			'\nSecurity Panel Icon not present on Current Page',
			function() {
				console.log('Clicking Security Panel Icon');
				browser.sleep(2000);
				var macAddress = element(by.css('#topCards svg > path~rect:nth-child(3)[x="0"]~svg:nth-of-type(2) > text[fill="#4a90e2"]'));
				var ipAddress = element(by.css('#topCards svg > path~rect:nth-child(3)[x="0"]~svg:nth-of-type(3) > text[fill="#4a90e2"]'))
				var deviceName = element(by.css('#topCards svg > path~rect:nth-child(3)[x="0"]~svg:nth-of-type(4) > text[fill="#4a90e2"]'));
				macAddress.getText().then(function(mac) {
					ipAddress.getText().then(function(ip) {
						deviceName.getText().then(function(name) {
							if(ip !== 'N/A') {
								expect(element(by.css('[id^="DeviceDetailPanel"] > text:nth-child(1)')).getText()).toContain(ip);
							}
							if(name !== mac) {
								expect(element(by.css('[id^="DeviceDetailPanel"] > text:nth-child(2)')).getText()).toContain(mac);
							}
							else {
								expect(element(by.css('[id^="DeviceDetailPanel"] > text:nth-child(1)')).getText()).toContain(mac);
							}
							expect(element.all(by.css('[id^=DeviceAlarmSection]')).count()).toBeGreaterThan(0);
							scrollToView(element(by.css('[id^="DeviceBrowserPanel(List of Devices)0"]')));
							browser.sleep(4000);
						});
					});
				});
			}
		);
	});

	it('Check Device List Info Clicking, Flow Genome Icon', function() {
		browser.sleep(2000);
		//Clicking Flow Genome Icon
		presentElementClick(element.all(by.css('#topCards svg > rect:nth-child(17)[fill="white"]')).first(),
			'\nFlow Genome Icon not present on Current Page',
			function() {
				console.log('\nClicking Flow Genome Icon');
				browser.sleep(5000);
				var macAddress = element(by.css('#topCards svg > path~rect:nth-child(3)[x="0"]~svg:nth-of-type(2) > text[fill="#4a90e2"]'));
				var ipAddress = element(by.css('#topCards svg > path~rect:nth-child(3)[x="0"]~svg:nth-of-type(3) > text[fill="#4a90e2"]'))
				var deviceName = element(by.css('#topCards svg > path~rect:nth-child(3)[x="0"]~svg:nth-of-type(4) > text[fill="#4a90e2"]'));
				macAddress.getText().then(function(mac) {
					ipAddress.getText().then(function(ip) {
						deviceName.getText().then(function(name) {
							if(ip !== 'N/A') {
								expect(element(by.css('[id^="DeviceDetailPanel"] > text:nth-child(1)')).getText()).toContain(ip);
							}
							if(name !== mac) {
								expect(element(by.css('[id^="DeviceDetailPanel"] > text:nth-child(2)')).getText()).toContain(mac);
							}
							else {
								expect(element(by.css('[id^="DeviceDetailPanel"] > text:nth-child(1)')).getText()).toContain(mac);
							}
							expect(element(by.css('[id="SankeyDiagram"]')).isPresent()).toBe(true);
							scrollToView(element(by.css('[id^="DeviceBrowserPanel(List of Devices)0"]')));
							browser.sleep(4000);
						});
					});
				});
			}
		);
	});

	it('Check Device List Info Clicking, Network Stats Icon', function() {
		browser.sleep(2000);
		//Clicking Network Stats Icon
		presentElementClick(element.all(by.css('#topCards svg > rect:nth-child(23)[fill="white"]')).first(),
			'\nNetwork Stats Icon not present on Current Page',
			function() {
				console.log('\nClicking Network Stats Icon');
				browser.sleep(2000);
				var macAddress = element(by.css('#topCards svg > path~rect:nth-child(3)[x="0"]~svg:nth-of-type(2) > text[fill="#4a90e2"]'));
				var ipAddress = element(by.css('#topCards svg > path~rect:nth-child(3)[x="0"]~svg:nth-of-type(3) > text[fill="#4a90e2"]'))
				var deviceName = element(by.css('#topCards svg > path~rect:nth-child(3)[x="0"]~svg:nth-of-type(4) > text[fill="#4a90e2"]'));
				macAddress.getText().then(function(mac) {
					ipAddress.getText().then(function(ip) {
						deviceName.getText().then(function(name) {
							if(ip !== 'N/A') {
								expect(element(by.css('[id^="DeviceDetailPanel"] > text:nth-child(1)')).getText()).toContain(ip);
							}
							if(name !== mac) {
								expect(element(by.css('[id^="DeviceDetailPanel"] > text:nth-child(2)')).getText()).toContain(mac);
							}
							else {
								expect(element(by.css('[id^="DeviceDetailPanel"] > text:nth-child(1)')).getText()).toContain(mac);
							}
							expect(element(by.css('[id="NTWK"]')).isPresent()).toBe(true);
							scrollToView(element(by.css('[id^="DeviceBrowserPanel(List of Devices)0"]')));
							browser.sleep(2000);
						});
					});
				});
			}
		);
	});

	it('Check Device List Info Clicking, Application Usage Icon', function() {
		browser.sleep(2000);
		//Clicking Application Usage Icon
		presentElementClick(element.all(by.css('#topCards svg > rect:nth-child(37)[fill="white"]')).first(),
			'\nApplication Usage Icon not present on Current Page',
			function() {
				console.log('\nClicking Application Usage Icon');
				browser.sleep(4000);
				expect(browser.getCurrentUrl()).toMatch('apppage');
				expect(element(by.css('[id="AppSunburstDiagram"]')).isPresent()).toBe(true);
				element(by.css('#gnTopMenu > rect:nth-child(21)')).click();
				browser.sleep(2000);
			}
		);
	});

	it('Change in Criticality Check', function() {
		console.log('\nDevice List Menu: Change the Profile of the first device and check if it remains');
		browser.sleep(2000);
		element.all(by.css('#topCards g[id] > svg > svg:nth-child(7) > text')).get(0).getText().then(function(name) {
			browser.actions().mouseMove(element.all(by.css('#topCards svg > svg:nth-child(4) > text')).get(0)).mouseMove({x: -5, y: 0}).click().perform();
			browser.sleep(4000);
			scrollToView(element(by.id('MetaInfoSection')));
			element(by.css('#MetaInfoSection g:nth-child(12) > g > text')).getText().then(function(oldLevel) {
				element(by.css('#MetaInfoSection g:nth-child(12) > g > rect:nth-child(4)')).click();
				browser.sleep(3000);
				var levelTexts = element.all(by.css('#modalLayerGElement svg g > g > text'));
				var levelClicks = element.all(by.css('#modalLayerGElement svg g > g > rect:nth-child(4)'));
				levelClicks.filter(function(elem, index){
					return levelTexts.get(index).getText().then(function(text) {
						return text !== oldLevel;
					});
				}).first().click();
				element(by.css('#MetaInfoSection svg>g:last-of-type rect:last-of-type')).click();
				browser.sleep(7000);

				element(by.css('#MetaInfoSection g:nth-child(12) > g > text')).getText().then(function(newLevel) {
					scrollToView(element(by.css('[id^="DeviceDetailPanel"][id$="0:0"]')));
					let deviceClassification = element.all(by.css('[id^="DeviceDetailPanel"] g:nth-child(2) > svg > text'));
					var num = oldLevel.match(/\d/g).join("");
					console.log(oldLevel+' should be changed to '+newLevel);
					findDetail(deviceClassification, ('^((?!'+num+').)*$'), 'Criticality');
					browser.sleep(5000);

					//changing criticality back
					loginUser(usrnm_keys, pswrd_keys);
					browser.sleep(deviceLoadDelay);
					element(by.css('#gnTopMenu > rect:nth-child(22)')).click();
					browser.sleep(2000);
					let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
					let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
					selectSideMenu('Device List', sideMenuClicks, sideMenuTexts);
					browser.sleep(2000);
					element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).clear();
					element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).sendKeys(name);
					browser.sleep(4000);
					browser.actions().mouseMove(element.all(by.css('#topCards svg > svg:nth-child(4) > text')).get(0)).mouseMove({x: -5, y: 0}).click().perform();
					browser.sleep(3000);
					expect(element(by.css('#MetaInfoSection g:nth-child(12) > g > text')).getText()).toBe(newLevel);

					scrollToView(element(by.id('MetaInfoSection')));
					element(by.css('#MetaInfoSection g:nth-child(12) > g > rect:nth-child(4)')).click();
					browser.sleep(3000).then(function(){console.log('\nChanging criticality back to '+oldLevel);});
					levelTexts = element.all(by.css('#modalLayerGElement svg g > g > text'));
					levelClicks = element.all(by.css('#modalLayerGElement svg g > g > rect:nth-child(4)'));
					levelClicks.filter(function(elem, index) {
						return levelTexts.get(index).getText().then(function(text) {
							return text == oldLevel;
						});
					}).first().click();
					element(by.css('#MetaInfoSection svg>g:last-of-type rect:last-of-type')).click();
					browser.sleep(7000);
					scrollToView(element(by.css('[id^="DeviceDetailPanel"][id$="0:0"]')));

					deviceClassification = element.all(by.css('[id^="DeviceDetailPanel"] g:nth-child(2) > svg > text'));
					findDetail(deviceClassification, oldLevel, 'Criticality');
					browser.sleep(5000);
				});
			});
		});
	});

	it('Rename Device Check', function() {
		console.log('\nDevice List Menu: Change the name of the first device and check if it remains');
		browser.sleep(2000);
		element.all(by.css('#topCards g[id] > svg > svg:nth-child(7) > text')).get(0).getText().then(function(oldName) {
			element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).clear();
			browser.sleep(2000);
			element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).sendKeys(oldName);
			browser.sleep(4000);

			element(by.css('[id^="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(filterDevice) {
				var num = filterDevice.match(/\d/g).join("");
				expect(parseInt(num, 10)).toBe(1);
			});

			browser.actions().mouseMove(element.all(by.css('#topCards svg > svg:nth-child(4) > text')).get(0)).mouseMove({x: -5, y: 0}).click().perform();
			browser.sleep(4000);
			console.log('Changing Device Name from \''+oldName+'\' to \'test_'+oldName+'\'')
			scrollToView(element(by.id('MetaDataEditSectionLabel')));
			element(by.id('MetaDataEditSectionLabel')).clear();
			element(by.id('MetaDataEditSectionLabel')).sendKeys('test_'+oldName);
			browser.sleep(3000);
			element(by.css('#MetaInfoSection svg>g:last-of-type rect:last-of-type')).click();
			browser.sleep(3000);
			scrollToView(element(by.css('[id^="DeviceBrowserPanel(List of Devices)0"]')));
			browser.sleep(3000);
			expect(element.all(by.css('#topCards g[id] > svg > svg:nth-child(7) > text')).get(0).getText()).toBe('test_'+oldName);

			loginUser(usrnm_keys, pswrd_keys);
			browser.sleep(deviceLoadDelay);
			element(by.css('#gnTopMenu > rect:nth-child(22)')).click();
			browser.sleep(2000);
			let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
			let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
			selectSideMenu('Device List', sideMenuClicks, sideMenuTexts);
			browser.sleep(2000);
			element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).clear();
			element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).sendKeys('test_'+oldName);
			browser.sleep(4000);

			element(by.css('[id^="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(filterDevice) {
				var num = filterDevice.match(/\d/g).join("");
				//if no device found with new name search
				if(parseInt(num, 10) == 0) {
					console.log('New name not searchable; possibly not properly saved');
					element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).clear();
					element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).sendKeys(oldName);
					browser.sleep(4000);
					element.all(by.css('#topCards g[id] > svg > svg:nth-child(7) > text')).get(0).getText().then(function(name) {
						browser.actions().mouseMove(element.all(by.css('#topCards svg > svg:nth-child(4) > text')).get(0)).mouseMove({x: -5, y: 0}).click().perform();
						browser.sleep(4000);
						scrollToView(element(by.id('MetaDataEditSectionLabel')));
						element(by.id('MetaDataEditSectionLabel')).clear();
						element(by.id('MetaDataEditSectionLabel')).sendKeys(oldName);
						browser.sleep(3000);
						element(by.css('#MetaInfoSection svg>g:last-of-type rect:last-of-type')).click();
						browser.sleep(3000);
						scrollToView(element(by.css('[id^="DeviceBrowserPanel(List of Devices)0"]')));
						browser.sleep(3000);
						expect(element.all(by.css('#topCards g[id] > svg > svg:nth-child(7) > text')).get(0).getText()).toBe(oldName);
						fail('\'test_'+oldName+'\' is not saved, actual is \''+name+'\'');
					});
				}
				//if device found with new name search
				else if(parseInt(num, 10) == 1) {
					element.all(by.css('#topCards g[id] > svg > svg:nth-child(7) > text')).get(0).getText().then(function(name) {
						if(name == ('test_'+oldName)) {
							console.log('Name has been successfully saved/changed, now changing back')
							browser.actions().mouseMove(element.all(by.css('#topCards svg > svg:nth-child(4) > text')).get(0)).mouseMove({x: -5, y: 0}).click().perform();
							browser.sleep(4000);
							scrollToView(element(by.id('MetaDataEditSectionLabel')));
							element(by.id('MetaDataEditSectionLabel')).clear();
							element(by.id('MetaDataEditSectionLabel')).sendKeys(oldName);
							browser.sleep(3000);
							element(by.css('#MetaInfoSection svg>g:last-of-type rect:last-of-type')).click();
							browser.sleep(3000);
							scrollToView(element(by.css('[id^="DeviceBrowserPanel(List of Devices)0"]')));
							browser.sleep(3000);
							expect(element.all(by.css('#topCards g[id] > svg > svg:nth-child(7) > text')).get(0).getText()).toBe(oldName);
						}
						else {
							console.log('Filter picks up changed name, but table shows wrong device name');
							browser.actions().mouseMove(element.all(by.css('#topCards svg > svg:nth-child(4) > text')).get(0)).mouseMove({x: -5, y: 0}).click().perform();
							browser.sleep(4000);
							scrollToView(element(by.id('MetaDataEditSectionLabel')));
							element(by.id('MetaDataEditSectionLabel')).clear();
							element(by.id('MetaDataEditSectionLabel')).sendKeys(oldName);
							browser.sleep(3000);
							element(by.css('#MetaInfoSection svg>g:last-of-type rect:last-of-type')).click();
							browser.sleep(3000);
							scrollToView(element(by.css('[id^="DeviceBrowserPanel(List of Devices)0"]')));
							browser.sleep(3000);
							expect(element.all(by.css('#topCards g[id] > svg > svg:nth-child(7) > text')).get(0).getText()).toBe(oldName);
							fail('\'test_'+oldName+'\' is not saved, actual is \''+name+'\'');
						}
					});
				}
			});
		});
	});
});