//test_menu_network.js
describe('Network Tab Suite', function() {

	it('Network Table should have some devices', function() {
		console.log('\n\nNetwork Menu: Check if Network Table is populated.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click().then(function() {
			browser.sleep(5000);
			console.log('Checking if a Seed IP has been entered.');
			let ipAddresses = element.all(by.css('*[id^=ipaddress]'));
			ipAddresses.get(0).getAttribute('value').then(function(ip) {
				if(ip.match(/\d+\.\d+\.\d+\.\d+/g) != null) {
					console.log('Seed IP found: '+ip);
					element(by.css('#gnTopMenu > rect:nth-child(29)')).click();
					browser.sleep(3000);
					expect(element(by.css('[id^="NetDeviceBrowserPanel"]')).isPresent()).toBe(true);
					var netDevices = element(by.css('[id^="NetDeviceBrowserPanel"] g:nth-child(4) > text')).getText().then(function(text) {
						var num = text.match(/\d/g);
						num = num.join("");
						console.log('Network Devices found: '+num);
						return parseInt(num, 10);
					});
					expect(netDevices).toBeGreaterThan(0);
				}
				else {
					console.log('Seed IP not found');
					element(by.css('#gnTopMenu > rect:nth-child(29)')).click();
					browser.sleep(3000);
				}
			})
		});
	});

	it('Look for Apply Action button and click it', function() {
		console.log('\nNetwork Menu: Check if Action Button is present.');
		browser.sleep(3000);
		let actionButton = element(by.css('[id^="NetDeviceBrowserPanel"] g:nth-child(7) > rect:nth-child(7)'));
		presentElement(actionButton, 'Action Button not present', function() {
			actionButton.click();
			browser.sleep(3000);
			expect(element(by.id('bulkActions')).isPresent()).toBe(true);
		}, function() {fail('no Action Button is clickable');})
	});
});