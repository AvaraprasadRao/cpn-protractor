//test_01_login.js

describe('Login Suite', function() {
	//login with admin username and password, verify if current browser matches homepage
	it('Check if positive username and password entered', function() {
		console.log('\n\nLogin: Checking if the correct credentials allow login.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		expect(browser.getCurrentUrl()).toMatch('systemsummary');
	});

	//click menu btn and then logout btn, and verify if current browser matches homepage and alert is displayed
	it('Check if able to logout', function() {
		console.log('\nLogout: Now checking if able to logout.');
		element(by.id('AccountMenu')).click();
		browser.sleep(3000);
		element(by.css('image+text+rect[x="0"][y="107"]')).click();
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toMatch('login');
	});

	// //login with garbage credentials, verify if current browser matches loginpage and alert is displayed
	it('Check if negative username or password entered', function() {
		console.log('\nLogin: Checking if alert pops up when credentials are incorrect.');
		loginUser(usrnm_neg, pswrd_neg);
		browser.sleep(deviceLoadDelay);
		expect(element(by.css('image+text[x="34"][y="40"]')).isDisplayed());
		expect(element(by.css('image+text[x="34"][y="40"]')).getText()).toMatch('INVALID LOGIN. TRY AGAIN');
		expect(browser.getCurrentUrl()).toMatch('login');
	});
});