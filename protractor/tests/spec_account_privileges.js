//test_account_privileges.js

describe('Account Privileges Suite', function() {

	function createUser(username, password, role) {
		element.all(by.css('#topCards > svg > g > g')).count().then(function(initial){
			console.log('Initial Accounts Number: '+initial);
			element(by.css('[id^="LocalAdminsBrowserPanel"] > g:nth-child(4) > g:nth-child(5) > rect:nth-child(7)')).click();

			element(by.id('username')).sendKeys(username);
			browser.sleep(2000);
			element(by.id('password')).sendKeys(password);
			browser.sleep(2000);
			//turn off 2FA Auth Button
			presentElement(element(by.css('[id^="AdminAccountDetail"] g > g:nth-child(10) > rect:nth-child(2)')), '', function() {
				element(by.css('[id^="AdminAccountDetail"] g > g:nth-child(10) > rect:nth-child(2)')).getAttribute('fill').then(function(color) {
					if (color == '#4bb3ad') {
						element(by.css('[id^="AdminAccountDetail"] g > g:nth-child(10) > rect:nth-child(4)')).click();
						browser.sleep(1000);
						expect(element(by.css('[id^="AdminAccountDetail"] > g > svg > g > g:nth-child(10) > rect:nth-child(2)')).getAttribute('fill')).toBe('transparent');
					}
				});
			});
			element(by.id('firstname')).sendKeys('John');
			browser.sleep(1000);
			element(by.id('lastname')).sendKeys('Doe');
			browser.sleep(1000);
			//Check Role
			presentElement(element(by.css('#dropDownContentG-user-roles > text')), '', function() {
				element(by.css('#dropDownContentG-user-roles > text')).getText().then(function(text) {
					if(!(text.includes(role))) {
						element(by.css('#dropDownContentG-user-roles > rect:nth-child(8)')).click();
						browser.sleep(2000);
						let dropdownOptions = element.all(by.css('#infoLayerGElement > g > text'));
						let dropdownClick = element.all(by.css('#infoLayerGElement > g > rect'));
						dropdownClick.filter(function(elem, index) {
							return dropdownOptions.get(index).getText().then(function(opt) {
								return opt.includes(role);
							});
						}).first().click();
						browser.sleep(2000);
						expect(element(by.css('#dropDownContentG-user-roles > text')).getText()).toMatch(role);
					}
				})
			});
			//turn off 2FA Dropdown
			presentElement(element(by.css('#dropDownContentG-mfa-choices > text')), '', function() {
				element(by.css('#dropDownContentG-mfa-choices > text')).getText().then(function(text) {
					if(!(text.includes('None'))) {
						element(by.css('#dropDownContentG-mfa-choices > rect:nth-child(8)')).click();
						browser.sleep(2000);
						let dropdownOptions = element.all(by.css('#infoLayerGElement > g > text'));
						let dropdownClick = element.all(by.css('#infoLayerGElement > g > rect'));
						dropdownClick.filter(function(elem, index) {
							return dropdownOptions.get(index).getText().then(function(opt) {
								return opt.includes('None');
							});
						}).first().click();
						browser.sleep(2000);
						expect(element(by.css('#dropDownContentG-mfa-choices > text')).getText()).toMatch('None');
					}
				})
			});
			element(by.id('email')).sendKeys('placeholder@cloudpostnetworks.com');
			browser.sleep(4000);
			element(by.id('SaveEventRectangle')).click();
			browser.sleep(4000);
			element.all(by.css('#topCards > svg > g > g')).count().then(function(final) {
				console.log('Current Accounts Number: '+final);
				expect(final).toBe(initial+1);
				browser.sleep(4000);
			});
		});
	}

	function deleteUser(username, password) {
		element.all(by.css('#topCards > svg > g > g')).count().then(function(initial){
			console.log('Initial Accounts Number: '+initial);
			browser.sleep(2000);
			let usernames = element.all(by.css('#topCards > svg  svg:nth-child(5) > text'));
			let deleteButtons = element.all(by.css('#topCards > svg > g rect:last-child'));
			//let deleteButtonsOld = element.all(by.css('#topCards > svg > g > g > svg > rect:nth-child(10)'));

			deleteButtons.filter(function(elem, index) {
				return usernames.get(index).getText().then(function(text) {
					return text === username;
				});
			}).first().click();
			browser.sleep(2000);

			expect(element(by.css('#modalLayerGElement g:nth-child(5) text:nth-child(2)')).getText()).toMatch(username);

			element(by.css('#DeleteEventRectangle')).click().then(function() {
				browser.sleep(5000);
				element.all(by.css('#topCards > svg > g > g')).count().then(function(final) {
					console.log('Current Accounts Number: '+final);
					expect(final).toBe(initial-1);
				});
			});
		});
	}

	function changeRole(username, password, role) {
		let usernames = element.all(by.css('#topCards > svg  svg:nth-child(5) > text'));
		let selectAccount = element.all(by.css('#topCards svg > rect:nth-child(3)[fill]'));

		selectAccount.filter(function(elem, index) {
			return usernames.get(index).getText().then(function(text) {
				return text === username;
			});
		}).first().click();
		browser.sleep(4000);

		expect(element(by.id('username')).getAttribute('value')).toBe(username);

		element(by.id('password')).clear();
		element(by.id('password')).sendKeys(password);
		browser.sleep(2000);
		presentElement(element(by.css('#dropDownContentG-user-roles > text')), '', function() {
			element(by.css('#dropDownContentG-user-roles > text')).getText().then(function(text) {
				if(!(text.includes(role))) {
					console.log('Changing '+username+' from '+text+' to '+role);
					element(by.css('#dropDownContentG-user-roles > rect:nth-child(8)')).click();
					browser.sleep(2000);
					let dropdownOptions = element.all(by.css('#infoLayerGElement > g > text'));
					let dropdownClick = element.all(by.css('#infoLayerGElement > g > rect'));
					dropdownClick.filter(function(elem, index) {
						return dropdownOptions.get(index).getText().then(function(opt) {
							return opt.includes(role);
						});
					}).first().click();
					browser.sleep(2000);
					expect(element(by.css('#dropDownContentG-user-roles > text')).getText()).toMatch(role);
				}
			})
		});
		browser.sleep(4000);
		element(by.id('SaveEventRectangle')).click();
		browser.sleep(4000);
	}

	function checkTabs() {
		element(by.css('#gnTopMenu > rect:nth-child(14)')).click().then(function() {console.log('Checking Group Tab.');});
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toMatch('grouppage');
		expect(element(by.id('GroupBrowserPanel(Group Browser)0:0')).isPresent()).toBe(true);

		element(by.css('#gnTopMenu > rect:nth-child(18)')).click().then(function() {console.log('Checking Profile Tab.');});
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toMatch('profilepage');
		expect(element(by.id('ProfileBrowserPanel(Classification Profiles)0:0')).isPresent()).toBe(true);

		element(by.css('#gnTopMenu > rect:nth-child(22)')).click().then(function() {console.log('Checking Device Tab.')});
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toMatch('devicepage');
		expect(element(by.id('DeviceChartPanel(Device Summary)0:0')).isPresent()).toBe(true);

		element(by.css('#gnTopMenu > rect:nth-child(26)')).click().then(function() {console.log('Checking Application Tab.');});
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toMatch('apppage');
		expect(element(by.id('ApplicationSummaryPanel(Device Application Usage Summary)0:0')).isPresent()).toBe(true);

		element(by.css('#gnTopMenu > rect:nth-child(30)')).click().then(function() {console.log('Checking Network Tab.');});
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toMatch('networkpage');
		expect(element(by.id('NetDeviceBrowserPanel(Network Devices)0:0')).isPresent()).toBe(true);

		element(by.css('#gnTopMenu > rect:nth-child(34)')).click().then(function() {console.log('Checking System Tab.');});
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toMatch('systempage');
		expect(element(by.id('NetworkConfigPanel(Equipment Configuration)0:0')).isPresent()).toBe(true);

		element(by.css('#gnTopMenu > rect:nth-child(9)')).click().then(function() {console.log('Checking Dashboard Tab.');});
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toMatch('systemsummary');
		expect(element(by.id('ExecutiveSummaryPanel0:0')).isPresent()).toBe(true);
	}

	//User Privilege Check
	it('General User Creation', function() {
		console.log('\n\nAccount Privileges: Create a new User Account.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		//element(by.css('g > g:nth-child(9) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('User Accounts', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);
		createUser('test_user', 'user_password', 'User');
	});

	it('Login General User', function() {
		console.log('\nAccount Privileges: Login and check reading privileges of new User Account.');
		loginUser('test_user', 'user_password');
		browser.sleep(deviceLoadDelay);
		checkTabs();
	});

	it('Check if General User does not have privilege to change Seed IP', function() {
		console.log('\nAccount Privileges: Check config privileges of new User Account.');
		browser.sleep(7000);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);

		let ipAddresses = element.all(by.css('*[id^=ipaddress]'));
		expect(ipAddresses.get(0).getAttribute('disabled')).toBe('true');
		expect(ipAddresses.get(0).isEnabled()).toBe(false);
	});

	it('Change Credentials and check if new password allows login for new User', function() {
		console.log('\nAccount Privileges: Check if allowed to change credentials of new User Account.');
		loginUser('test_user', 'user_password');
		browser.sleep(deviceLoadDelay);
		element(by.id('AccountMenu')).click();
		browser.sleep(2000);
		element(by.id('ChangeCredentialsMenuItem')).click();
		browser.sleep(2000);
		element(by.id('old-password')).sendKeys('user_password');
		element(by.id('new-password')).sendKeys('new_user_password');
		element(by.id('confirm-password')).sendKeys('new_user_password');
		browser.sleep(2000);
		element(by.id('SaveEventRectangle')).click();
		browser.sleep(4000).then(function(){console.log('Logging in with changed password')});
		loginUser('test_user', 'new_user_password');
		browser.sleep(deviceLoadDelay);
		expect(browser.getCurrentUrl()).toMatch('systemsummary');
	});

	it('Delete General User', function() {
		console.log('\nAccount Privileges: Delete new User Account.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		//element(by.css('g > g:nth-child(9) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('User Accounts', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);
		deleteUser('test_user');
	});

	//Admin Privilege Check
	it('General Admin Creation', function() {
		console.log('\n\nAccount Privileges: Create a new Admin Account.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		//element(by.css('g > g:nth-child(9) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('User Accounts', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);
		createUser('test_admin', 'admin_password', 'Admin');
	});

	it('Login Admin User', function() {
		console.log('\nAccount Privileges: Login and check reading privileges of new Admin Account.');
		loginUser('test_admin', 'admin_password');
		browser.sleep(deviceLoadDelay);
		checkTabs();		
	});

	it('Check if Admin User has privilege to change Seed IP', function() {
		console.log('\nAccount Privileges: Check config privileges of new Admin Account.');
		browser.sleep(7000);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);

		let ipAddresses = element.all(by.css('*[id^=ipaddress]'));
		ipAddresses.get(0).getAttribute('value').then(function(ip) {
			console.log('Current Seed IP: \"'+ip+'\"');
			ipAddresses.get(0).clear();
			ipAddresses.get(0).sendKeys('garbage');
			browser.sleep(3000);
			ipAddresses.get(0).getAttribute('value').then(function(newIp) {
				console.log('Typing New Seed IP: \"'+newIp+'\"');
				expect(newIp).toBe('garbage');
			});
		});
	});

	it('Change Credentials and check if new password allows login for new Admin', function() {
		console.log('\nAccount Privileges: Check if allowed to change credentials of new Admin Account.');
		loginUser('test_admin', 'admin_password');
		browser.sleep(deviceLoadDelay);
		element(by.id('AccountMenu')).click();
		browser.sleep(2000);
		element(by.id('ChangeCredentialsMenuItem')).click();
		browser.sleep(2000);
		element(by.id('old-password')).sendKeys('admin_password');
		element(by.id('new-password')).sendKeys('new_admin_password');
		element(by.id('confirm-password')).sendKeys('new_admin_password');
		browser.sleep(2000);
		element(by.id('SaveEventRectangle')).click();
		browser.sleep(4000).then(function(){console.log('Logging in with changed password')});
		loginUser('test_admin', 'new_admin_password');
		browser.sleep(deviceLoadDelay);
		expect(browser.getCurrentUrl()).toMatch('systemsummary');
	});

	it('Delete Admin User', function() {
		console.log('\nAccount Privileges: Delete new Admin Account.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		//element(by.css('g > g:nth-child(9) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('User Accounts', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);
		deleteUser('test_admin');
	});

	//Admin to User Privilege Check
	it('Create New Account to test changing privileges from admin to user', function() {
		console.log('\n\nAccount Privileges: Create a new Admin Account to test downgrading privileges.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		//element(by.css('g > g:nth-child(9) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('User Accounts', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);
		createUser('change_to_user', 'test_password', 'Admin');
	});

	it('Downgrade to User privilege and check if not able to change Seed IP', function() {
		console.log('\nAccount Privileges: Downgrading account and check config privileges.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		//element(by.css('g > g:nth-child(9) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('User Accounts', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);
		changeRole('change_to_user', 'test_password','User');

		loginUser('change_to_user', 'test_password');
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);

		let ipAddresses = element.all(by.css('*[id^=ipaddress]'));
		expect(ipAddresses.get(0).getAttribute('disabled')).toBe('true');
		expect(ipAddresses.get(0).isEnabled()).toBe(false);
	});

	it('Delete New Account to test changing privileges from admin to user', function() {
		console.log('\nAccount Privileges: Delete newly downgraded account.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		//element(by.css('g > g:nth-child(9) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('User Accounts', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);
		deleteUser('change_to_user');
	});

	//User to Admin Privilege Check
	it('Create New Account to test changing privileges from user to admin', function() {
		console.log('\n\nAccount Privileges: Create a new User Account to test upgrading privileges.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		//element(by.css('g > g:nth-child(9) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('User Accounts', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);
		createUser('change_to_admin', 'test_password', 'User');
	});

	it('Upgrade to Admin privilege and check if able to change Seed IP', function() {
		console.log('\nAccount Privileges: Upgrading account and check config privileges.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		//element(by.css('g > g:nth-child(9) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('User Accounts', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);
		changeRole('change_to_admin', 'test_password','Admin');

		loginUser('change_to_admin', 'test_password');
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);

		let ipAddresses = element.all(by.css('*[id^=ipaddress]'));
		ipAddresses.get(0).getAttribute('value').then(function(ip) {
			console.log('Current Seed IP: \"'+ip+'\"');
			ipAddresses.get(0).clear();
			ipAddresses.get(0).sendKeys('garbage');
			browser.sleep(3000);
			ipAddresses.get(0).getAttribute('value').then(function(newIp) {
				console.log('Typing New Seed IP: \"'+newIp+'\"');
				expect(newIp).toBe('garbage');
			});
		});
	});

	it('Delete New User to Admin Account', function() {
		console.log('\nAccount Privileges: Delete newly upgraded account.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		//element(by.css('g > g:nth-child(9) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('User Accounts', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);
		deleteUser('change_to_admin');
	});

	//bowers privileges
	it('Login bowers Account', function() {
		console.log('\n\nAccount Privileges: Login \"bowers\" Account and check privileges.');
		loginUser(bowers_usrnm_keys, bowers_pswrd_keys);
		browser.sleep(deviceLoadDelay);
		checkTabs();
	});

	it('Check if bowers does not have privilege to change Seed IP', function() {
		console.log('\nAccount Privileges: Check config privileges of \"bowers\" Account.');
		browser.sleep(7000);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);

		let ipAddresses = element.all(by.css('*[id^=ipaddress]'));
		expect(ipAddresses.get(0).getAttribute('disabled')).toBe('true');
		expect(ipAddresses.get(0).isEnabled()).toBe(false);
	});

	//mccarthy privileges
	it('Login mccarthy Account', function() {
		console.log('\n\nAccount Privileges: Login \"mccarthy\" Account and check privileges.');
		loginUser(mccarthy_usrnm_keys, mccarthy_pswrd_keys);
		browser.sleep(deviceLoadDelay);
		checkTabs();
	});

	it('Check if mccarthy does not have privilege to change Seed IP', function() {
		console.log('\nAccount Privileges: Check config privileges of \"mccarthy\" Account.');
		browser.sleep(7000);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);

		let ipAddresses = element.all(by.css('*[id^=ipaddress]'));
		expect(ipAddresses.get(0).getAttribute('disabled')).toBe('true');
		expect(ipAddresses.get(0).isEnabled()).toBe(false);
	});
});