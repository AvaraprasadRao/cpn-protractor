//test_04_device.js

describe('Device Tab Sanity Suite', function() {
	//login with admin username and password, click Group Tab and check if at least one card is viewable
	it('Active Directory Test', function() {
		console.log("\n\nDevice Sanity: Check Active Directory in Device List");
		loginUser(usrnm_keys, pswrd_keys);

		//expect(browser.getCurrentUrl()).toContain('systemsummary');
		browser.sleep(3000);
		element(by.css('#gnTopMenu > rect:nth-child(22)')).click();
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toContain('devicepage');
		element(by.css('g:nth-child(2) > g > g:nth-child(6) > rect:nth-child(4)')).click();
		browser.sleep(3000);
		element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).sendKeys('%hasuser');
		browser.sleep(3000);

		let userDevices = element.all(by.css('svg > path~rect:nth-child(3)[x="0"]'));
		userDevices.count().then(function(count) {
			if(count > 0) {
				userDevices.get(0).click();
				browser.sleep(3000);
				element(by.css('[id*="DeviceDetailPanel"] > g:nth-child(4) > g:nth-child(7) > rect:nth-child(7)')).click();
				browser.sleep(3000);
				element(by.css('[id*="dataSet-2"] > svg > text:nth-child(1)')).getText().then(function(text) {
					console.log("User Found: "+text);
					expect(element(by.css('[id*="dataSet-2"] > image')).getAttribute('href')).toContain('data:image/jpeg;charset=utf-8;base64');
					element(by.css('[id*="DeviceDetailPanel"] > g:nth-child(4) > g:nth-child(13) > rect:nth-child(7)')).click();
					browser.sleep(3000);
				});
			}
			else {
				console.log("No Devices with Active Directory");
			}
		});
	});
});