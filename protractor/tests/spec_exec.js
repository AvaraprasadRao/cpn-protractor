//test_menu_exec.js
describe('Exec Menu Suite', function() {
	
	//Exec Page
	it('Check Device Classification', function() {
		console.log('\n\nExec Menu: Verify some of the Devices are Classified.');
		loginUser(usrnm_keys, pswrd_keys);
		//make sure numbers load
		browser.sleep(deviceLoadDelay);
		element(by.css('#devicesInfoSection text[fill="#4bb3ad"]')).getText().then(function(classified) {
			console.log('There are '+classified+' Devices');
			expect(parseInt(classified, 10)).toBeGreaterThan(0);
		});
	});

	it('Check if Total Device Number', function() {
		console.log('\nExec Menu: Check if all executive summary sections have a number; greater than or equal to 0.');

		browser.sleep(2000);
		element(by.css('#devicesInfoSection text[fill="#f65139"]')).getText().then(function(total) {
			var sum = 0;
			console.log('Total Devices Number found: '+parseInt(total, 10));
			expect(parseInt(total, 10)).toBeGreaterThan(-1);

			element(by.css('#devicesInfoSection text[fill="#bd3e3e"]')).getText().then(function(critical) {
				console.log('Critical Risk Devices Number found: '+parseInt(critical, 10));
				sum += parseInt(critical, 10);
				expect(parseInt(critical, 10)).toBeGreaterThan(-1);

				element(by.css('#devicesInfoSection text[fill="#c67734"]')).getText().then(function(high) {
					console.log('High Risk Devices Number found: '+parseInt(high, 10));
					sum += parseInt(high, 10);
					expect(parseInt(high, 10)).toBeGreaterThan(-1);

					element(by.css('#devicesInfoSection text[fill="#cfb329"]')).getText().then(function(medium) {
						console.log('Medium Risk Devices Number found: '+parseInt(medium, 10));
						sum += parseInt(medium, 10);
						expect(parseInt(medium, 10)).toBeGreaterThan(-1);

						element(by.css('#devicesInfoSection text[fill="#428353"]')).getText().then(function(low) {
							console.log('Low Risk Devices Number found: '+parseInt(low, 10));
							sum += parseInt(low, 10);
							expect(parseInt(low, 10)).toBeGreaterThan(-1);

							element(by.css('#devicesInfoSection text[fill="#3173a5"]')).getText().then(function(normal) {
								console.log('Normal Devices Number found: '+parseInt(normal, 10));
								sum += parseInt(normal, 10);
								expect(parseInt(normal, 10)).toBeGreaterThan(-1);

								expect(sum).toEqual(parseInt(total, 10));
							});
						});
					});
				});
			});
		});
	});

	it('Check if Clicking number changes Page to Device Page/Device List', function() {
		console.log ('\nExec Menu: Check if Clicking on some of the numbers change to Device Page/Device List.')
		//clicking total number
		element(by.css('#devicesInfoSection text[fill="#f65139"]')).getText().then(function(total) {
			if(parseInt(total, 10) > 0) {
				element(by.css('#devicesInfoSection text[fill="#f65139"]+rect')).click();
				browser.sleep(3000);
				expect(browser.getCurrentUrl()).toMatch('devicepage');
				element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
					console.log('Expect \"'+devices+'\" to contain \"'+total+'\"');
					expect(devices).toContain(total);
					element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
					browser.sleep(3000);
				});
			}
			else {
				console.log('There are 0 devices for the Total Devices Section');
				browser.sleep(3000);
			}
		});

		//clicking critical number
		element(by.css('#devicesInfoSection text[fill="#bd3e3e"]')).getText().then(function(critical) {
			if(parseInt(critical, 10) > 0) {
				element(by.css('#devicesInfoSection text[fill="#bd3e3e"]+line+rect')).click();
				browser.sleep(3000);
				expect(browser.getCurrentUrl()).toMatch('devicepage');
				element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
					console.log('Expect \"'+devices+'\" to contain \"'+critical+'\"');
					expect(devices).toContain((critical+' out of'));
					element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
					browser.sleep(3000);
				});
			}
			else {
				console.log('There are 0 devices for the Critical Risk Section');
				browser.sleep(3000);
			}
		});
		
		//clicking high number
		element(by.css('#devicesInfoSection text[fill="#c67734"]')).getText().then(function(high) {
			if(parseInt(high, 10) > 0) {
				element(by.css('#devicesInfoSection text[fill="#c67734"]+line+rect')).click();
				browser.sleep(3000);
				expect(browser.getCurrentUrl()).toMatch('devicepage');
				element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
					console.log('Expect \"'+devices+'\" to contain \"'+high+'\"');
					expect(devices).toContain((high+' out of'));
					element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
					browser.sleep(3000);
				});
			}
			else {
				console.log('There are 0 devices for the High Risk Section');
				browser.sleep(3000);
			}
		});

		//clicking medium number
		element(by.css('#devicesInfoSection text[fill="#cfb329"]')).getText().then(function(medium) {
			if(parseInt(medium, 10) > 0) {
				element(by.css('#devicesInfoSection text[fill="#cfb329"]+line+rect')).click();
				browser.sleep(3000);
				expect(browser.getCurrentUrl()).toMatch('devicepage');
				element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
					console.log('Expect \"'+devices+'\" to contain \"'+medium+'\"');
					expect(devices).toContain((medium+' out of'));
					element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
					browser.sleep(3000);
				});
			}
			else {
				console.log('There are 0 devices for the Medium Risk Section');
				browser.sleep(3000);
			}
		});

		//clicking low number
		element(by.css('#devicesInfoSection text[fill="#428353"]')).getText().then(function(low) {
			if(parseInt(low, 10) > 0) {
				element(by.css('#devicesInfoSection text[fill="#428353"]+line+rect')).click();
				browser.sleep(3000);
				expect(browser.getCurrentUrl()).toMatch('devicepage');
				element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
					console.log('Expect \"'+devices+'\" to contain \"'+low+'\"');
					expect(devices).toContain((low+' out of'));
					element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
					browser.sleep(3000);
				});
			}
			else {
				console.log('There are 0 devices for the Low Risk Section');
				browser.sleep(3000);
			}
		});

		//clicking normal number
		element(by.css('#devicesInfoSection text[fill="#3173a5"]')).getText().then(function(normal) {
			if(parseInt(normal, 10) > 0) {
				element(by.css('#devicesInfoSection text[fill="#3173a5"]+line+rect')).click();
				browser.sleep(3000);
				expect(browser.getCurrentUrl()).toMatch('devicepage');
				element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
					console.log('Expect \"'+devices+'\" to contain \"'+normal+'\"');
					expect(devices).toContain((normal+' out of'));
					element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
					browser.sleep(3000);
				});
			}
			else {
				console.log("There are 0 devices for the Normal Section");
				browser.sleep(3000);
			}
		});
	});

	it('Clicking percent archs should change to Device Page/Device List', function() {
		console.log('\nExec Menu: Clicking % Archs.');
		browser.sleep(3000);

		//critical percent
		element(by.css('#deviceRiskLevelArcChart text[fill="#bd3e3e"]')).getText().then(function(percent) {
			if(parseInt(percent, 10) > 0) {
				console.log('Clicking \"'+percent+'\" Critical Risk Arch');
				element(by.css('#devicesInfoSection text[fill="#bd3e3e"]')).getText().then(function(critical) {
					element(by.css('#deviceRiskLevelArcChart text[fill="#bd3e3e"]~rect:nth-child(9)')).click();
					browser.sleep(3000);
					expect(browser.getCurrentUrl()).toMatch('devicepage');
					element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
						expect(devices).toContain((critical+' out of'));
						expect(devices).toContain('Critical');
						element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
						browser.sleep(3000);
					});
				});
			}
			else {
				console.log('There are 0% Critical Risk Devices');
			}
		});

		//high percent
		element(by.css('#deviceRiskLevelArcChart text[fill="#c67734"]')).getText().then(function(percent) {
			if(parseInt(percent, 10) > 0) {
				console.log('Clicking \"'+percent+'\" High Risk Arch');
				element(by.css('#devicesInfoSection text[fill="#c67734"]')).getText().then(function(high) {
					element(by.css('#deviceRiskLevelArcChart text[fill="#c67734"]~rect:nth-child(13)')).click();
					browser.sleep(3000);
					expect(browser.getCurrentUrl()).toMatch('devicepage');
					element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
						expect(devices).toContain((high+' out of'));
						expect(devices).toContain('High');
						element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
						browser.sleep(3000);
					});
				});
			}
			else {
				console.log('There are 0% High Risk Devices');
			}
		});

		//medium percent
		element(by.css('#deviceRiskLevelArcChart text[fill="#cfb329"]')).getText().then(function(percent) {
			if(parseInt(percent, 10) > 0) {
				console.log('Clicking \"'+percent+'\" Medium Risk Arch');
				element(by.css('#devicesInfoSection text[fill="#cfb329"]')).getText().then(function(medium) {
					element(by.css('#deviceRiskLevelArcChart text[fill="#cfb329"]~rect:nth-child(17)')).click();
					browser.sleep(3000);
					expect(browser.getCurrentUrl()).toMatch('devicepage');
					element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
						expect(devices).toContain((medium+' out of'));
						expect(devices).toContain('Medium');
						element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
						browser.sleep(3000);
					});
				});
			}
			else {
				console.log('There are 0% Medium Risk Devices');
			}
		});

		//low percent
		element(by.css('#deviceRiskLevelArcChart text[fill="#428353"]')).getText().then(function(percent) {
			if(parseInt(percent, 10) > 0) {
				console.log('Clicking \"'+percent+'\" Low Risk Arch');
				element(by.css('#devicesInfoSection text[fill="#428353"]')).getText().then(function(low) {
					element(by.css('#deviceRiskLevelArcChart text[fill="#428353"]~rect:nth-child(21)')).click();
					browser.sleep(3000);
					expect(browser.getCurrentUrl()).toMatch('devicepage');
					element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
						expect(devices).toContain((low+' out of'));
						expect(devices).toContain('Low');
						element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
						browser.sleep(3000);
					});
				});
			}
			else {
				console.log('There are 0% Low Risk Devices');
			}
		});

		//normal percent
		element(by.css('#deviceRiskLevelArcChart text[fill="#3173a5"]')).getText().then(function(percent) {
			if(parseInt(percent, 10) > 0) {
				console.log('Clicking \"'+percent+'\" Normal Arch');
				element(by.css('#devicesInfoSection text[fill="#3173a5"]')).getText().then(function(normal) {
					element(by.css('#deviceRiskLevelArcChart text[fill="#3173a5"]~rect:nth-child(25)')).click();
					browser.sleep(3000);
					expect(browser.getCurrentUrl()).toMatch('devicepage');
					element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
						expect(devices).toContain((normal+' out of'));
						expect(devices).toContain('Normal');
						element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
						browser.sleep(3000);
					});
				});
			}
			else {
				console.log('There are 0% Normal Devices');
			}
		});
	});

	it('Clicking on Classification Groups', function() {
		console.log('\nExec Menu: Checking Latest Classification Groups leads to respective page.');
		browser.sleep(2000);

		//Row 1
		presentElementText(element(by.css('#topNGroupsList > svg > svg:nth-child(10) > text')), 'Row 1 no text', function() {
			expect(element(by.css('#topNGroupsList > svg > text:nth-child(7)')).getText()).toBe('1');
			element(by.css('#topNGroupsList > svg > text:nth-child(8)')).getText().then(function(num) {
				element(by.css('#topNGroupsList > svg > svg:nth-child(10) > text')).getText().then(function(group) {
					console.log("Clicking Row 1 - \""+group+"\" Classification Group");
					element(by.css('#topNGroupsList > svg > rect:nth-child(9)')).click();
					browser.sleep(3000);
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
					element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
					browser.sleep(3000);
				});
			});
		});

		//Row 2
		presentElementText(element(by.css('#topNGroupsList > svg > svg:nth-child(16) > text')), 'Row 2 no text', function() {
			expect(element(by.css('#topNGroupsList > svg > text:nth-child(13)')).getText()).toBe('2');
			element(by.css('#topNGroupsList > svg > text:nth-child(14)')).getText().then(function(num) {
				element(by.css('#topNGroupsList > svg > svg:nth-child(16) > text')).getText().then(function(group) {
					console.log("Clicking Row 2 - \""+group+"\" Classification Group");
					element(by.css('#topNGroupsList > svg > rect:nth-child(15)')).click();
					browser.sleep(3000);
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
					element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
					browser.sleep(3000);
				});
			});
		});

		//Row 3
		presentElementText(element(by.css('#topNGroupsList > svg > svg:nth-child(22) > text')), 'Row 3 no text', function() {
			expect(element(by.css('#topNGroupsList > svg > text:nth-child(19)')).getText()).toBe('3');
			element(by.css('#topNGroupsList > svg > text:nth-child(20)')).getText().then(function(num) {
				element(by.css('#topNGroupsList > svg > svg:nth-child(22) > text')).getText().then(function(group) {
					console.log("Clicking Row 3 - \""+group+"\" Classification Group");
					element(by.css('#topNGroupsList > svg > rect:nth-child(21)')).click();
					browser.sleep(3000);
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
					element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
					browser.sleep(3000);
				});
			});
		});

		//Row 4
		presentElementText(element(by.css('#topNGroupsList > svg > svg:nth-child(28) > text')), 'Row 4 no text', function() {
			expect(element(by.css('#topNGroupsList > svg > text:nth-child(25)')).getText()).toBe('4');
			element(by.css('#topNGroupsList > svg > text:nth-child(26)')).getText().then(function(num) {
				element(by.css('#topNGroupsList > svg > svg:nth-child(28) > text')).getText().then(function(group) {
					console.log("Clicking Row 4 - \""+group+"\" Classification Group");
					element(by.css('#topNGroupsList > svg > rect:nth-child(27)')).click();
					browser.sleep(3000);
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
					element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
					browser.sleep(3000);
				});
			});
		});

		//Row 5
		presentElementText(element(by.css('#topNGroupsList > svg > svg:nth-child(34) > text')), 'Row 5 no text', function() {
			expect(element(by.css('#topNGroupsList > svg > text:nth-child(31)')).getText()).toBe('5');
			element(by.css('#topNGroupsList > svg > text:nth-child(32)')).getText().then(function(num) {
				element(by.css('#topNGroupsList > svg > svg:nth-child(34) > text')).getText().then(function(group) {
					console.log("Clicking Row 5 - \""+group+"\" Classification Group");
					element(by.css('#topNGroupsList > svg > rect:nth-child(33)')).click();
					browser.sleep(3000);
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
					element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
					browser.sleep(3000);
				});
			});
		});

		//Row 6-10
		presentElementClick(element(by.css('#topNGroupsList g:nth-child(3) path[opacity="0.5"]+circle')), 'Row 6-10 unavailable', function() {
			browser.sleep(2000);
			//Row 6
			presentElementText(element(by.css('#topNGroupsList > svg > svg:nth-child(10) > text')), 'Row 6 no text', function() {
				expect(element(by.css('#topNGroupsList > svg > text:nth-child(7)')).getText()).toBe('6');
				element(by.css('#topNGroupsList > svg > text:nth-child(8)')).getText().then(function(num) {
					element(by.css('#topNGroupsList > svg > svg:nth-child(10) > text')).getText().then(function(group) {
						console.log("Clicking Row 6 - \""+group+"\" Classification Group");
						element(by.css('#topNGroupsList > svg > rect:nth-child(9)')).click();
						browser.sleep(3000);
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
						element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
						browser.sleep(3000);
					});
				});
			});

			//Row 7
			element(by.css('#topNGroupsList > svg > text:nth-child(13)')).getText().then(function(row) {
				if(row != '7') {
					element(by.css('#topNGroupsList g:nth-child(3) path[opacity="0.5"]+circle')).click();
					browser.sleep(2000);
				}
			});
			presentElementText(element(by.css('#topNGroupsList > svg > svg:nth-child(16) > text')), 'Row 7 no text', function() {
			expect(element(by.css('#topNGroupsList > svg > text:nth-child(13)')).getText()).toBe('7');
				element(by.css('#topNGroupsList > svg > text:nth-child(14)')).getText().then(function(num) {
					element(by.css('#topNGroupsList > svg > svg:nth-child(16) > text')).getText().then(function(group) {
						console.log("Clicking Row 7 - \""+group+"\" Classification Group");
						element(by.css('#topNGroupsList > svg > rect:nth-child(15)')).click();
						browser.sleep(3000);
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
						element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
						browser.sleep(3000);
					});
				});
			});

			//Row 8
			element(by.css('#topNGroupsList > svg > text:nth-child(19)')).getText().then(function(row) {
				if(row != '8') {
					element(by.css('#topNGroupsList g:nth-child(3) path[opacity="0.5"]+circle')).click();
					browser.sleep(2000);
				}
			});
			presentElementText(element(by.css('#topNGroupsList > svg > svg:nth-child(22) > text')), 'Row 8 no text',function() {
				expect(element(by.css('#topNGroupsList > svg > text:nth-child(19)')).getText()).toBe('8');
				element(by.css('#topNGroupsList > svg > text:nth-child(20)')).getText().then(function(num) {
					element(by.css('#topNGroupsList > svg > svg:nth-child(22) > text')).getText().then(function(group) {
						console.log("Clicking Row 8 - \""+group+"\" Classification Group");
						element(by.css('#topNGroupsList > svg > rect:nth-child(21)')).click();
						browser.sleep(3000);
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
						element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
						browser.sleep(3000);
					});
				});
			});

			//Row 9
			element(by.css('#topNGroupsList > svg > text:nth-child(25)')).getText().then(function(row) {
				if(row != '9') {
					element(by.css('#topNGroupsList g:nth-child(3) path[opacity="0.5"]+circle')).click();
					browser.sleep(2000);
				}
			});
			presentElementText(element(by.css('#topNGroupsList > svg > svg:nth-child(28) > text')), 'Row 9 no text', function() {
				expect(element(by.css('#topNGroupsList > svg > text:nth-child(25)')).getText()).toBe('9');
				element(by.css('#topNGroupsList > svg > text:nth-child(26)')).getText().then(function(num) {
					element(by.css('#topNGroupsList > svg > svg:nth-child(28) > text')).getText().then(function(group) {
						console.log("Clicking Row 9 - \""+group+"\" Classification Group");
						element(by.css('#topNGroupsList > svg > rect:nth-child(27)')).click();
						browser.sleep(3000);
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
						element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
						browser.sleep(3000);
					});
				});
			});

			//Row 10
			element(by.css('#topNGroupsList > svg > text:nth-child(31)')).getText().then(function(row) {
				if(row != '10') {
					element(by.css('#topNGroupsList g:nth-child(3) path[opacity="0.5"]+circle')).click();
					browser.sleep(2000);
				}
			});
			presentElementText(element(by.css('#topNGroupsList > svg > svg:nth-child(34) > text')), 'Row 10 no text', function() {
				expect(element(by.css('#topNGroupsList > svg > text:nth-child(31)')).getText()).toBe('10');
				element(by.css('#topNGroupsList > svg > text:nth-child(32)')).getText().then(function(num) {
					element(by.css('#topNGroupsList > svg > svg:nth-child(34) > text')).getText().then(function(group) {
						console.log("Clicking Row 10 - \""+group+"\" Classification Group");
						element(by.css('#topNGroupsList > svg > rect:nth-child(33)')).click();
						browser.sleep(3000);
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
						element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
						browser.sleep(3000);
					});
				});
			});
		});
	});

	it('Check Latest 5 Devices with Alarms', function() {
		browser.sleep(2000);
		console.log('\nExec Menu: Clicking all of the Latest 5 Devices with Alarms');

		//First Latest
		presentElement(element(by.css('#latestDeviceWithAlarmList circle:nth-child(3)[fill]')), 'First Device N/A', function() {
			presentElementText(element(by.css('#latestDeviceWithAlarmList text:nth-child(4)')), 'First Device no text', function() {
				element(by.css('#latestDeviceWithAlarmList text:nth-child(4)')).getText().then(function(fullDetails) {
					console.log('Clicking First device: \"'+fullDetails+'\"');
					element(by.css('#latestDeviceWithAlarmList rect:nth-child(5)')).click();
					browser.sleep(3000);
					expect(fullDetails).toContain(element(by.css('#topCards svg:nth-child(2) svg:nth-child(6) > text')).getText());
					expect(fullDetails).toContain(element(by.css('#topCards svg:nth-child(2) svg:nth-child(7) > text')).getText());
					element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
					browser.sleep(3000);
				});
			});
		});

		//Second Latest
		presentElement(element(by.css('#latestDeviceWithAlarmList circle:nth-child(6)[fill]')), 'Second Device N/A', function() {
			presentElementText(element(by.css('#latestDeviceWithAlarmList text:nth-child(7)')), "Second Device no text", function() {
				element(by.css('#latestDeviceWithAlarmList text:nth-child(7)')).getText().then(function(fullDetails) {
					console.log('Clicking Second device: \"'+fullDetails+'\"');
					element(by.css('#latestDeviceWithAlarmList rect:nth-child(8)')).click();
					browser.sleep(3000);
					expect(fullDetails).toContain(element(by.css('#topCards svg:nth-child(2) svg:nth-child(6) > text')).getText());
					expect(fullDetails).toContain(element(by.css('#topCards svg:nth-child(2) svg:nth-child(7) > text')).getText());
					element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
					browser.sleep(3000);
				});
			});
		});

		//Third Latest
		presentElement(element(by.css('#latestDeviceWithAlarmList circle:nth-child(9)[fill]')), 'Third Device N/A', function() {
			presentElementText(element(by.css('#latestDeviceWithAlarmList text:nth-child(10)')), "Third Device no text", function() {
				element(by.css('#latestDeviceWithAlarmList text:nth-child(10)')).getText().then(function(fullDetails) {
					console.log('Clicking Third device: \"'+fullDetails+'\"');
					element(by.css('#latestDeviceWithAlarmList rect:nth-child(11)')).click();
					browser.sleep(3000);
					expect(fullDetails).toContain(element(by.css('#topCards svg:nth-child(2) svg:nth-child(6) > text')).getText());
					expect(fullDetails).toContain(element(by.css('#topCards svg:nth-child(2) svg:nth-child(7) > text')).getText());
					element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
					browser.sleep(3000);
				});
			});
		});

		//Fourth Latest
		presentElement(element(by.css('#latestDeviceWithAlarmList circle:nth-child(12)[fill]')), 'Fourth Device N/A', function() {
			presentElementText(element(by.css('#latestDeviceWithAlarmList text:nth-child(13)')), "Fourth Device no text", function() {
				element(by.css('#latestDeviceWithAlarmList text:nth-child(13)')).getText().then(function(fullDetails) {
					console.log('Clicking Fourth device: \"'+fullDetails+'\"');
					element(by.css('#latestDeviceWithAlarmList rect:nth-child(14)')).click();
					browser.sleep(3000);
					expect(fullDetails).toContain(element(by.css('#topCards svg:nth-child(2) svg:nth-child(6) > text')).getText());
					expect(fullDetails).toContain(element(by.css('#topCards svg:nth-child(2) svg:nth-child(7) > text')).getText());
					element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
					browser.sleep(3000);
				});
			});
		});

		//Fifth Latest
		presentElement(element(by.css('#latestDeviceWithAlarmList circle:nth-child(15)[fill]')), 'Fifth Device N/A', function() {
			presentElementText(element(by.css('#latestDeviceWithAlarmList text:nth-child(16)')), "Fifth Device no text", function() {
				element(by.css('#latestDeviceWithAlarmList text:nth-child(16)')).getText().then(function(fullDetails) {
					console.log('Clicking Fifth device: \"'+fullDetails+'\"');
					element(by.css('#latestDeviceWithAlarmList rect:nth-child(17)')).click();
					browser.sleep(3000);
					expect(fullDetails).toContain(element(by.css('#topCards svg:nth-child(2) svg:nth-child(6) > text')).getText());
					expect(fullDetails).toContain(element(by.css('#topCards svg:nth-child(2) svg:nth-child(7) > text')).getText());
					element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
					browser.sleep(3000);
				});
			});
		});
	});

	//Bottom Corner Buttons
	it('Clicking Bottom Corner About Us Section', function() {
		console.log('\nExec Menu: Clicking Bottom Section and check if \"About Us\" loads correctly.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		//About Us
		element(by.css('#gnBottomMenu > rect:nth-child(7)')).click().then(function() {console.log('Loading \"About Us\" Page');});
		browser.sleep(3000);
		browser.getAllWindowHandles().then(function (handles) {
			browser.sleep(2000);
			browser.switchTo().window(handles[1]).then(function() {
				ignoreSynchronization(function() {
					browser.refresh();
					browser.sleep(7000);
					expect(browser.getTitle()).toEqual('CloudPost');
					expect(browser.getCurrentUrl()).toMatch('cloudpostnetworks');
					browser.close();
					browser.switchTo().window(handles[0]);
				});
			});
		});
	});

	it('Clicking Bottom Corner EULA Section', function() {
		console.log('\nExec Menu: Clicking Bottom Section and check if \"EULA\" loads correctly.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		//EULA
		element(by.css('#gnBottomMenu > rect:nth-child(11)')).click().then(function() {console.log('Loading \"EULA\" Page');});
		browser.sleep(5000);
		browser.getAllWindowHandles().then(function (handles) {
			browser.sleep(2000);
			browser.switchTo().window(handles[1]).then(function() {
				ignoreSynchronization(function() {
					expect(handles.length).toEqual(2);
					//browser.sleep(3000);
					//expect(browser.getTitle()).toMatch('CPN Clickthrough');
					expect(browser.getCurrentUrl()).toMatch('eula.pdf');
					browser.close();
					browser.switchTo().window(handles[0]);
				});
			});
		});
	});

	it('Clicking Bottom Corner Credit Section', function() {
		console.log('\nExec Menu: Clicking Bottom Section and check if \"Credit\" loads correctly.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		//Credit
		element(by.css('#gnBottomMenu > rect:nth-child(15)')).click().then(function() {console.log('Loading \"Credit\" Page');});
		browser.sleep(5000);
		browser.getAllWindowHandles().then(function (handles) {
			browser.sleep(2000);
			browser.switchTo().window(handles[1]).then(function() {
				ignoreSynchronization(function() {
					expect(handles.length).toEqual(2);
					//browser.sleep(3000);
					//expect(browser.getTitle()).toMatch('CPN-Open-Source-Licenses');
					expect(browser.getCurrentUrl()).toMatch('credit.pdf');
					browser.close();
					browser.switchTo().window(handles[0]);
				});
			});
		});
	});
});