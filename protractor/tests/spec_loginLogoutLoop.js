describe('Individual Devices Suite', function() {

	it('Continuously login and logout', function(done) {
		var i;
		for (i = 0; i < 100; i++) {
			browser.call(getURL);
			element(by.css(startLogin_css)).click();
			element(by.id(skipLoginButtin_id)).click();
			browser.actions().click().perform();
			element(by.id(usrnm_id)).sendKeys(usrnm_keys);
			element(by.id(pswrdInputSelect_id)).click();
			element(by.id(pswrd_id)).sendKeys(pswrd_keys);
			element(by.css(startLogin_css)).click().then(function() {
				console.log('\nLogin');
			});
			browser.sleep(1000);
			expect(browser.getCurrentUrl()).toMatch('systemsummary');
			element(by.id('AccountMenu')).click();
			element(by.css('image+text+rect[x="0"][y="107"]')).click().then(function() {
				console.log('Logout');
			});
			//browser.sleep(1000);
			expect(browser.getCurrentUrl()).toMatch('login');
		}
	});
});