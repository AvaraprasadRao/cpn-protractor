//test03_systemPageIntegration.js

describe('System Tab Integration Suite', function() {

	function clickService(service) {
		let integrationTabs = element.all(by.css('[id^="ServicesBrowserPanel"] > g > g > rect:nth-child(7)'));
		integrationTabs.get(0).click();
		browser.sleep(3000);
		expect(element(by.id('ExtServiceBrowserPanel(External Services)0:0')).isPresent()).toBe(true);

		let externalServices = element.all(by.css('#topCards svg text'));
		let externalServicesClick = element.all(by.css('#topCards svg > rect:last-of-type'));
		presentElementClick(externalServicesClick.filter(function(elem, index) {
			return externalServices.get(index).getText().then(function(text) {
				return stringContains(text, service);
			});
		}).first(), '', function() {}, function() {
			integrationTabs.get(1).click();
			browser.sleep(3000);
			expect(element(by.id('IntServiceBrowserPanel(Internal Services)0:0')).isPresent()).toBe(true);

			let internalServices = element.all(by.css('#topCards svg text'));
			let internalServicesClick = element.all(by.css('#topCards svg > rect:last-of-type'));
			presentElementClick(internalServicesClick.filter(function(elem, index) {
				return internalServices.get(index).getText().then(function(text) {
					return stringContains(text, service);
				});
			}).first(), '', function() {});
		});
	}
	
	//Active Directory
	it('Checking Active Directory', function() {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		expect(browser.getCurrentUrl()).toMatch('systempage');
		expect(element(by.id('NetworkConfigPanel(Equipment Configuration)0:0')).isPresent()).toBe(true);
		element.all(by.css('#sideMenu~g>text~rect')).get(4).click();
		browser.sleep(4000).then(function() {
			console.log('\n\nSystem Integration Check: Active Directory Service');
		});
		expect(element(by.id('ServicesBrowserPanel(Service Integration)0:0')).isPresent()).toBe(true);

		let dropdown = element(by.css('*[id*=pollFrequency]'));
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(2).click();
		clickService('Active Directory');

		browser.sleep(2000);
		expect(element(by.id('ad-servername')).getAttribute('value')).toBe('ad-server-2');
		expect(element(by.id('ad-domainname')).getAttribute('value')).toBe('ad2.cpn.lan');
		expect(element(by.id('ad-serverip')).getAttribute('value')).toBe('192.168.103.11');
		expect(element(by.id('ad-username')).getAttribute('value')).toBe('cpnadmin');
		//expect(element(by.id('ad-password')).getAttribute('value')).toBe('ironfist2!');
		// expect(element(by.id('ad-servername')).getAttribute('value')).toBe('ad-server-1');
		// expect(element(by.id('ad-domainname')).getAttribute('value')).toBe('ad1.cpn.lan');
		// expect(element(by.id('ad-serverip')).getAttribute('value')).toBe('192.168.103.89');
		// expect(element(by.id('ad-username')).getAttribute('value')).toBe('cpnadmin');
		// expect(element(by.id('ad-password')).getAttribute('value')).toBe('ironfist2!');

		expect(dropdown.all(by.css('text')).get(0).getText()).toEqual('1 hour');

		browser.sleep(1000);
	});

	//Email
	it('Checking Email Service', function() {
		console.log('\nSystem Integration Check: Email Service');
		let dropdown = element(by.css('*[id*=securitymode]'));
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(0).click();
		clickService('Email|SMTP');
		browser.sleep(2000);

		expect(element(by.id('smtp-server')).getAttribute('value')).toBe('smtp.office365.com');
		expect(element(by.id('smtp-port')).getAttribute('value')).toBe('587');
		expect(element(by.id('smtp-username')).getAttribute('value')).toBe('support@cloudpostnetworks.com');
		//expect(element(by.id('smtp-password')).getAttribute('value')).toBe('sP0bS3lN!');
		
		expect(dropdown.all(by.css('text')).get(0).getText()).toEqual('STARTTLS');

		browser.sleep(1000);
	});

	//Alarms Notification
	it('Checking Alarms Notification Service', function() {
		console.log('\nSystem Integration Check: Alarms Notification Service');
		let emailElements = element.all(by.css('*[id^=email]'));
		let dropdown = element(by.css('*[id*=frequencyInput-alarms-notification]'));
		let timeInputs = element.all(by.css('div input'));
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(1).click();		
		clickService('Alarms Notification');
		browser.sleep(2000);

		var url = require('url');
		var host = url.parse(browser.params.url, true).host;
		expect(emailElements.get(0).getAttribute('value')).toBe(browser.params.email);
		expect(emailElements.get(1).getAttribute('value')).toBe(host+'_Alarm_Alert');
		
		expect(dropdown.all(by.css('text')).get(0).getText()).toEqual('Daily');

		expect(timeInputs.get(2).getAttribute('value')).toBe('11');
		expect(timeInputs.get(3).getAttribute('value')).toContain('0');

		browser.sleep(1000);
	});

	//Devices Notification
	it('Checking Devices Notification Service', function() {
		console.log('\nSystem Integration Check: Devices Notification Service');
		let emailElements = element.all(by.css('*[id^=email]'));
		let dropdown = element(by.css('*[id*=frequencyInput-devicegroups-notification]'));
		let timeInputs = element.all(by.css('div input'));
		//element.all(by.css('#topCards image ~ rect ~ rect')).get(2).click();
		clickService('Devices Notification');
		browser.sleep(2000);

		var url = require('url');
		var host = url.parse(browser.params.url, true).host;
		expect(emailElements.get(0).getAttribute('value')).toBe(browser.params.email);
		expect(emailElements.get(1).getAttribute('value')).toBe(host+'_Devices_Alert');

		expect(dropdown.all(by.css('text')).get(0).getText()).toEqual('Daily');

		expect(timeInputs.get(2).getAttribute('value')).toBe('11');
		expect(timeInputs.get(3).getAttribute('value')).toContain('0');

		browser.sleep(1000);
	});
});