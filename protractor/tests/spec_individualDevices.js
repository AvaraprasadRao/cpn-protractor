//test_individualDevices.js
describe('Individual Devices Suite', function() {

	//Individual Devices
	it('Individual Device Details Test - AXIS Device', function() {
		console.log("\n\nIndividual Devices: AXIS Device Information Check");
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);

		var searchValues = {
			"mac": /AC:CC:8E:0F:57:88/i,
			"desc": /Network Camera/i,
			"manu": /Axis Communications/i,
			"model": /P3364|N\/A/i,
			"serial": /[\w\d]{12}|N\/A/i,
			"ost": /Linux|3\.2|4\.8|N\/A/i,
			"sw": /axis|P3364|Network Camera|6\.50\.1\.2/i,
			"fqdn": /axis-accc8e0f5788|\.cpn\.lan|N\/A/i,
			"dhcp": /axis|accc8e0f5788/i,
			"class": /Classified/i,
			"type": /Network Camera/i,
			"group": /Physical Security Devices/i,
			"prof": /axis|P3364|Network Camera/i,
			"endp": /\bIoT Endpoint\b/i
		};

		let macAddresses = element.all(by.css('svg > svg:nth-child(5) > text'));
		let deviceLinks = element.all(by.css('#topCards svg > svg:nth-child(4) > text'));

		element(by.css('#gnTopMenu > rect:nth-child(22)')).click();
		browser.sleep(2000);
		//element(by.css('g:nth-child(2) > g > g:nth-child(6) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('Device List', sideMenuClicks, sideMenuTexts);
		browser.sleep(2000);
		element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).clear();
		browser.sleep(2000);
		element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).sendKeys('AXIS');
		browser.sleep(3000);

		presentElementClick(deviceLinks.filter(function(elem, index) {
				return macAddresses.get(index).getText().then(function(text) {
					return text === 'AC:CC:8E:0F:57:88';
				});
			}).first(), 'AXIS Device: Mac Address - \"AC:CC:8E:0F:57:88\" Not Found', 

			function() {
				//browser.actions().mouseMove(deviceLinks.first()).mouseMove({x: -5, y: 0}).click().perform();

				console.log('Clicking AXIS Device with Mac Address: \"AC:CC:8E:0F:57:88\"');

				browser.sleep(5000).then(function(){console.log('\nInformation:');});

				let deviceInformation = element.all(by.css('[id^="DeviceDetailPanel"] g:nth-child(1) > svg > text'));
				let deviceClassification = element.all(by.css('[id^="DeviceDetailPanel"] g:nth-child(2) > svg > text'));

				findDetail(deviceInformation, searchValues["mac"], 'Mac Address');
				findDetail(deviceInformation, searchValues["desc"], 'Device Description');
				findDetail(deviceInformation, searchValues["manu"], 'Manufacturer');
				findDetail(deviceInformation, searchValues["model"], 'Model Name/No.');
				findDetail(deviceInformation, searchValues["serial"], 'Serial No.');
				findDetail(deviceInformation, searchValues["ost"], 'OS Type');
				findDetail(deviceInformation, searchValues["sw"], 'SW Version');
				findDetail(deviceInformation, searchValues["fqdn"], 'FQDN');
				findDetail(deviceInformation, searchValues["dhcp"], 'DHCP Hostname');

				browser.sleep(1000).then(function(){console.log('\nClassification:');});

				findDetail(deviceClassification, searchValues["class"], 'Classification State');
				findDetail(deviceClassification, searchValues["type"], 'Device Type');
				findDetail(deviceClassification, searchValues["group"], 'Group');
				findDetail(deviceClassification, searchValues["prof"], 'Profile');
				findDetail(deviceClassification, searchValues["endp"], 'End Point Type');
			}
		, function() {fail('no AXIS device with Mac Address - \"AC:CC:8E:0F:57:88\"');}, -5, 0);
	});

	it('Individual Device Details Test - Dell Laptop', function() {
		console.log("\n\nIndividual Devices: Dell Laptop Device Information Check");

		var searchValues = {
			"mac": /70:1C:E7:D5:D3:33/i,
			"desc": /Workstation/i,
			"manu": /Intel Corporate/i,
			"model": /Windows/i,
			"serial": /[\w\d]{12}|N\/A/i,
			"ost": /Windows|Windows 10|N\/A/i,
			"osv": /10\.0|10|N\/A/i,
			"fqdn": /TEST-DELL1500-LAPTOP|\.cpn\.lan|N\/A/i,
			"dhcp": /TEST-DELL1500-LAPTOP/i,
			"class": /Classified/i,
			"type": /Workstation/i,
			"group": /Workstations/i,
			"prof": /IntelCor|Windows|Workstation/i,
			"endp": /Non-IoT Endpoint/i
		};

		let macAddresses = element.all(by.css('svg > svg:nth-child(5) > text'));
		//let deviceLinks = element.all(by.css('#topCards svg > rect:nth-child(3)'));
		let deviceLinks = element.all(by.css('#topCards svg > svg:nth-child(4) > text'));

		scrollToView(element(by.css('[id^="DeviceBrowserPanel(List of Devices)0"]')));
		browser.sleep(2000);
		element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).clear();
		browser.sleep(2000);
		element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).sendKeys('DELL');
		browser.sleep(3000);

		presentElementClick(deviceLinks.filter(function(elem, index) {
				return macAddresses.get(index).getText().then(function(text) {
					return text === '70:1C:E7:D5:D3:33';
				});
			}).first(), 'Dell Device: Mac Address - \"70:1C:E7:D5:D3:33\" Not Found', 

			function() {
				//browser.actions().mouseMove(deviceLinks.first()).mouseMove({x: -5, y: 0}).click().perform();

				console.log('Clicking Dell Device with Mac Address: \"70:1C:E7:D5:D3:33\"');				

				browser.sleep(5000).then(function(){console.log('\nInformation:');});

				let deviceInformation = element.all(by.css('[id^="DeviceDetailPanel"] g:nth-child(1) > svg > text'));
				let deviceClassification = element.all(by.css('[id^="DeviceDetailPanel"] g:nth-child(2) > svg > text'));

				findDetail(deviceInformation, searchValues["mac"], 'Mac Address');
				findDetail(deviceInformation, searchValues["desc"], 'Device Description');
				findDetail(deviceInformation, searchValues["manu"], 'Manufacturer');
				findDetail(deviceInformation, searchValues["model"], 'Model Name/No.');
				findDetail(deviceInformation, searchValues["serial"], 'Serial No.');
				findDetail(deviceInformation, searchValues["ost"], 'OS Type');
				findDetail(deviceInformation, searchValues["osv"], 'OS Version');
				findDetail(deviceInformation, searchValues["fqdn"], 'FQDN');
				findDetail(deviceInformation, searchValues["dhcp"], 'DHCP Hostname');

				browser.sleep(1000).then(function(){console.log('\nClassification:');})

				findDetail(deviceClassification, searchValues["class"], 'Classification State');
				findDetail(deviceClassification, searchValues["type"], 'Device Type');
				findDetail(deviceClassification, searchValues["group"], 'Group');
				findDetail(deviceClassification, searchValues["prof"], 'Profile');
				findDetail(deviceClassification, searchValues["endp"], 'End Point Type');
			}
		, function() {fail('no Dell device with Mac Address - \"70:1C:E7:D5:D3:33\"');}, -5, 0);
	});

	it('Individual Device Details Test - Medical DICOM Device', function() {
		console.log("\n\nIndividual Devices: Medical DICOM Device Information Check");

		var searchValues = {
			"mac": /00:50:56:F2:B1:1C/i,
			"desc": /CT Scanner/i,
			"manu": /GE Medical Systems/i,
			"model": /HiSpeed CT\/i/i,
			"serial": /N\/A/i,
			"ost": /N\/A/i,
			"sw": /05/i,
			"fqdn": /N\/A/i,
			"dhcp": /CT[\d]/i,
			"class": /Classified/i,
			"type": /CT Scanner/i,
			"group": /Medical Devices/i,
			"prof": /GE-HighSpeedCTi-CT Scanner/i,
			"endp": /\bIoT Endpoint\b/i
		};

		let macAddresses = element.all(by.css('svg > svg:nth-child(5) > text'));
		//let deviceLinks = element.all(by.css('#topCards svg > rect:nth-child(3)'));
		let deviceLinks = element.all(by.css('#topCards svg > svg:nth-child(4) > text'));


		scrollToView(element(by.css('[id^="DeviceBrowserPanel(List of Devices)0"]')));
		browser.sleep(2000);
		element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).clear();
		browser.sleep(2000);
		element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).sendKeys('CT');
		browser.sleep(3000);

		presentElementClick(deviceLinks.filter(function(elem, index) {
				return macAddresses.get(index).getText().then(function(text) {
					return text === '00:50:56:F2:B1:1C';
				});
			}).first(), 'Medical DICOM Device: Mac Address - \"00:50:56:F2:B1:1C\" Not Found', 

			function() {
				console.log('Clicking Medical DICOM Device with Mac Address: \"00:50:56:F2:B1:1C\"');				

				browser.sleep(5000).then(function(){console.log('\nInformation:');});

				let deviceInformation = element.all(by.css('[id^="DeviceDetailPanel"] g:nth-child(1) > svg > text'));
				let deviceClassification = element.all(by.css('[id^="DeviceDetailPanel"] g:nth-child(2) > svg > text'));

				findDetail(deviceInformation, searchValues["mac"], 'Mac Address');
				findDetail(deviceInformation, searchValues["desc"], 'Device Description');
				findDetail(deviceInformation, searchValues["manu"], 'Manufacturer');
				findDetail(deviceInformation, searchValues["model"], 'Model Name/No.');
				findDetail(deviceInformation, searchValues["serial"], 'Serial No.');
				findDetail(deviceInformation, searchValues["ost"], 'OS Type');
				findDetail(deviceInformation, searchValues["sw"], 'SW Version');
				findDetail(deviceInformation, searchValues["fqdn"], 'FQDN');
				findDetail(deviceInformation, searchValues["dhcp"], 'DHCP Hostname');

				browser.sleep(1000).then(function(){console.log('\nClassification:');});

				findDetail(deviceClassification, searchValues["class"], 'Classification State');
				findDetail(deviceClassification, searchValues["type"], 'Device Type');
				findDetail(deviceClassification, searchValues["group"], 'Group');
				findDetail(deviceClassification, searchValues["prof"], 'Profile');
				findDetail(deviceClassification, searchValues["endp"], 'End Point Type');
			}
		, function() {fail('no Medical DICOM device with Mac Address - \"00:50:56:F2:B1:1C\"');}, -5, 0);
	});

	it('Individual Device Details Test - Brother Printer', function() {
		console.log("\n\nIndividual Devices: Brother Printer Device Information Check");

		var searchValues = {
			"mac": /00:80:92:D6:DE:FD/i,
			"desc": /Printer/i,
			"manu": /Silex Technology/i,
			"model": /NC-7900w|N\/A/i,
			"serial": /N\/A/i,
			"ost": /N\/A/i,
			"fqdn": /N\/A/i,
			"dhcp": /N\/A/i,
			"class": /Classified/i,
			"type": /SilexTec|Printer/i,
			"group": /Printers|Copiers/i,
			"prof": /SilexTec|Printer/i,
			"endp": /\bIoT Endpoint\b/i
		};

		let macAddresses = element.all(by.css('svg > svg:nth-child(5) > text'));
		//let deviceLinks = element.all(by.css('#topCards svg > rect:nth-child(3)'));
		let deviceLinks = element.all(by.css('#topCards svg > svg:nth-child(4) > text'));

		scrollToView(element(by.css('[id^="DeviceBrowserPanel(List of Devices)0"]')));
		browser.sleep(2000);
		element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).clear();
		browser.sleep(2000);
		element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).sendKeys('Printer');
		browser.sleep(3000);

		presentElementClick(deviceLinks.filter(function(elem, index) {
				return macAddresses.get(index).getText().then(function(text) {
					return text === '00:80:92:D6:DE:FD';
				});
			}).first(), 'Printer Device: Mac Address - \"00:80:92:D6:DE:FD\" Not Found', 

			function() {
				//browser.actions().mouseMove(deviceLinks.first()).mouseMove({x: -5, y: 0}).click().perform();

				console.log('Clicking Printer Device with Mac Address: \"00:80:92:D6:DE:FD\"');

				browser.sleep(5000).then(function(){console.log('\nInformation:');});

				let deviceInformation = element.all(by.css('[id^="DeviceDetailPanel"] g:nth-child(1) > svg > text'));
				let deviceClassification = element.all(by.css('[id^="DeviceDetailPanel"] g:nth-child(2) > svg > text'));

				findDetail(deviceInformation, searchValues["mac"], 'Mac Address');
				findDetail(deviceInformation, searchValues["desc"], 'Device Description');
				findDetail(deviceInformation, searchValues["manu"], 'Manufacturer');
				findDetail(deviceInformation, searchValues["model"], 'Model Name/No.');
				findDetail(deviceInformation, searchValues["serial"], 'Serial No.');
				findDetail(deviceInformation, searchValues["ost"], 'OS Type');
				findDetail(deviceInformation, searchValues["fqdn"], 'FQDN');
				findDetail(deviceInformation, searchValues["dhcp"], 'DHCP Hostname');

				browser.sleep(1000).then(function(){console.log('\nClassification:');});

				findDetail(deviceClassification, searchValues["class"], 'Classification State');
				findDetail(deviceClassification, searchValues["type"], 'Device Type');
				findDetail(deviceClassification, searchValues["group"], 'Group');
				findDetail(deviceClassification, searchValues["prof"], 'Profile');
				findDetail(deviceClassification, searchValues["endp"], 'End Point Type');
			}
		, function() {fail('no Printer device with Mac Address - \"00:80:92:D6:DE:FD\"');}, -5, 0);
	});

	it('Individual Device Details Test - Roku TV Device', function() {
		console.log("\n\nIndividual Devices: Roku TV Device Information Check");

		var searchValues = {
			"mac": /AC:E0:10:B3:EA:16/i,
			"desc": /Television/i,
			"manu": /Roku/i,
			"model": /TCL 40FS3750 5000X/i,
			"serial": /2N0051129600|N\/A/i,
			"ost": /N\/A/i,
			"fqdn": /N\/A/i,
			"dhcp": /TCL|Roku TV|600/i,
			"class": /Classified/i,
			"type": /Television/i,
			"group": /Media Devices/i,
			"prof": /Roku-Television/i,
			"endp": /Non-IoT Endpoint/i
		};

		let macAddresses = element.all(by.css('svg > svg:nth-child(5) > text'));
		//let deviceLinks = element.all(by.css('#topCards svg > rect:nth-child(3)'));
		let deviceLinks = element.all(by.css('#topCards svg > svg:nth-child(4) > text'));

		scrollToView(element(by.css('[id^="DeviceBrowserPanel(List of Devices)0"]')));
		browser.sleep(2000);
		element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).clear();
		browser.sleep(2000);
		element(by.id('DeviceBrowserPanel(List of Devices)BPCTxtField')).sendKeys('Roku');
		browser.sleep(3000);

		presentElementClick(deviceLinks.filter(function(elem, index) {
				return macAddresses.get(index).getText().then(function(text) {
					return text === 'AC:E0:10:B3:EA:16';
				});
			}).first(), 'Roku TV Device: Mac Address - \"AC:E0:10:B3:EA:16\" Not Found', 

			function() {
				browser.actions().mouseMove(deviceLinks.first()).mouseMove({x: -5, y: 0}).click().perform();

				console.log('Clicking Roku TV Device with Mac Address: \"AC:E0:10:B3:EA:16\"');
				
				browser.sleep(5000).then(function(){console.log('\nInformation:');});

				let deviceInformation = element.all(by.css('[id^="DeviceDetailPanel"] g:nth-child(1) > svg > text'));
				let deviceClassification = element.all(by.css('[id^="DeviceDetailPanel"] g:nth-child(2) > svg > text'));

				findDetail(deviceInformation, searchValues["mac"], 'Mac Address');
				findDetail(deviceInformation, searchValues["desc"], 'Device Description');
				findDetail(deviceInformation, searchValues["manu"], 'Manufacturer');
				findDetail(deviceInformation, searchValues["model"], 'Model Name/No.');
				findDetail(deviceInformation, searchValues["serial"], 'Serial No.');
				findDetail(deviceInformation, searchValues["ost"], 'OS Type');
				findDetail(deviceInformation, searchValues["fqdn"], 'FQDN');
				findDetail(deviceInformation, searchValues["dhcp"], 'DHCP Hostname');

				browser.sleep(1000).then(function(){console.log('\nClassification:');});

				findDetail(deviceClassification, searchValues["class"], 'Classification State');
				findDetail(deviceClassification, searchValues["type"], 'Device Type');
				findDetail(deviceClassification, searchValues["group"], 'Group');
				findDetail(deviceClassification, searchValues["prof"], 'Profile');
				findDetail(deviceClassification, searchValues["endp"], 'End Point Type');
			}
		, function() {fail('no Roku TV device with Mac Address - \"AC:E0:10:B3:EA:16\"');}, -5, 0);
	});
});