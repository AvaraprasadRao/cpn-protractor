//test04_systemPageIntegrationDelete.js

describe('System Tab Delete Configurations Suite', function() {

	function clickService(service) {
		let integrationTabs = element.all(by.css('[id^="ServicesBrowserPanel"] > g > g > rect:nth-child(7)'));
		integrationTabs.get(0).click();
		browser.sleep(3000);
		expect(element(by.id('ExtServiceBrowserPanel(External Services)0:0')).isPresent()).toBe(true);

		let externalServices = element.all(by.css('#topCards svg text'));
		let externalServicesClick = element.all(by.css('#topCards svg > rect:last-of-type'));
		presentElementClick(externalServicesClick.filter(function(elem, index) {
			return externalServices.get(index).getText().then(function(text) {
				return stringContains(text, service);
			});
		}).first(), '', function() {}, function() {
			integrationTabs.get(1).click();
			browser.sleep(3000);
			expect(element(by.id('IntServiceBrowserPanel(Internal Services)0:0')).isPresent()).toBe(true);

			let internalServices = element.all(by.css('#topCards svg text'));
			let internalServicesClick = element.all(by.css('#topCards svg > rect:last-of-type'));
			presentElementClick(internalServicesClick.filter(function(elem, index) {
				return internalServices.get(index).getText().then(function(text) {
					return stringContains(text, service);
				});
			}).first(), '', function() {});
		});
	}

	//External Services: Active Directory
	it('Deleting External Services', function() {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(7000);
		expect(browser.getCurrentUrl()).toMatch('systempage');
		expect(element(by.id('NetworkConfigPanel(Equipment Configuration)0:0')).isPresent()).toBe(true);
		element.all(by.css('#sideMenu~g>text~rect')).get(4).click();
		browser.sleep(4000).then(function() {
			console.log('\n\nSystem Cleanup: Cleaning Active Directory Details');
		});
		expect(element(by.id('ServicesBrowserPanel(Service Integration)0:0')).isPresent()).toBe(true);

		clickService('Active Directory');
		browser.sleep(2000);

		element(by.id('DeleteEventRectangle')).click();
		browser.sleep(2000);
		// ignoreSynchronization(function() {
		// 	expect(element(by.css('text[fill="#4bb3ad"]')).isPresent()).toBe(true);
		// 	expect(element(by.css('text[fill="#4bb3ad"]')).getText()).toMatch("Successfully saved changes.");
		// });
		element(by.id('SaveEventRectangle')).click();
		element(by.id('SaveEventRectangle')).click();
		browser.sleep(1000);
	});

	//Internal Services: Email
	it('Cleaning up Internal Email Services', function() {
		console.log('\nSystem Cleanup: Cleaning Internal Email Services');
		clickService('Email|SMTP');
		browser.sleep(2000);

		element(by.id('DeleteEventRectangle')).click();
		browser.sleep(2000);
		element(by.id('SaveEventRectangle')).click();
		element(by.id('SaveEventRectangle')).click();
		browser.sleep(1000);
	});

	//Internal Services: Alarms Notification
	it('Setting up Internal Alarms Notification Service', function() {
		console.log('\nSystem Cleanup: Cleaning Internal Alarm Notifications Service');
		clickService('Alarms Notification');
		browser.sleep(2000);

		element(by.id('DeleteEventRectangle')).click();
		browser.sleep(2000);
		// ignoreSynchronization(function() {
		// 	expect(element(by.css('text[fill="#4bb3ad"]')).isPresent()).toBe(true);
		// 	expect(element(by.css('text[fill="#4bb3ad"]')).getText()).toMatch("Successfully saved changes.");
		// });
		browser.sleep(2000);
		element(by.id('SaveEventRectangle')).click();
		element(by.id('SaveEventRectangle')).click();
		browser.sleep(1000);
	});

	//Internal Services: Devices Notification
	it('Setting up Internal Devices Notification Service', function() {
		console.log('\nSystem Cleanup: Cleaning Internal Devices Notifications Service');
		clickService('Devices Notification');
		browser.sleep(2000);
		
		element(by.id('DeleteEventRectangle')).click();
		browser.sleep(2000);
		// ignoreSynchronization(function() {
		// 	expect(element(by.css('text[fill="#4bb3ad"]')).isPresent()).toBe(true);
		// 	expect(element(by.css('text[fill="#4bb3ad"]')).getText()).toMatch("Successfully saved changes.");
		// });
		browser.sleep(2000);
		element(by.id('SaveEventRectangle')).click();
		element(by.id('SaveEventRectangle')).click();
		browser.sleep(1000);
	});
});
