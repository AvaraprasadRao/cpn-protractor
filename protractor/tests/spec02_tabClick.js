//test_02_tabClick.js

describe('Main Tabs Click Suite', function() {
	//login with admin username and password, click Group Tab and check if page loaded properly
	it('Check Group Tab is Clickable', function() {
		console.log('\n\nMain Tabs: Clicking on Group Tab.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(14)')).click();
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toMatch('grouppage');
		expect(element(by.id('GroupBrowserPanel(Group Browser)0:0')).isPresent()).toBe(true);
		//expect(element(by.css('#GroupBrowserPanel(Group Browser)0:0 > text:nth-child(1)')).getText()).toMatch('Group Browser');
	});

	it('Check Profile Tab is Clickable', function() {
		console.log('\nMain Tabs: Clicking on Profile Tab.');
		element(by.css('#gnTopMenu > rect:nth-child(18)')).click();
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toMatch('profilepage');
		expect(element(by.id('ProfileBrowserPanel(Classification Profiles)0:0')).isPresent()).toBe(true);
	});

	it('Check Device Tab is Clickable', function() {
		console.log('\nMain Tabs: Clicking on Device Tab.');
		element(by.css('#gnTopMenu > rect:nth-child(22)')).click();
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toMatch('devicepage');
		expect(element(by.id('DeviceChartPanel(Device Summary)0:0')).isPresent()).toBe(true);
	});

	it('Check Application Tab is Clickable', function() {
		console.log('\nMain Tabs: Clicking on Application Tab.');
		element(by.css('#gnTopMenu > rect:nth-child(26)')).click();
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toMatch('apppage');
		expect(element(by.id('ApplicationSummaryPanel(Device Application Usage Summary)0:0')).isPresent()).toBe(true);
	});

	it('Check Network Tab is Clickable', function() {
		console.log('\nMain Tabs: Clicking on Network Tab.');
		element(by.css('#gnTopMenu > rect:nth-child(30)')).click();
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toMatch('networkpage');
		expect(element(by.id('NetDeviceBrowserPanel(Network Devices)0:0')).isPresent()).toBe(true);
	});

	it('Check System Tab is Clickable', function() {
		console.log('\nMain Tabs: Clicking on System Tab.');
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toMatch('systempage');
		expect(element(by.id('NetworkConfigPanel(Equipment Configuration)0:0')).isPresent()).toBe(true);
	});

	it('Check Dashboard Tab is Clickable', function() {
		console.log('\nMain Tabs: Clicking on Dashboard Tab.');
		element(by.css('#gnTopMenu > rect:nth-child(9)')).click();
		browser.sleep(3000);
		expect(browser.getCurrentUrl()).toMatch('systemsummary');
		expect(element(by.id('ExecutiveSummaryPanel0:0')).isPresent()).toBe(true);
	});
});