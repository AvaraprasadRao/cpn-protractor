//test_menu_deviceSummary.js

describe('Device Summary Menu Suite', function() {

	function waitForElementTextToChange(elem, currText) {
		return browser.wait(function() {
			return elem.getText().then(function (text) {
					return text !== currText;
				},
				function() {
					return elem.getText().then(function (text) {
						return text !== currText;
					});
				}
			);
		});
	}
	function waitElementChange(elem) {
		elem.getText().then(function(text) {
			waitForElementTextToChange(elem, text);
		});
	}

	//Device Summary Page
	it('Check sum of all Identified Devices',function() {
		console.log('\n\nDevice Summary Menu: Sum up all \"Identified\" Devices, check if it equals total.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(22)')).click();
		
		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));

		element(by.css('[id^="DeviceChartPanel"] svg > g:nth-child(1) svg > text.textSlimSelectable')).getText().then(function(total) {
			var sum = 0;
			console.log("Total Identified Devices Number found: "+parseInt(total, 10));
			expect(parseInt(total, 10)).toBeGreaterThan(-1);

			element(by.css('[id^="DeviceChartPanel"] text:nth-child(13)')).getText().then(function(iot) {
				console.log("IoT Devices Number found: "+parseInt(iot, 10));
				sum += parseInt(iot, 10);
				expect(parseInt(iot, 10)).toBeGreaterThan(-1);

				element(by.css('[id^="DeviceChartPanel"] text:nth-child(18)')).getText().then(function(nonIot) {
					console.log("Non-IoT Devices Number found: "+parseInt(nonIot, 10));
					sum += parseInt(nonIot, 10);
					expect(parseInt(nonIot, 10)).toBeGreaterThan(-1);

					element(by.css('[id^="DeviceChartPanel"] g > g > svg > text:nth-child(23)')).getText().then(function(nonAnalyzed) {
						console.log("Non-Analyzed Devices Number found: "+parseInt(nonAnalyzed, 10));
						sum += parseInt(nonAnalyzed, 10);
						expect(parseInt(nonAnalyzed, 10)).toBeGreaterThan(-1);
						expect(sum).toEqual(parseInt(total, 10));
					});
				});
			});
		});
	});

	it('Click on Identified Device Numbers to lead to respective Device Lists', function() {
		console.log('\nDevice Summary Menu: Checking \"Identified\" Devices Graph.');
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));

		let total =  element(by.css('[id^="DeviceChartPanel"] svg > g:nth-child(1) svg > text.textSlimSelectable'));
		total.getText().then(function(num) {
			if(parseInt(num, 10) > 0){
				scrollToView(total);
				browser.actions().mouseMove(total).click().perform();
				browser.sleep(1000);
				element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
					console.log("Expect \""+devices+"\" to contain \""+num+"\"");
					expect(devices).toContain(num);
					expect(devices).toContain('Total');
					selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
				});
			}
			else {
				console.log('There are 0 Identified Devices');
			}
		});

		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));

		let iotDevices = element(by.css('[id^="DeviceChartPanel"] text:nth-child(13)'));
		iotDevices.getText().then(function(num) {
			if(parseInt(num, 10) > 0){
				element(by.css('[id^="DeviceChartPanel"] svg > g > g > svg > rect:nth-child(15)')).click();
				browser.sleep(1000);
				element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
					console.log("Expect \""+devices+"\" to contain \""+num+"\"");
					expect(devices).toContain((num+' out of'));
					expect(devices).toContain('IoT Devices');
					//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
					selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
				});
			}
			else {
				console.log('There are 0 IoT Devices');
			}
		});

		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));

		let noniotDevices = element(by.css('[id^="DeviceChartPanel"] text:nth-child(18)'));
		noniotDevices.getText().then(function(num) {
			if(parseInt(num, 10) > 0){
				element(by.css('[id^="DeviceChartPanel"] svg > g > g > svg > rect:nth-child(20)')).click();
				browser.sleep(1000);
				element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
					console.log("Expect \""+devices+"\" to contain \""+num+"\"");
					expect(devices).toContain((num+' out of'));
					expect(devices).toContain('Non-IoT Devices');
					//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
					selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
				});
			}
			else {
				console.log('There are 0 Non-IoT Devices');
			}
		});

		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));

		let notAnalyzed = element(by.css('[id^="DeviceChartPanel"] g > g > svg > text:nth-child(23)'));
		notAnalyzed.getText().then(function(num) {
			if(parseInt(num, 10) > 0) {
				element(by.css('[id^="DeviceChartPanel"] rect:nth-child(25)')).click();
				browser.sleep(1000);
				element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
					console.log("Expect \""+devices+"\" to contain \""+num+"\"");
					expect(devices).toContain((num+' out of'));
					//expect(devices).toContain('Non-IoT Devices');
					//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
					selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
				});
			}
			else {
				console.log('There are 0 Non-Analyzed Devices');
			}
		});
	});

	it('Check sum of all In Use Devices',function() {
		console.log('\nDevice Summary Menu: Sum up all \"In Use\" Devices, check if it equals total.');
		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));

		element(by.css('[id^="DeviceChartPanel"] svg > g:nth-child(1) svg > text.textSlimSelectable')).getText().then(function(total) {
			var sum = 0;
			console.log("Total In Use Devices Number found: "+parseInt(total, 10));
			expect(parseInt(total, 10)).toBeGreaterThan(-1);

			element(by.css('[id^="DeviceChartPanel"] g:nth-child(2) > g > svg > text:nth-child(10)')).getText().then(function(online) {
				console.log("Online Devices Number found: "+parseInt(online, 10));
				sum += parseInt(online, 10);
				expect(parseInt(online, 10)).toBeGreaterThan(-1);

				element(by.css('[id^="DeviceChartPanel"] g:nth-child(2) > g > svg > text:nth-child(15)')).getText().then(function(offline) {
					console.log("Offline Devices Number found: "+parseInt(offline, 10));
					sum += parseInt(offline, 10);
					expect(parseInt(offline, 10)).toBeGreaterThan(-1);
					expect(sum).toEqual(parseInt(total, 10));
				});
			});
		});
	});

	it('Click on Identified Device Numbers to lead to respective Device Lists', function() {
		console.log('\nDevice Summary Menu: Checking \"In Use\" Graph.');
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));

		let onlineDevices = element(by.css('[id^="DeviceChartPanel"] g:nth-child(2) > g > svg > text:nth-child(10)'));
		onlineDevices.getText().then(function(num) {
			if(parseInt(num, 10) > 0){
				element(by.css('[id^="DeviceChartPanel"] rect:nth-child(12)')).click();
				browser.sleep(1000);
				element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
					console.log("Expect \""+devices+"\" to contain \""+num+"\"");
					expect(devices).toContain((num+' out of'));
					expect(devices).toContain('connected');
					//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
					selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
				});
			}
			else {
				console.log('There are 0 Online Devices');
			}
		});

		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));

		let offlineDevices = element(by.css('[id^="DeviceChartPanel"] g:nth-child(2) > g > svg > text:nth-child(15)'));
		offlineDevices.getText().then(function(num) {
			if(parseInt(num, 10) > 0){
				element(by.css('[id^="DeviceChartPanel"] rect:nth-child(17)')).click();
				browser.sleep(1000);
				element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
					console.log("Expect \""+devices+"\" to contain \""+num+"\"");
					expect(devices).toContain((num+' out of'));
					expect(devices).toContain('offline');
					//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
					selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
				});
			}
			else {
				console.log('There are 0 Offline Devices');
			}
		});
	});

	it('Clicking Device Update List Numbers', function() {
		console.log('\nDevice Summary Menu: Checking Device Update Section.');
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		//wait for Summary Refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));

		let newDaily = element(by.css('#deviceUpdateList text:nth-child(12)'));
		newDaily.getText().then(function(num) {
			if(parseInt(num, 10) > 0){
				element(by.css('#deviceUpdateList rect:nth-child(13)')).click();
				browser.sleep(1000);
				element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
					console.log("Expect \""+devices+"\" to contain \""+num+"\"");
					expect(devices).toContain((num+' out of'));
					expect(devices).toContain('in last 24 hours');
					//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
					selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
				});
			}
			else {
				console.log('There are 0 New Devices in the last 24 hours');
			}
		});

		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));

		let newWeekly = element(by.css('#deviceUpdateList text:nth-child(19)'));
		newWeekly.getText().then(function(num) {
			if(parseInt(num, 10) > 0){
				element(by.css('#deviceUpdateList rect:nth-child(20)')).click();
				browser.sleep(1000);
				element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
					console.log("Expect \""+devices+"\" to contain \""+num+"\"");
					expect(devices).toContain((num+' out of'));
					expect(devices).toContain('in last 14 days');
					//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
					selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
				});
			}
			else {
				console.log('There are 0 New Devices in the last 14 days');
			}
		});
	});

	it('Clicking Usage Update Numbers', function() {
		console.log('\nDevice Summary Menu: Checking Usage Update Section.');
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));

		let offlineDay = element(by.css('#usageUpdateList text:nth-child(12)'));
		offlineDay.getText().then(function(num) {
			if(parseInt(num, 10) > 0){
				element(by.css('#usageUpdateList rect:nth-child(13)')).click();
				browser.sleep(1000);
				element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
					console.log("Expect \""+devices+"\" to contain \""+num+"\"");
					expect(devices).toContain((num+' out of'));
					expect(devices).toContain('offline for more than 24 hours');
					//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
					selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
				});
			}
			else {
				console.log('There are 0 New Devices in the last 24 hours');
			}
		});

		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));

		let offlineWeek = element(by.css('#usageUpdateList text:nth-child(19)'));
		offlineWeek.getText().then(function(num) {
			if(parseInt(num, 10) > 0){
				element(by.css('#usageUpdateList rect:nth-child(20)')).click();
				browser.sleep(1000);
				element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
					console.log("Expect \""+devices+"\" to contain \""+num+"\"");
					expect(devices).toContain((num+' out of'));
					expect(devices).toContain('offline for more than 7 days');
					//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
					selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
				});
			}
			else {
				console.log('There are 0 New Devices in the last 7 days');
			}
		});

		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));

		let certExpire = element(by.css('#usageUpdateList text:nth-child(12)'));
		presentElementClick(element(by.css('#usageUpdateList path[opacity="0.5"]+circle')), '', function() {
			browser.sleep(1000);
			certExpire.getText().then(function(num) {
				if(parseInt(num, 10) > 0){
					element(by.css('#usageUpdateList rect:nth-child(13)')).click();
					browser.sleep(1000);
					element(by.css('[id*="DeviceBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > text')).getText().then(function(devices) {
						console.log("Expect \""+devices+"\" to contain \""+num+"\"");
						expect(devices).toContain((num+' out of'));
						expect(devices).toContain('cert expiring in 365 days');
						//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
						selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
					});
				}
				else {
					console.log('There are 0 Devices with a Certification Expiring in a year');
				}
			});
		});
	});

	it('Clicking Group Membership Section', function() {
		console.log('\nDevice Summary Menu: Clicking Group Membership Section leads to respective page.');
		//wait for Summary refresh
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));
		//Row 1
		presentElementText(element(by.css('#topNGroupsList text:nth-child(9)')), 'Row 1 no text', function() {
			expect(element(by.css('#topNGroupsList text:nth-child(10)')).getText()).toBe('1');
			element(by.css('#topNGroupsList text:nth-child(12)')).getText().then(function(num) {
				element(by.css('#topNGroupsList text:nth-child(9)')).getText().then(function(group) {
					console.log("Clicking Row 1 - \""+group+"\" Classification Group");
					element(by.css('#topNGroupsList rect:nth-child(13)')).click();
					browser.sleep(2000);
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
					//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
					selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
					//browser.sleep(2000);
				});
			});
		});

		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));
		//Row 2
		presentElementText(element(by.css('#topNGroupsList text:nth-child(16)')), 'Row 2 no text', function() {
			expect(element(by.css('#topNGroupsList text:nth-child(17)')).getText()).toBe('2');
			element(by.css('#topNGroupsList text:nth-child(19)')).getText().then(function(num) {
				element(by.css('#topNGroupsList text:nth-child(16)')).getText().then(function(group) {
					console.log("Clicking Row 2 - \""+group+"\" Classification Group");
					element(by.css('#topNGroupsList rect:nth-child(20)')).click();
					browser.sleep(2000);
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
					//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
					selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
					//browser.sleep(2000);
				});
			});
		});

		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));
		//Row 3
		presentElementText(element(by.css('#topNGroupsList text:nth-child(23)')), 'Row 3 no text', function() {
			expect(element(by.css('#topNGroupsList text:nth-child(24)')).getText()).toBe('3');
			element(by.css('#topNGroupsList text:nth-child(26)')).getText().then(function(num) {
				element(by.css('#topNGroupsList text:nth-child(23)')).getText().then(function(group) {
					console.log("Clicking Row 3 - \""+group+"\" Classification Group");
					element(by.css('#topNGroupsList rect:nth-child(27)')).click();
					browser.sleep(2000);
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
					//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
					selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
					//browser.sleep(2000);
				});
			});
		});

		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));
		//Row 4
		presentElementText(element(by.css('#topNGroupsList text:nth-child(30)')), 'Row 4 no text', function() {
			expect(element(by.css('#topNGroupsList text:nth-child(31)')).getText()).toBe('4');
			element(by.css('#topNGroupsList text:nth-child(33)')).getText().then(function(num) {
				element(by.css('#topNGroupsList text:nth-child(30)')).getText().then(function(group) {
					console.log("Clicking Row 4 - \""+group+"\" Classification Group");
					element(by.css('#topNGroupsList rect:nth-child(34)')).click();
					browser.sleep(2000);
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
					//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
					selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
					//browser.sleep(2000);
				});
			});
		});

		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));
		//Row 5
		presentElementText(element(by.css('#topNGroupsList text:nth-child(37)')), 'Row 5 no text', function() {
			expect(element(by.css('#topNGroupsList text:nth-child(38)')).getText()).toBe('5');
			element(by.css('#topNGroupsList text:nth-child(40)')).getText().then(function(num) {
				element(by.css('#topNGroupsList text:nth-child(37)')).getText().then(function(group) {
					console.log("Clicking Row 5 - \""+group+"\" Classification Group");
					element(by.css('#topNGroupsList rect:nth-child(41)')).click();
					browser.sleep(2000);
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
					expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
					//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
					selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
					//browser.sleep(2000);
				});
			});
		});

		//wait for Summary refresh
		waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));
		//Row 6-10
		presentElementClick(element(by.css('#topNGroupsList path[opacity="0.5"]+circle')), 'Row 6-10 unavailable', function() {
			browser.sleep(2000);
			
			//Row 6
			presentElementText(element(by.css('#topNGroupsList text:nth-child(9)')), 'Row 6 no text', function() {
				expect(element(by.css('#topNGroupsList text:nth-child(10)')).getText()).toBe('6');
				element(by.css('#topNGroupsList text:nth-child(12)')).getText().then(function(num) {
					element(by.css('#topNGroupsList text:nth-child(9)')).getText().then(function(group) {
						console.log("Clicking Row 6 - \""+group+"\" Classification Group");
						element(by.css('#topNGroupsList rect:nth-child(13)')).click();
						browser.sleep(2000);
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
						//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
						selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
						//browser.sleep(2000);
					});
				});
			});

			//wait for Summary refresh
			waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));
			//Row 7
			presentElementText(element(by.css('#topNGroupsList text:nth-child(16)')), 'Row 7 no text', function() {
				expect(element(by.css('#topNGroupsList text:nth-child(17)')).getText()).toBe('7');
				element(by.css('#topNGroupsList text:nth-child(19)')).getText().then(function(num) {
					element(by.css('#topNGroupsList text:nth-child(16)')).getText().then(function(group) {
						console.log("Clicking Row 7 - \""+group+"\" Classification Group");
						element(by.css('#topNGroupsList rect:nth-child(20)')).click();
						browser.sleep(2000);
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
						//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
						selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
						//browser.sleep(2000);
					});
				});
			});

			//wait for Summary refresh
			waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));
			//Row 8
			presentElementText(element(by.css('#topNGroupsList text:nth-child(23)')), 'Row 8 no text', function() {
				expect(element(by.css('#topNGroupsList text:nth-child(24)')).getText()).toBe('8');
				element(by.css('#topNGroupsList text:nth-child(26)')).getText().then(function(num) {
					element(by.css('#topNGroupsList text:nth-child(23)')).getText().then(function(group) {
						console.log("Clicking Row 8 - \""+group+"\" Classification Group");
						element(by.css('#topNGroupsList rect:nth-child(27)')).click();
						browser.sleep(2000);
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
						//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
						selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
						//browser.sleep(2000);
					});
				});
			});

			//wait for Summary refresh
			waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));
			//Row 9
			presentElementText(element(by.css('#topNGroupsList text:nth-child(30)')), 'Row 9 no text', function() {
				expect(element(by.css('#topNGroupsList text:nth-child(31)')).getText()).toBe('9');
				element(by.css('#topNGroupsList text:nth-child(33)')).getText().then(function(num) {
					element(by.css('#topNGroupsList text:nth-child(30)')).getText().then(function(group) {
						console.log("Clicking Row 9 - \""+group+"\" Classification Group");
						element(by.css('#topNGroupsList rect:nth-child(34)')).click();
						browser.sleep(2000);
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
						//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
						selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
						//browser.sleep(2000);
					});
				});
			});

			//wait for Summary refresh
			waitElementChange(element(by.css('[id^=DeviceChartPanel] > text:nth-child(2)')));
			//Row 10
			presentElementText(element(by.css('#topNGroupsList text:nth-child(37)')), 'Row 10 no text', function() {
				expect(element(by.css('#topNGroupsList text:nth-child(38)')).getText()).toBe('10');
				element(by.css('#topNGroupsList text:nth-child(40)')).getText().then(function(num) {
					element(by.css('#topNGroupsList text:nth-child(37)')).getText().then(function(group) {
						console.log("Clicking Row 10 - \""+group+"\" Classification Group");
						element(by.css('#topNGroupsList rect:nth-child(41)')).click();
						browser.sleep(2000);
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain((num+' out of'));
						expect(element(by.css('[id^="DeviceBrowserPanel"] > g > g:nth-child(6) > text')).getText()).toContain(group);
						//element(by.css('g:nth-child(2) > g > g:nth-child(5) > rect:nth-child(4)')).click();
						selectSideMenu('Device Summary', sideMenuClicks, sideMenuTexts);
						//browser.sleep(2000);
					});
				});
			});
		});
	});
});