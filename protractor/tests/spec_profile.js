//test_profile.js
describe('Profile Tab Suite', function() {

	function checkBrokenImages() {
		browser.executeAsyncScript(function (callback) {
			var imgs = document.getElementsByTagName('img');
			var loaded = 0;
			for (var i = 0; i < imgs.length; i++) {
				if (imgs[i].naturalWidth > 0) {
					loaded = loaded + 1;
				}
			}
			callback(imgs.length - loaded);
		}).then(function (brokenImagesCount) {
			if(brokenImagesCount > 0) {
				console.log('There are '+brokenImagesCount+' broken images');
			}
			expect(brokenImagesCount).toBe(0);
		});
	}

	function changeFilter(filterText) {
		element(by.css('[id*=BPCTxtField]')).clear();
		browser.sleep(6000);
		element(by.css('[id*=BPCTxtField]')).sendKeys(filterText);
		browser.sleep(8000);
	};

	function checkProfileCount(filterText, expectedProfiles) {
		var total = element(by.css('[id^="ProfileBrowserPanel"] > g:nth-child(4) > g:nth-child(5) > text')).getText().then(function(text) {
		//var total = element(by.css('[id^="ProfileBrowserPanel"] > g:nth-child(4) > g:nth-child(4) > text')).getText().then(function(text) {
			var num = text.match(/\d/g);
			num = num.join("");
			console.log(filterText+' Profiles found: '+num);
			return parseInt(num, 10);
		});
		expect(total).toBeGreaterThan(expectedProfiles);
	}

	it('Profile Total Profiles', function() {
		console.log('\n\nProfile Menu: Icon View - All Profiles.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(18)')).click();
		browser.sleep(3000);
		element(by.css('[id^="ProfileBrowserPanel"] > g:nth-child(4) > g:nth-child(8) > rect:nth-child(7)')).click();
		//element(by.css('[id^="ProfileBrowserPanel"] > g:nth-child(4) > g:nth-child(7) > rect:nth-child(7)')).click();
		browser.sleep(3000);
		checkProfileCount('Total', 5000);
		checkBrokenImages();
	});

	it('Profile Icon View Medical Filter Check', function() {
		console.log('\nProfile Menu: Icon View - Medical filter.');
		changeFilter('Medical');
		checkProfileCount('Medical', 2000);
		checkBrokenImages();
	});

	it('Profile Icon View Camera Filter Check', function() {
		console.log('\nProfile Menu: Icon View - Camera filter.');
		changeFilter('Camera');
		checkProfileCount('Camera', 300);
		checkBrokenImages();
	});

	it('Profile Icon View Phone Filter Check', function() {
		console.log('\nProfile Menu: Icon View - Phone filter.');
		changeFilter('Phone');
		checkProfileCount('Phone', 400);
		checkBrokenImages();
	});

	it('Profile Icon View Printer Filter Check', function() {
		console.log('\nProfile Menu: Icon View - Printer filter.');
		changeFilter('Printer');
		checkProfileCount('Printer', 1500);
		checkBrokenImages();
	});

	it('Profile Icon View TV Filter Check', function() {
		console.log('\nProfile Menu: Icon View - TV filter.');
		changeFilter('TV');
		checkProfileCount('TV', 10);
		checkBrokenImages();
	});

	it('Profile Icon View Philips Manufacturer Filter Check', function() {
		console.log('\nProfile Menu: Icon View - Philips filter.');
		changeFilter('Philips');
		checkProfileCount('Philips', 200);
		checkBrokenImages();
	});

	it('Profile List View Media Devices filter', function() {
		console.log('\nProfile Menu: List View - Media Devices filter.');
		element(by.css('[id^="ProfileBrowserPanel"] > g:nth-child(4) > g:nth-child(6) > rect:nth-child(7)')).click();
		browser.sleep(3000);
		changeFilter('Media Devices');
		checkProfileCount('Media', 50);
	});

	it('Profile List View Physical Security Devices filter', function() {
		console.log('\nProfile Menu: List View - Physical Security Devices filter.');
		changeFilter('Physical Security Devices');
		checkProfileCount('Physical Security', 400);
	});

	it('Profile Name Sorting Check', function() {
		console.log('\nProfile Menu: Sorting by Name.');
		changeFilter('');
		browser.sleep(3000);
		element.all(by.css('#topCards > g svg > g > path+rect')).get(0).click();
		browser.sleep(3000);
		var firstProfileName = element.all(by.css('#topCards [id] > svg > svg:nth-child(17) > text')).get(0).getText().then(function(text) {
			console.log("Clicking Up Arrow Filter - Top Row Profile Name: "+text)
			return text;
		});

		element.all(by.css('#topCards > g svg > g > path+rect')).get(1).click();
		browser.sleep(3000);

		var lastProfileName = element.all(by.css('#topCards [id] > svg > svg:nth-child(17) > text')).get(0).getText().then(function(text) {
			console.log("Clicking Down Arrow Filter - Top Row Profile Name: "+text);
			return text;
		});
		expect(firstProfileName).not.toBe(lastProfileName);
	});

	it('Profile Group Sorting Check', function() {
		console.log('\nProfile Menu: Sorting by Group.');
		browser.sleep(3000);
		element.all(by.css('#topCards > g svg > g > path+rect')).get(2).click();
		browser.sleep(3000);
		var firstGroupName = element.all(by.css('#topCards [id] > svg > svg:nth-child(18) > text')).get(0).getText().then(function(text) {
			console.log("Clicking Up Arrow Filter - Top Row Group Name: "+text)
			return text;
		});

		element.all(by.css('#topCards > g svg > g > path+rect')).get(3).click();
		browser.sleep(3000);

		var lastGroupName = element.all(by.css('#topCards [id] > svg > svg:nth-child(18) > text')).get(0).getText().then(function(text) {
			console.log("Clicking Down Arrow Filter - Top Row Group Name: "+text);
			return text;
		});

		expect(firstGroupName).not.toBe(lastGroupName);
	});

	it('Profile Classification Specific Profile Information Check', function() {
		console.log('\nProfile Menu: Check Specific Details of a AXIS Profile.');
		element(by.css('[id^="ProfileBrowserPanel"] > g:nth-child(4) > g:nth-child(8) > rect:nth-child(7)')).click();
		browser.sleep(3000);
		var searchValues = {
			"name": /Axis|P3214|P3364|Network|Camera/i,
			"manu": /Axis/i,
			"cat": /Physical Security Devices/i,
			"type": /Network|Camera/i,
			"ost": /Linux|3\.2|4\.8/i,
			"mode": /Learning|Enabled/i,
			"group": /Physical Security Devices/i
		};

		changeFilter('AXIS');
		presentElementClick(element.all(by.css('#topCards [id] > svg > rect:nth-child(15)[fill]')).get(0), 'No AXIS Profiles Present',
			function() {
				console.log('Clicking AXIS Profile');
				browser.sleep(5000).then(function(){console.log('\nInformation:');});

				let profileInformation = element.all(by.css('[id^="ProfileDetailPanel"] g:nth-child(1) > svg > text'));
				let profileModeScope = element.all(by.css('[id^="ProfileDetailPanel"] g:nth-child(2) > svg > text'));

				findDetail(profileInformation, searchValues["name"], 'Profile Name');
				findDetail(profileInformation, searchValues["manu"], 'Manufacturer');
				findDetail(profileInformation, searchValues["cat"], 'Device Category');
				findDetail(profileInformation, searchValues["type"], 'Device Type');
				findDetail(profileInformation, searchValues["ost"], 'OS Type');

				browser.sleep(1000).then(function(){console.log('\nMode and Scope:');});

				findDetail(profileModeScope, searchValues["mode"], 'Mode');
				findDetail(profileModeScope, searchValues["group"], 'Group Name');
			}
		, function() {fail('no AXIS profile is clickable');});
	});

	// it('Profile Classification Clicking Icons', function() {
	// 	console.log('\nProfile Menu: Clicking all Icons.');
	// 	scrollToView(element(by.css('[id^="ProfileBrowserPanel(Classification Profiles)0"]')));
	// 	browser.sleep(4000);
	// 	//Clicking Device Members Icon
	// 	presentElementClick(element.all(by.css('#topCards [id] > svg > rect:nth-child(29)[fill]')).first(),
	// 		'Device Members Icon not present on Current Page',
	// 		function() {
	// 			console.log('Clicking Device Members Icon');
	// 			browser.sleep(4000);
	// 			expect(browser.getCurrentUrl()).toMatch('devicepage');
	// 			expect(element.all(by.css('#topCards svg > path~rect:nth-child(3)[x="0"]')).count()).toBeGreaterThan(0);
	// 			element(by.css('#gnTopMenu > rect:nth-child(17)')).click();
	// 			browser.sleep(2000);
	// 		}
	// 	);

	// 	browser.sleep(2000);
	// 	//Clicking Flow Genome Icon
	// 	presentElementClick(element.all(by.css('#topCards [id] > svg > rect:nth-child(30)[fill]')).first(),
	// 		'Flow Genome Icon not present on Current Page',
	// 		function() {
	// 			console.log('Clicking Flow Genome Icon');
	// 			browser.sleep(4000);
	// 			expect(element(by.css('[id="SankeyDiagram"]')).isPresent()).toBe(true);
	// 			scrollToView(element(by.css('[id^="ProfileBrowserPanel(Classification Profiles)0"]')));
	// 			browser.sleep(2000);
	// 		}
	// 	);

	// 	browser.sleep(2000);
	// 	//Clicking Application Usage Icon
	// 	presentElementClick(element.all(by.css('#topCards [id] > svg > rect:nth-child(31)[fill]')).first(),
	// 		'Application Usage Icon not present on Current Page',
	// 		function() {
	// 			console.log('Clicking Application Usage Icon');
	// 			browser.sleep(4000);
	// 			expect(browser.getCurrentUrl()).toMatch('apppage');
	// 			expect(element(by.css('[id="AppSunburstDiagram"]')).isPresent()).toBe(true);
	// 			element(by.css('#gnTopMenu > rect:nth-child(17)')).click();
	// 			browser.sleep(2000);
	// 			scrollToView(element(by.css('[id^="ProfileBrowserPanel(Classification Profiles)0"]')));
	// 			browser.sleep(2000);
	// 		}
	// 	);

	// 	browser.sleep(2000);
	// 	//Clicking Vulnerabilities Icon
	// 	presentElementClick(element.all(by.css('#topCards [id] > svg > rect:nth-child(32)[fill]')).first(),
	// 		'Known Vulnerabilities Icon not present on Current Page',
	// 		function() {
	// 			console.log('Clicking Known Vulnerabilities Icon');
	// 			browser.sleep(4000);
	// 			expect(element(by.css('[id^="ProfileDetailPanel"]')).isPresent()).toBe(true);
	// 			expect(element(by.css('[id^="ProfileDetailPanel"] > g:nth-child(3) > svg > g:nth-child(3) > text')).getText()).toMatch(/[\d]+ vulnerability|[\d]+ vulnerabilities/i);
	// 			browser.sleep(2000);
	// 		}
	// 	);
	// });

	it('Vlan Profiles Table Sorting Check', function() {
		console.log('\n\nVlan Profiles: Sorting by Name.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(18)')).click();
		browser.sleep(3000);
		//element(by.css('g:nth-child(2) > g > g:nth-child(6) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('Vlan Profiles', sideMenuClicks, sideMenuTexts);
		browser.sleep(3000);
		expect(element(by.id('VlanSubnetProfileBrowserPanel(Vlan Profiles)0:0')).isPresent()).toBe(true);

		element.all(by.css('#topCards [id] > svg > rect:nth-child(3)[fill]')).count().then(function(count) {
			if(count > 1) {
				element.all(by.css('#topCards > g svg > g > path+rect')).get(0).click();
				browser.sleep(3000);
				var firstProfileName = element.all(by.css('#topCards [id] > svg > svg:nth-child(5) > text')).get(0).getText().then(function(text) {
					console.log("Clicking Up Arrow Filter - Top Row Profile Name: "+text)
					return text;
				});

				element.all(by.css('#topCards > g svg > g > path+rect')).get(1).click();
				browser.sleep(3000);

				var lastProfileName = element.all(by.css('#topCards [id] > svg > svg:nth-child(5) > text')).get(0).getText().then(function(text) {
					console.log("Clicking Down Arrow Filter - Top Row Profile Name: "+text);
					return text;
				});
				expect(firstProfileName).not.toBe(lastProfileName);
			}
			else {fail('not enough vlan profiles for sorting');console.log('Less than 2 vlan profiles available, no way to test sorting');}
		});
	});

	it('Vlan Profiles Table Filter Check', function() {
		console.log('\nVlan Profiles: Filter Functionality.');
		browser.sleep(3000);
		presentElementText(element.all(by.css('#topCards [id] > svg > svg:nth-child(8) > text')).first(), 'No Group Text in Table', function() {
			element.all(by.css('#topCards [id] > svg > svg:nth-child(8) > text')).first().getText().then(function(text) {
				changeFilter(text);
				var profile = element(by.css('[id^="VlanSubnetProfileBrowserPanel"] > g:nth-child(4) > g:nth-child(4) > text')).getText().then(function(prof) {
					var num = prof.match(/\d/g).join("");
					return parseInt(num, 10);
				});
				expect(profile).toBeGreaterThan(0);
				expect(profile).toBeLessThan(5);
			});
		})
	});

	it('Subnet Profiles Table Sorting Check', function() {
		console.log('\n\nSubnet Profiles: Sorting by Name.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(18)')).click();
		browser.sleep(3000);
		//element(by.css('g:nth-child(2) > g > g:nth-child(7) > rect:nth-child(4)')).click();
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('Subnet Profiles', sideMenuClicks, sideMenuTexts);
		browser.sleep(3000);
		expect(element(by.id('VlanSubnetProfileBrowserPanel(Subnet Profiles)0:0')).isPresent()).toBe(true);

		element.all(by.css('#topCards [id] > svg > rect:nth-child(3)[fill]')).count().then(function(count) {
			if(count > 1) {
				element.all(by.css('#topCards > g svg > g > path+rect')).get(0).click();
				browser.sleep(3000);
				var firstProfileName = element.all(by.css('#topCards [id] > svg > svg:nth-child(5) > text')).get(0).getText().then(function(text) {
					console.log("Clicking Up Arrow Filter - Top Row Profile Name: "+text)
					return text;
				});

				element.all(by.css('#topCards > g svg > g > path+rect')).get(1).click();
				browser.sleep(3000);

				var lastProfileName = element.all(by.css('#topCards [id] > svg > svg:nth-child(5) > text')).get(0).getText().then(function(text) {
					console.log("Clicking Down Arrow Filter - Top Row Profile Name: "+text);
					return text;
				});
				expect(firstProfileName).not.toBe(lastProfileName);
			}
			else {fail('not enough subnet profiles for sorting');console.log('Less than 2 subnet profiles available, no way to test sorting');}
		});
	});

	it('Subnet Profiles Table Filter Check', function() {
		console.log('\nSubnet Profiles: Filter Functionality.');
		browser.sleep(3000);
		presentElementText(element.all(by.css('#topCards [id] > svg > svg:nth-child(8) > text')).first(), 'No Group Text in Table', function() {
			element.all(by.css('#topCards [id] > svg > svg:nth-child(8) > text')).first().getText().then(function(text) {
				changeFilter(text);
				var subnet = element(by.css('[id^="VlanSubnetProfileBrowserPanel"] > g:nth-child(4) > g:nth-child(4) > text')).getText().then(function(prof) {
					var num = prof.match(/\d/g).join("");
					return parseInt(num, 10);
				});
				expect(subnet).toBeGreaterThan(0);
				expect(subnet).toBeLessThan(5);
			});
		})
	});
});