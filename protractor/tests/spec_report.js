//test_report.js
describe('Report Generation Suite', function() {

	function selectReport(reportType, reportFormat) {
		let dropdownTypeText = element.all(by.css('#infoLayerGElement > g > text'));
		let dropdownTypeClick = element.all(by.css('#infoLayerGElement > g > rect'));
		let dropdownFormatText = element.all(by.css('#infoLayerGElement > g > text'));
		let dropdownFormatClick = element.all(by.css('#infoLayerGElement > g > rect'));
		
		browser.sleep(2000);
		element(by.css('#gnTopMenu > rect:nth-child(40)')).click();
		browser.sleep(2000);
		
		element(by.css('#dropDownContentG-reports-type > text')).getText().then(function(currText) {
			if(currText != reportType) {
				element(by.css('#dropDownContentG-reports-type > rect:nth-child(8)')).click();
				browser.sleep(1000);
				dropdownTypeClick.filter(function(elem, index) {
					return dropdownTypeText.get(index).getText().then(function(text) {
						return text === reportType;
					});
				}).first().click();
			}
		});

		element(by.css('#dropDownContentG-reports-format > text')).getText().then(function(currText) {
			if(currText != reportFormat) {
				element(by.css('#dropDownContentG-reports-format > rect:nth-child(8)')).click();
				browser.sleep(1000);
				dropdownFormatClick.filter(function(elem, index) {
					return dropdownFormatText.get(index).getText().then(function(text) {
						return text === reportFormat;
					});
				}).first().click();
			}
		});
	}

	//Generate Device Reports
	it('Check Device HTML Report Generation', function() {
		browser.sleep(3000);
		browser.restart();
		console.log('\n\nReport: Generating Device HTML Report.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);

		//HTML Report
		selectReport('Device', 'HTML');

		element(by.id('GenerateEventRectangle')).click().then(function() {console.log('Loading Device HTML Report');});
		browser.sleep(15000);
		browser.getAllWindowHandles().then(function (handles) {
			browser.sleep(2000);
			browser.switchTo().window(handles[1]).then(function() {
				ignoreSynchronization(function() {
					expect(browser.getTitle()).toEqual('Device List Report');
					expect(element.all(by.css('.report-block')).count()).toBeGreaterThan(1);
					browser.close();
					browser.switchTo().window(handles[0]);
				});				
			});
		});
	});

    it('Check Device PDF Report Generation', function() {
		browser.sleep(3000);
		browser.restart();
		console.log('\nReport: Generating Device PDF Report.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);

		//PDF Report
		selectReport('Device', 'PDF');

		element(by.id('GenerateEventRectangle')).click().then(function() {console.log('Loading Device PDF Report');});
		browser.sleep(15000);
		browser.getAllWindowHandles().then(function (handles) {
			browser.sleep(2000);
			browser.switchTo().window(handles[1]).then(function() {
				ignoreSynchronization(function() {
					expect(handles.length).toEqual(2);
					//browser.sleep(3000);
					//expect(browser.getTitle()).toEqual('report');
					expect(browser.getCurrentUrl()).toMatch('nodejs/report');
					browser.close();
					browser.switchTo().window(handles[0]);
				});				
			});
		});
    });

	it('Check Device CSV Report Generation', function() {
		browser.sleep(3000);
		browser.restart();
		console.log('\nReport: Generating Device CSV Report.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);

		//CSV Report
		selectReport('Device', 'CSV');

		element(by.id('GenerateEventRectangle')).click().then(function() {console.log('Loading Device CSV Report');});

		browser.sleep(60000);
		browser.getAllWindowHandles().then(function (handles) {
			if (handles.length > 1) {
				ignoreSynchronization(function() {
					browser.switchTo().window(handles[1]).then(function() {
						fail('Device CSV Report generation stuck');
						browser.sleep(2000);
						browser.close();
						browser.switchTo().window(handles[0]);
						browser.refresh();
					});
				});
			}
			else {
				expect(handles.length).toEqual(1);
			}
		});
	});

    //Generate Threat Reports
	it('Check Threat HTML Report Generation', function() {
		browser.sleep(3000);
		browser.restart();
		console.log('\nReport: Generating Threat HTML Report.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);

		//HTML Report
		selectReport('Threat', 'HTML');

		element(by.id('GenerateEventRectangle')).click().then(function() {console.log('Loading Threat/Incident HTML Report');});
		browser.sleep(7000);
		browser.getAllWindowHandles().then(function (handles) {
			browser.sleep(2000);
			browser.switchTo().window(handles[1]).then(function() {
				ignoreSynchronization(function() {
					expect(browser.getTitle()).toEqual('Incident List Report');
					expect(element.all(by.css('.report-block')).count()).toBeGreaterThan(1);
					browser.close();
					browser.switchTo().window(handles[0]);
				});				
			});
		});
    });

	it('Check Threat PDF Report Generation', function() {
		browser.sleep(3000);
		browser.restart();
		console.log('\nReport: Generating Threat PDF Report.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);

		//PDF Report
		selectReport('Threat', 'PDF');

		element(by.id('GenerateEventRectangle')).click().then(function() {console.log('Loading Threat/Incident PDF Report');});
		browser.sleep(15000);
		browser.getAllWindowHandles().then(function (handles) {
			browser.sleep(2000);
			browser.switchTo().window(handles[1]).then(function() {
				ignoreSynchronization(function() {
					expect(handles.length).toEqual(2);
					//browser.sleep(3000);
					//expect(browser.getTitle()).toEqual('report');
					expect(browser.getCurrentUrl()).toMatch('nodejs/report');
					browser.close();
					browser.switchTo().window(handles[0]);
				});				
			});
		});
    });

	it('Check Threat CSV Report Generation', function() {
		browser.sleep(3000);
		browser.restart();
		console.log('\nReport: Generating Threat CSV Report.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);

		//CSV Report
		selectReport('Threat', 'CSV');

		element(by.id('GenerateEventRectangle')).click().then(function() {console.log('Loading Threat/Incident CSV Report');});

		browser.sleep(60000);
		browser.getAllWindowHandles().then(function (handles) {
			if (handles.length > 1) {
				ignoreSynchronization(function() {
					browser.switchTo().window(handles[1]).then(function() {
						fail('Thread CSV Report generation stuck');
						browser.sleep(2000);
						browser.close();
						browser.switchTo().window(handles[0]);
						browser.refresh();
					});
				});
			}
			else {
				expect(handles.length).toEqual(1);
			}
		});
	});

	// //Generate Application Report
	it('Check Application CSV Report Generation', function() {
		browser.sleep(3000);
		browser.restart();
		console.log('\nReport: Generating Application CSV Report.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);

		//CSV Report
		selectReport('Application', 'CSV');

		element(by.id('GenerateEventRectangle')).click().then(function() {console.log('Loading Application Usage CSV Report');});

		browser.sleep(120000);
		browser.getAllWindowHandles().then(function (handles) {
			if (handles.length > 1) {
				ignoreSynchronization(function() {
					browser.switchTo().window(handles[1]).then(function() {
						fail('Application CSV Report generation stuck');
						browser.sleep(2000);
						browser.close();
						browser.switchTo().window(handles[0]);
						browser.refresh();
					});
				});
			}
			else {
				expect(handles.length).toEqual(1);
			}
		});
	});
});