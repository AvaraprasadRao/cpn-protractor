//test_menu_deviceCommunications.js

describe('Device Communications Menu Suite', function() {

	it('Check Endpoint Internal Communications from bottom filtered section', function() {
		console.log('\nDevice Internal Communications: From last endpoint to first endpoint, clicking a few endpoints.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(22)')).click();
		browser.sleep(8000);
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));

		//element.all(by.css('#sideMenu~g>text~rect')).get(3).click();
		selectSideMenu('Int. Communication', sideMenuClicks, sideMenuTexts);
		browser.sleep(30000);
		element(by.css('#topCards svg > g:nth-child(14) > rect')).click();
		browser.sleep(5000);
		//element(by.css('[id^="InternalPeerBrowserPanel"] g:nth-child(4) > rect:nth-child(7)')).click();
		//browser.sleep(3000);
		let internalComms = element.all(by.css('#topCards svg > rect:nth-child(3)[fill]'));

		internalComms.each(function(elem, index) {
			if(index < 10) {
				scrollToView(internalComms.get(index));
				browser.sleep(1000);
				presentElementClick(internalComms.get(index), '', function() {
					browser.sleep(4000);
					scrollToView(element(by.css('[id^="EndpointDetailPanel"][id$="0:0"]')));
					let endpointComms = element.all(by.css('[id^="EndpointDetailPanel"] svg > g > g > svg > g > g'));
					expect(endpointComms.count()).toBeGreaterThan(0);
					browser.sleep(1000);
					//element(by.css('[id^="EndpointDetailPanel"] g:nth-child(5) > rect:nth-child(7)')).click();
					//browser.sleep(1000);
				},
				function() {fail('Internal Endpoint Communications Unclickable')}, -5, 0);
			}
		});
	});

	it('Check Endpoint Internal Communications from top filtered section', function() {
		console.log('\nDevice Internal Communications: From first endpoint to last endpoint, clicking a few endpoints.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(22)')).click();
		browser.sleep(2000);
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));

		//element.all(by.css('#sideMenu~g>text~rect')).get(3).click();
		selectSideMenu('Int. Communication', sideMenuClicks, sideMenuTexts);
		browser.sleep(30000);
		element(by.css('#topCards svg > g:nth-child(13) > rect')).click();
		browser.sleep(5000);
		//element(by.css('[id^="InternalPeerBrowserPanel"] g:nth-child(4) > rect:nth-child(7)')).click();
		//browser.sleep(3000);
		let internalComms = element.all(by.css('#topCards svg > rect:nth-child(3)[fill]'));

		internalComms.each(function(elem, index) {
			if(index < 10) {
				scrollToView(internalComms.get(index));
				browser.sleep(1000);
				presentElementClick(internalComms.get(index), '', function() {
					browser.sleep(5000);
					scrollToView(element(by.css('[id^="EndpointDetailPanel"][id$="0:0"]')));
					let endpointComms = element.all(by.css('[id^="EndpointDetailPanel"] svg > g > g > svg > g > g'));
					expect(endpointComms.count()).toBeGreaterThan(0);
					browser.sleep(1000);
					//element(by.css('[id^="EndpointDetailPanel"] g:nth-child(5) > rect:nth-child(7)')).click();
					//browser.sleep(1000);
				},
				function() {fail('Internal Endpoint Communications Unclickable')}, -5, 0);
			}
		});
	});

	it('Check Endpoint External Communications from bottom filtered section', function() {
		console.log('\n\nDevice External Communications: From last endpoint to first endpoint, clicking a few endpoints.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(22)')).click();
		browser.sleep(8000);
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));

		//element.all(by.css('#sideMenu~g>text~rect')).get(3).click();
		selectSideMenu('Ext. Communication', sideMenuClicks, sideMenuTexts);
		browser.sleep(120000);
		element(by.css('#topCards svg > g:nth-child(18) > rect')).click();
		browser.sleep(5000);
		//element(by.css('[id^="InternalPeerBrowserPanel"] g:nth-child(4) > rect:nth-child(7)')).click();
		//browser.sleep(3000);
		let externalComms = element.all(by.css('#topCards svg > rect:nth-child(3)[fill]'));

		externalComms.each(function(elem, index) {
			if(index < 10) {
				scrollToView(externalComms.get(index));
				browser.sleep(1000);
				presentElementClick(externalComms.get(index), '', function() {
					browser.sleep(4000);
					scrollToView(element(by.css('[id^="EndpointDetailPanel"][id$="0:0"]')));
					let endpointComms = element.all(by.css('[id^="EndpointDetailPanel"] svg > g > g > svg > g > g'));
					expect(endpointComms.count()).toBeGreaterThan(0);
					browser.sleep(1000);
					//element(by.css('[id^="EndpointDetailPanel"] g:nth-child(5) > rect:nth-child(7)')).click();
					//browser.sleep(1000);
				},
				function() {fail('External Endpoint Communications Unclickable')}, -5, 0);
			}
		});
	});

	it('Check Endpoint External Communications from top filtered section', function() {
		console.log('\nDevice External Communications: From first endpoint to last endpoint, clicking a few endpoints.');
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(22)')).click();
		browser.sleep(2000);
		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));

		//element.all(by.css('#sideMenu~g>text~rect')).get(3).click();
		selectSideMenu('Ext. Communication', sideMenuClicks, sideMenuTexts);
		browser.sleep(120000);
		element(by.css('#topCards svg > g:nth-child(17) > rect')).click();
		browser.sleep(5000);
		//element(by.css('[id^="InternalPeerBrowserPanel"] g:nth-child(4) > rect:nth-child(7)')).click();
		//browser.sleep(3000);
		let externalComms = element.all(by.css('#topCards svg > rect:nth-child(3)[fill]'));

		externalComms.each(function(elem, index) {
			if(index < 10) {
				scrollToView(externalComms.get(index));
				browser.sleep(1000);
				presentElementClick(externalComms.get(index), '', function() {
					browser.sleep(4000);
					scrollToView(element(by.css('[id^="EndpointDetailPanel"][id$="0:0"]')));
					let endpointComms = element.all(by.css('[id^="EndpointDetailPanel"] svg > g > g > svg > g > g'));
					expect(endpointComms.count()).toBeGreaterThan(0);
					browser.sleep(1000);
					//element(by.css('[id^="EndpointDetailPanel"] g:nth-child(5) > rect:nth-child(7)')).click();
					//browser.sleep(1000);
				},
				function() {fail('External Endpoint Communications Unclickable')}, -5, 0);
			}
		});
	});
});