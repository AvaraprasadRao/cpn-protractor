//test04_systemPageConfigDelete.js

describe('System Tab Delete Configurations Suite', function() {

	it('1. Config: Network Equipment IP Adresseses', function () {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(5000).then(function() {
			console.log('\n\nSystem Cleanup: Clearing Config Information.');
			console.log('\nSystem Cleanup: Network Equipment IP Adresseses.');
		});
		expect(browser.getCurrentUrl()).toMatch('systempage');
		expect(element(by.id('NetworkConfigPanel(Equipment Configuration)0:0')).isPresent()).toBe(true);

		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		let plusButtons = element.all(by.css('path[fill="#4bb3ad"] ~ rect'));
		let minusButtons = element.all(by.css('path[fill="#bd3e3e"] ~ rect'));

		//1. Network Equipment IP Adresseses 
		plusButtons.get(0).click();
		browser.sleep(2000);
		minusButtons.get(0).click();
		browser.sleep(1000);
		scrollAndClick(nextButtons.get(0));
	});

	it('2. Network Equipment CLI Credentials', function () {
		let plusButtons = element.all(by.css('path[fill="#4bb3ad"] ~ rect'));
		let minusButtons = element.all(by.css('path[fill="#bd3e3e"] ~ rect'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		console.log('\nSystem Cleanup: Network Equipment CLI Credentials.');

		//2. Network Equipment CLI Credentials
		browser.sleep(1000);
		minusButtons.count().then(function(num) {
			if (num > 0){
				for(i = 0; i < (num - 1); i++) {
					minusButtons.get(0).click();
					browser.sleep(1000);
				}
			}
		});
		browser.sleep(1000);
		plusButtons.get(1).click();
		browser.sleep(2000);
		minusButtons.get(0).click();
		browser.sleep(1000);
		scrollAndClick(nextButtons.get(1));
	});

	it('3. Network Equipment SNMP Credentials', function () {
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		console.log('\nSystem Cleanup: Network Equipment SNMP Credentials');

		//3. Network Equipment SNMP Credentials
		browser.sleep(2000);
		scrollAndClick(nextButtons.get(2));
	});

	it('4. Configure Vulnerability Scan', function () {
		let plusButtons = element.all(by.css('path[fill="#4bb3ad"] ~ rect'));
		let minusButtons = element.all(by.css('path[fill="#bd3e3e"] ~ rect'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		let dropdowns = element.all(by.css('*[id*=frequencyInput]'));
		console.log('\nSystem Cleanup: Configure Vulnerability Scan.');

		//4. Configure Vulnerability Scan
		browser.sleep(2000);
		plusButtons.get(2).click();
		browser.sleep(2000);
		minusButtons.get(0).click();
		browser.sleep(1000);
		expect(dropdowns.all(by.css('text')).get(0).getText()).toEqual('None');
		scrollAndClick(nextButtons.get(3));
	});

	it('5. Configure Password Scan.', function () {
		let plusButtons = element.all(by.css('path[fill="#4bb3ad"] ~ rect'));
		let minusButtons = element.all(by.css('path[fill="#bd3e3e"] ~ rect'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		let dropdowns = element.all(by.css('*[id*=frequencyInput]'));
		console.log('\nSystem Cleanup: Configure Password Scan.');

		//5. Configure Password Scan
		browser.sleep(2000);
		plusButtons.get(4).click();
		browser.sleep(2000);
		minusButtons.get(0).click();
		browser.sleep(1000);
		expect(dropdowns.all(by.css('text')).get(1).getText()).toEqual('None');
		scrollAndClick(nextButtons.get(4));
	});

	it('6. Enable or Disable Probes', function () {
		let checkBoxes = element.all(by.css('g:nth-child(6) > g:nth-child(3) > svg > g > svg > g > g > rect:nth-child(3)'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		console.log('\nSystem Cleanup: Enable or Disable Probes.');

		//6. Enable or Disable Probes
		browser.sleep(2000);
		element.all(by.css('g:nth-child(6) > g:nth-child(3) > svg > g > svg > g > g > rect:nth-child(1)[fill="#4bb3ad"]')).count().then(function(num) {
			if(num == 4) {
				checkBoxes.get(0).click();
				checkBoxes.get(1).click();
				checkBoxes.get(2).click();
				checkBoxes.get(3).click();
			}
		});
		browser.sleep(1000);
		scrollAndClick(nextButtons.get(5));
	});

	it('7. Configure CloudPost Equipment', function () {
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		console.log('\nSystem Cleanup: Configure CloudPost Equipment');
 
		//7. Configure CloudPost Equipment
		browser.sleep(2000);
		scrollAndClick(nextButtons.get(6));
		browser.sleep(2000);
		nextButtons.get(6).click();
		browser.sleep(2000);
		ignoreSynchronization(function() {
			expect(element(by.css('text[fill="#4bb3ad"]')).isPresent()).toBe(true);
			//expect(element(by.css('text[fill="#4bb3ad"]')).getText()).toMatch("Successfully saved Configuration.");
		});
		browser.sleep(1000);
	});
});
