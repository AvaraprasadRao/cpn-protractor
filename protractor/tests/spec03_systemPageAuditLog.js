//test_03_systemPageAuditLog.js

describe('System Config Check Audit Log Suite', function() {

	function checkLog(allElem, description) {
		let found = false
		allElem.each(function(elem, index) {
			presentElementText(elem, '', function() {
				elem.getText().then(function(descText) {
					if((stringContains(descText, description)) && (!found)) {
						console.log('Description Found: \"'+descText+'\"');
						expect(descText.toLowerCase()).toMatch(description.toLowerCase());
						found = true;
					}
				});
			});
		}).then(function() {
			if(!found) {
				console.log('Description Not Found: \"'+description+'\"')
				fail('Description Not Found: \"'+description+'\"')
			}
		});
	}

	it('Check if there are more than 1 trails present', function () {
		loginUser(usrnm_keys, pswrd_keys);
		console.log('\n\nSystem Audit Trail: Check if Trails are present.');
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(5000);

		let sideMenuClicks = element.all(by.css('#sideMenu~g > rect:last-of-type'));
		let sideMenuTexts = element.all(by.css('#sideMenu~g > text:first-of-type'));
		selectSideMenu('Audit Trail', sideMenuClicks, sideMenuTexts);
		browser.sleep(4000);

		var auditTrails = element(by.css('[id^="AuditTrailBrowserPanel"] g:nth-child(4) > text')).getText().then(function(text) {
			var num = text.match(/\d/g);
			num = num.join("");
			console.log('Audit Trail Entries found: '+num);
			return parseInt(num, 10);
		});
		expect(auditTrails).toBeGreaterThan(4);
	});

	it('Check individual Trails for specific descriptions', function() {
		console.log('\nSystem Audit Trail: Check individual descriptions.');
		browser.sleep(2000);
		element(by.css('svg > g:nth-child(9) > rect')).click();
		browser.sleep(3000);
		var descriptions = [
			"Network Scrape",
			"Device Probes",
			"Span Traffic",
			"Active Directory",
			"EmailAlarmNotification",
			"EmailDeviceNotification"
		];
		element(by.css('[id^="AuditTrailBrowserPanel"] g:nth-child(5) > rect:nth-child(7)')).click();
		browser.sleep(3000);
		for (i = 0; i < descriptions.length; i++) {
			browser.sleep(2000);
			element(by.css('[id$="BPCTxtField"]')).clear();
			element(by.css('[id$="BPCTxtField"]')).sendKeys(descriptions[i]);
			browser.sleep(2000);
			var tableDescriptions = element.all(by.css("#topCards svg > svg:nth-child(7) > text"));
			checkLog(tableDescriptions, descriptions[i]);
		}
	});
});