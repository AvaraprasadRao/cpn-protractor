//test_04_systemPageConfig.js

describe('System Tab Config Suite', function() {
	
	it('1. Config: Network Equipment IP Adresseses', function () {
		loginUser(usrnm_keys, pswrd_keys);
		browser.sleep(deviceLoadDelay);
		element(by.css('#gnTopMenu > rect:nth-child(34)')).click();
		browser.sleep(5000).then(function() {
			console.log('\n\nSystem Config: Inputting Config Information:');
			console.log('\nSystem Config: Network Equipment IP Adresseses.');
		});
		expect(browser.getCurrentUrl()).toMatch('systempage');
		expect(element(by.id('NetworkConfigPanel(Equipment Configuration)0:0')).isPresent()).toBe(true);

		let ipAddresses = element.all(by.css('*[id^=ipaddress]'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));

		//1. Network Equipment IP Adresseses
		browser.sleep(3000);
		ipAddresses.get(0).clear();
		// ipAddresses.get(0).getAttribute('value').then(function(text) {
		// 	if(text.length != 0) {
		// 		clearElem(ipAddresses.get(0), text.length);
		// 	}
		// });
		ipAddresses.get(0).sendKeys('10.200.201.2');
		browser.sleep(1000);
		scrollAndClick(nextButtons.get(0));
	});

	it('2. Network Equipment CLI Credentials', function () {
		let usernames = element.all(by.css('*[id^=username]'));
		let passwords = element.all(by.css('*[id^="password"]'));
		let enpasswords = element.all(by.css('*[id^=enpassword]'));
		let plusButtons = element.all(by.css('path[fill="#4bb3ad"] ~ rect'));
		let minusButtons = element.all(by.css('path[fill="#bd3e3e"] ~ rect'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		console.log('\nSystem Config: Network Equipment CLI Credentials.');

		//2. Network Equipment CLI Credentials
		minusButtons.count().then(function(num) {
			if (num > 0){
				browser.sleep(2000);
				for(i = 0; i < (num - 1); i++) {
					minusButtons.get(0).click();
					browser.sleep(1000);
				}
				browser.sleep(1000);
				plusButtons.get(1).click();
				browser.sleep(1000);
				minusButtons.get(0).click();;
			}
		});
		browser.sleep(2000);
		usernames.get(0).sendKeys('cisco');
		passwords.get(0).sendKeys('cisco');
		enpasswords.get(0).sendKeys('cisco');
		browser.sleep(1000);
		plusButtons.get(1).click();
		
		usernames.get(1).sendKeys('admin');
		passwords.get(1).sendKeys('admins');
		enpasswords.get(1).sendKeys('enable');
		browser.sleep(1000);
		plusButtons.get(1).click();

		usernames.get(2).sendKeys('manager');
		passwords.get(2).sendKeys('cloudpost');
		enpasswords.get(2).sendKeys('cloudpost');
		browser.sleep(1000);
		scrollAndClick(nextButtons.get(1));
	});

	it('3. Network Equipment SNMP Credentials', function () {
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		console.log('\nSystem Config: Network Equipment SNMP Credentials');

		//3. Network Equipment SNMP Credentials
		browser.sleep(2000);
		scrollAndClick(nextButtons.get(2));
	});

	it('4. Configure Vulnerability Scan', function () {
		let ipAddresses = element.all(by.css('*[id^=ipaddress]'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		let dropdowns = element.all(by.css('*[id*=frequencyInput]'));
		let optionBoxes = element.all(by.css('#infoLayerGElement g rect'));
		let optionTexts = element.all(by.css('#infoLayerGElement g text'));
		console.log('\nSystem Config: Configure Vulnerability Scan.');

		//4. Configure Vulnerability Scan
		browser.sleep(2000);
		//ipAddresses.get(1).clear();
		ipAddresses.get(1).getAttribute('value').then(function(text) {
			if(text.length != 0) {
				clearElem(ipAddresses.get(1), text.length);
			}
		});
		ipAddresses.get(1).sendKeys('10.200.205.0/24');
		dropdowns.get(0).click();
		browser.sleep(1000);
		dropdowns.all(by.css('text')).get(0).getText().then(function(scanText) {
			if(scanText != "Now") {
				optionBoxes.filter(function(elem, index) {
					return optionTexts.get(index).getText().then(function(text) {
						return text === 'Now';
					});
				}).first().click();
			}
		});
		browser.sleep(1000);
		expect(dropdowns.all(by.css('text')).get(0).getText()).toEqual('Now');
		browser.sleep(1000);
		scrollAndClick(nextButtons.get(3));
	});

	it('5. Configure Password Scan.', function () {
		let ipAddresses = element.all(by.css('*[id^=ipaddress]'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		let dropdowns = element.all(by.css('*[id*=frequencyInput]'));
		let optionBoxes = element.all(by.css('#infoLayerGElement g rect'));
		let optionTexts = element.all(by.css('#infoLayerGElement g text'));
		console.log('\nSystem Config: Configure Password Scan.');

		//5. Configure Password Scan
		browser.sleep(2000);
		//ipAddresses.get(3).clear();
		ipAddresses.get(3).getAttribute('value').then(function(text) {
			if(text.length != 0) {
				clearElem(ipAddresses.get(3), text.length);
			}
		});
		ipAddresses.get(3).sendKeys('10.200.205.0/24');
		dropdowns.get(1).click();
		browser.sleep(1000);
		dropdowns.all(by.css('text')).get(1).getText().then(function(scanText) {
			if(scanText != 'Now') {
				optionBoxes.filter(function(elem, index) {
					return optionTexts.get(index).getText().then(function(text) {
						return text === 'Now';
					});
				}).first().click();
			}
		});
		browser.sleep(1000);
		expect(dropdowns.all(by.css('text')).get(1).getText()).toEqual('Now');
		scrollAndClick(nextButtons.get(4));
	});

	it('6. Enable or Disable Probes', function () {
		let checkBoxes = element.all(by.css('g:nth-child(6) > g:nth-child(3) > svg > g > svg > g > g > rect:nth-child(3)'));
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		console.log('\nSystem Config: Enable or Disable Probes.');

		//6. Enable or Disable Probes
		browser.sleep(2000);
		element.all(by.css('g:nth-child(6) > g:nth-child(3) > svg > g > svg > g > g > rect:nth-child(1)[fill="#4bb3ad"]')).count().then(function(num) {
			if(num == 0) {
				checkBoxes.get(0).click();
				checkBoxes.get(1).click();
				checkBoxes.get(2).click();
				checkBoxes.get(3).click();
			}
		});
		browser.sleep(1000);
		scrollAndClick(nextButtons.get(5));
	});

	it('7. Configure CloudPost Equipment', function () {
		let nextButtons = element.all(by.css('g:nth-child(3) > svg > g > svg > g > svg > g > g:nth-child(7) > g > rect[fill="#4bb3ad"]~rect'));
		console.log('\nSystem Config: Configure CloudPost Equipment');
 
		//7. Configure CloudPost Equipment
		browser.sleep(2000);
		scrollAndClick(nextButtons.get(6));
		browser.sleep(2000);
		nextButtons.get(6).click();
		browser.sleep(2000);
		ignoreSynchronization(function() {
			expect(element(by.css('text[fill="#4bb3ad"]')).isPresent()).toBe(true);
			//expect(element(by.css('text[fill="#4bb3ad"]')).getText()).toMatch("Successfully saved Configuration.");
		});
		browser.sleep(3000);
	});
});