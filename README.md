# GUI Automated Testing with Protractor

## Overview

Protractor is an e2e test framework for Angular applications. This framework runs tests on the UI, as a normal user would.
Utilizing tests created here, manual testing to check for bugs would be uneccessary. I have set up a Jenkins CI server that
grabs the up-to-date scripts and allows you to run the scripts in a simple manner, here: http://192.168.106.175:8080.
The following instructions are setup to allow you to run the commands yourself on your local device.

## Prerequisites

Need the following installed and available in Terminal/Command Prompt:

* **bash** (optional for automated script, recommended)
* **Python**
* **Java**
* **Protractor**
* **npm packages** (optional for protractor HTML report)

Need to have an remote server running the selenium standalone jar or have webdriver-manager up and running locally.
Also all the webdrivers that you'll want to use would need to be downloaded if using the remote selenium option.

The selenium standalone server jar and webdrivers can be downloaded from this [Downloads](http://docs.seleniumhq.org/download/) page.
A remote server is already available and utilized by the Jenkins server to run the scripts as well, here: http://192.168.106.58:5555/wd/hub.

Let's start off by downloading Protractor!

### Downloading Protractor:

* Install the current node.js with default settings [here](https://nodejs.org/en/).
* Check if the following commands prints out a version number seeing something like the following:
(for node.js anything past 8.0.0 should work)
```
node --version
v8.10.0

npm --version
5.8.0
```
* Once the download/installation is complete, type in Command Prompt/Terminal:
```sudo npm install -g protractor```

* This will install two command line tools, ```protractor``` and ```webdriver-manager```.
Try running ```protractor --version``` to make sure it's setup properly:
```
protractor --version
Version 5.3.0
```
* Protractor is now set! All that's left to do is to set up a Selenium Server.

### Running webdriver-manager

If you want to locally run a Selenium Server and watch the test script's actions in real-time... 
Then webdriver-manager needs to be initialized.

Make sure you have a native version of the browser you want the Selenium Server to access: 
[Chrome](https://www.google.com/chrome/), [Firefox](https://www.mozilla.org/en-US/firefox/new/), [Safari](https://support.apple.com/en_AU/downloads/safari), etc.

Start by running the following:

```sudo webdriver-manager update```

With this command the webdrivers: chromedriver, selenium standalone, and geckodriver (Firefox) are downloaded/updated

Following this step run:

```sudo webdriver-manager start```

Wait until the message: "Selenium Server is up and running on port 4444" is printed on the console

Now whenever you run Protractor the scripts will be ran locally with the **seleniumAddress** set to `'http://localhost:4444/wd/hub'` (automatically included in conf.js with ```local``` parameter when using the bash script)

## How to Run Protractor

All that is needed to run protractor is a single **conf.js** file, one or multiple **spec.js** files, and a **selenium webdriver server**. (locally with webdriver-manager or remotely)

Then call:

```protractor conf.js``` 

More instructions and tutorial on how this works are listed below.
### Workflow

![alt text](https://blog.trigent.com/wp-content/uploads/2017/01/2.jpg "Protractor Workfloww")

It all starts with a simple ```conf.js``` file like the following:
```javascript
// conf.js
exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['spec.js'],

    capabilities: {
        'browserName': 'chrome',
        'version': '54'
    }
};
```

along with a script like the following code in a ```spec.js``` file (calculator app test):

```javascript
// spec.js
describe('Protractor Demo App', function() {
    it('should add forty and two', function() {
        browser.get('http://juliemr.github.io/protractor-demo/');
        element(by.model('first')).sendKeys(40);
        element(by.model('second')).sendKeys(2);

        element(by.id('gobutton')).click();

        expect(element(by.binding('latest')).getText()).toEqual('42');
    });
});
```

Once these two files are together this simple call on the command line begins the test:

```protractor conf.js```

The Protractor Test Runner then sends the tests to the selenium server that is present in the conf.js file in **seleniumAddress**.

At this point the selenium standalone server at the specified address runs the tests in the spec.js file.

In this case the calculator test will be ran against Chrome. (can be also ran against other browsers simultaneously) 

Once the test is complete the results will be sent back to the location of where **protractor conf.js** was called and logged accordingly.

There you go, you have created a simple automated test for your GUI!

Here is the example conf.js and spec.js running through Protractor!

![Alt Text](https://www.codeproject.com/KB/applications/1066968/4_java.gif)

## Using the Automated Script

You will need bash in order to use this script.

As shown above, running Protractor is simple with just a conf file and spec file.

Included in this repository is a dynamic python script that can create conf.js with specified values, the protractor test specs all in a single directory here, and a bash script that handles this python script and other filing commands to organize the run/data/results under a single command.

Start off by changing directories into cpn-protractor so the following files/directory should be seen when typing the ```ls``` command:

* **protractor-launch** (bash launch script)
* **protractor-dynamic-conf.py** (dynamic Python script)
* **protractor** (directory holds the global variables and test scripts)

With the bash **protractor-launch** script, tests can be dynamically ran.

```bash
bash protractor-launch login    chr         https://setup-test.cpn.lan cpn@cloudpostnetworks.com port 5555

bash protractor-launch {suite}  {browsers}  {setup_url}                {email}                   {selenium/port}
```

This command does all the work mentioned above instantly!

First, all of the files mentioned above will be placed into a new directory, "protractorResults".

The bash script calls **protractor-dynamic-conf.py**, which creates a conf file on-the-fly that matches the parameters stated in the command. Specific desired capabilities are included into each conf file depending on the browsers.

Along with the conf file, a copy of the global variables file and test files under the **protractor** directory are also created that links to the conf file allowing the test runner to get all the neccessary variables/test scripts in order to run smoothly.

The script then calls the newly created conf file with ```protractor conf_br.js --suite <suites>```
Protractor will run the designated suites against the specified url and browser, passing or failing certain expectations.

Once the test is complete, a XML result file, HTML report file, and a screnshots directory that holds the pngs of when a success/failure in a test occurred will also be placed in this results directory once the program run is complete. (To have access to this feature - "protractor-screenshoter-plugin", "jasmine2-protractor-utils", "protractor-html-reporter-2", "jasmine-reporters", "fs-extra" npm packages are required).

### npm Required Downloads to use the Automated Script

This section is here to explain what is left to download in order for you to not run into errors when running the automated script.

**5** packages are required in order for the HTML report to be available after each test depending on the browser:

* [protractor-screenshoter-plugin](https://www.npmjs.com/package/protractor-screenshoter-plugin)
* [jasmine2-protractor-utils](https://www.npmjs.com/package/jasmine2-protractor-utils)
* [protractor-html-reporter-2](https://www.npmjs.com/package/protractor-html-reporter-2)
* [jasmine-reporters](https://www.npmjs.com/package/jasmine-reporters)
* [fs-extra](https://www.npmjs.com/package/fs-extra)

If you are not using the automated script, but only the python dynamic conf script then you'll still need to download these packages, or just comment out the affected sections before running ```protractor new_conf.js```.

Such sections that you'll want to comment out will be like these:

```javascript
plugins: [{
    path: './chrome-mouse-tracker.js',
  }, {
    package: 'protractor-screenshoter-plugin',
    screenshotPath: './chr-html-report',
    screenshotOnExpect: 'none',
    screenshotOnSpec: 'none',
    withLogs: 'false',
    writeReportFreq: 'asap',
    verbose: 'info',
    imageToAscii: 'none',
    clearFoldersBeforeTest: true
}],
```
```javascript
var jasmineReporters = require('jasmine-reporters');
    jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
      consolidateAll: true,
      savePath: './',
      filePrefix: 'xmlresults'
    }));
    var fs = require('fs-extra');
    fs.emptyDir('screenshots/', function (err) {
        console.log(err);
      });
    jasmine.getEnv().addReporter({
      specDone: function(result) {
        if (result.status == 'failed') {
          browser.getCapabilities().then(function (caps) {
            var browserName = caps.get('browserName');
            browser.takeScreenshot().then(function (png) {
              var stream = fs.createWriteStream('screenshots/'+result.fullName+".png");
              stream.write(new Buffer(png, 'base64'));
              stream.end();
            });
          });
        }
      }
    });
```
```javascript
var browserName, browserVersion;
    browser.getCapabilities().then(function (caps) {
      browserName = caps.get('browserName');
      browserVersion = caps.get('version');
      platform = caps.get('platform');
      var HTMLReport = require('protractor-html-reporter-2');
      testConfig = {
        reportTitle: 'Protractor Test Execution Report',
        outputPath: './',
        outputFilename: 'ProtractorTestReport',
        screenshotPath: './screenshots',
        testBrowser: browserName,
        browserVersion: browserVersion,
        modifiedSuiteName: false,
        screenshotsOnlyOnFailure: true,
        testPlatform: platform
      };
      new HTMLReport().from('xmlresults.xml', testConfig);
    });
```

Once you do that then there should be no errors when trying to run the protractor command separate from the automated bash script:
Remember if these sections are commented out then the HTML report will not be created when running the tests.

### Explanation

The command above requires **5** arguments: The ***Suite***, ***Browsers***, ***URL***, ***Email***, ***Selenium***

The `login` parameter signals to protractor to run login suite against the setup

The `chr` parameter signals the script to create temporary conf file with the single capability for each browser specified; this allows for a seamless, continuous run. 

To elaborate, the `chr` portion of the parameter runs a single protractor suite against the Chrome browser. This means a conf file is created to allow Protractor to run the specified suite (individually) against Chrome, like below.

```protractor conf_chrome.js ```

Once this run is complete, the next run for the next browser is started, in this#!/usr/bin/env  case no other browser was in the parameter like, `chr,ff`.

The `https://setup-test.cpn.lan` url parameter is needed to access the web application and run the tests directly on it.

The `cpn@cloudpostnetworks.com` email parameter will be used for certain tests and used by Jenkins to send a result email.

The last parameter `port 5555` is used to say which port on the remote selenium server should be used. If you use `local` instead of the two strings then the tests are ran on the local browser as long as the webdriver-manager is up and running on the local host.
One last option is `host 192.168.106.178 port 5555` which give you the option to select a separate selenium server and port if applicable.

### Suite Parameters - (Case-Sensitive)

* **login** - "Login tests (login/logout/bad credentials)"
* **basicClick** - "Basic clicking of tabs test"
* **menu_test** - "Checks particular card values and clicks them"
* **config** - "Types System Configuration values and saves"
* **delete_config** - "Used for cleanup of the config suite"


### Browser Parameters - (Case-Insensitive)
* *Must be separated by commas if multiple are used in command*
* **all** - All of the following browsers
* **chr** - Google Chrome
* **ff** - Mozilla Firefox (eventual)
* **saf** - Safari (eventual)
* **ie** - Microsoft Internet Explorer (eventual)

## For Windows 10 Users to Run Bash Script

Bash will only be available for those running the Windows 10 Anniversary Update (if Step 3/4 are unapplicable then you are not on the update). If unable to update then follow the steps under this section to utilize the Python script solely.

Bash shell needs to be configured before the automated script can be used.
To Install Bash on Windows 10 follow these steps:

1. Open **Settings**.
2. Click on **Update & Security**.
3. Click on **For Developers**.
4. Under "Use developer features", select the **Developer mode** option to setup the environment to install Bash.
![alt text](https://www.windowscentral.com/sites/wpcentral.com/files/styles/xlarge/public/field/image/2016/09/developer-mode-windows10.jpg?itok=ukqVNDbQ, "Developer Mode")

5. On the message box, click **Yes** to turn on developer mode.
    * After the necessary components install, you'll need to restart your computer.
![alt text](https://www.windowscentral.com/sites/wpcentral.com/files/styles/xlarge/public/field/image/2016/09/turnon-developer-mode-win10.jpg?itok=ynLMWgpn, "Developer Mode Message")

6. Once your computer reboots, open **Control Panel**.
7. Click on **Programs**.
8. Click on **Turn Windows features on or off**.
![alt text](https://www.windowscentral.com/sites/wpcentral.com/files/styles/xlarge/public/field/image/2016/09/programs-features-control-panel.jpg?itok=7YuUr4sT, "Window Features Option")

9. Check the **Windows Subsystem for Linux (Beta)** option.

![alt text](https://www.windowscentral.com/sites/wpcentral.com/files/styles/xlarge/public/field/image/2016/09/windows-subsystem-linux.jpg?itok=MJLsCJ1K, "Window Sunsystem Linux Option")

10. Click **OK**.
11. Once the components installed on your computer, click the **Restart now** button to complete the task.

After your computer restarts, you will notice that Bash will not appear in the "Recently added" list of apps, this is because Bash isn't actually installed yet. Now that you have setup the necessary components, use the following steps to complete the installation of Bash:

1. Open Start, do a search for **bash/bash.exe**, and press Enter.
2. A command prompt should open, and you should be prompted to download and install a Linux distribution from the Windows Store. (The Ubuntu Version is the best and highly recommended)

3. Then you'll need to create a default **UNIX** user account. This account doesn't have to be the same as your Windows account. Enter the username in the required field and press **Enter** (you can't use the username "admin").
![alt text](https://www.windowscentral.com/sites/wpcentral.com/files/styles/xlarge/public/field/image/2016/09/install-bash-ubuntu-windows10.jpg?itok=2C3IHYrF, "UNIX User Creation")

4. Close the command prompt.

Some added benefits of doing this process:

* Bash environment to run tools like ls, awk, sed, and grep.
* Basic features for languages, such as NodeJS / npm, Python, Perl, Git.
* Command line editor, including vi, emacs, and ssh.
* Linux user support.
* Symlink support.
* Ability to run apt and apt-get for updates and package testing.

Now that you completed the installation and setup, you can open the Bash tool from the Start menu like you would with any other app. More importantly, you can open powershell/command prompt from the directory that holds the scripts and tests and then type ```bash``` to switch over instantaneously; you can now work in the environment with all the UNIX commands.

So, once webdriver-manager or a Remote Selenium Sever is running you should be able to run the following:
```bash
bash protractor-launch login chr <setup_url> <email> {local/host <> port <>/port <>}
```

## Using the Dynamic Python Script

Script uses Python 2.
Use this only if unable to run the full bash script which does all the work.

This script creates the conf file for you to run and since the spec files are available in this directory, you can still run protractor without the bash script.

The command should be ran like below:
```
python protractor-dynamic-conf.py <browser> <full_url> <email> <file_name/loc> <seleniumAddress>
```

The ```<browser>``` parameter is the same as the **Browser Parameters** section above.

The ```<full_url>``` parameter is the full "https://..." for the setup you want to run the test on.

The ```<email>``` parameter is the normal email with the "@cloudpostnetworks" suffix.

The ```<file_name/loc>``` parameter is the just the name of the file along with a possible directory like 
```new_conf_files/new_conf.js```.

The ```<seleniumAddress>``` parameter is the location of selenium webdrivers that you want protractor to use; will be
```http://localhost:4444/wd/hub``` if using webdriver-manager to run the tests locally.

Now once the command is ran and you can see the file is created, just run: 
```protractor <new_conf.js> --suite <suites>```

You can run against multiple suites separating each by a comma from the **Suite Parameters** section above. 

## Where to Make Changes
***Selenium Address changes***

Go to **protractor-launch** and change:

```bash
if [ "$4" == 'debug' ]; then
  SELENIUM='localhost'
  SELENIUM_PATH='http://localhost:4444/wd/hub'
else
  SELENIUM='SPECIFIC_SEL'
  SELENIUM_PATH='http://SPECIFIC_SEL:4444/wd/hub'
fi
```

***New Tests being added***

Make sure the new tests/spec.js files are included in the tests folder with the others in **protractor/tests/**.

Go to write_suites in **protractor-dynamic-conf.py** and change if you want to add the new tests into specific suites:

```python
self.file.write(" suites: {\n")
self.file.write("   all: ['tests/spec*.js'],\n")
self.file.write("   basic: ['tests/spec01*.js', 'tests/spec02*.js', 'tests/spec03*.js'],\n")
self.file.write("   delete: ['tests/spec04_systemPageConfigDelete.js', 'tests/spec04_systemPageIntegrationDelete.js'],\n")
self.file.write("   config: ['tests/spec04_systemPageConfig.js'],\n")
self.file.write("   new_suite: ['tests/new_tests*.js'],\n")
self.file.write(" },\n\n")
```

## Issues/Questions Unanswered

* If you downloaded any of the following requirements (Protractor, Node, Java, Python) and can't see the versioning of any of them in your environment: Then you need to include the main folder for these applications into your PATH variable (most likely needs to be done for Windows Users).

## Built Using

* [Protractor](http://www.protractortest.org/#/)
* [Node](https://nodejs.org/en/)
* [JavaScript](https://www.javascript.com)
* [Selenium](http://www.seleniumhq.org)
* [Python](https://www.python.org)
* npm packages: [protractor-screenshoter-plugin](https://www.npmjs.com/package/protractor-screenshoter-plugin), [jasmine2-protractor-utils](https://www.npmjs.com/package/jasmine2-protractor-utils), [protractor-html-reporter-2](https://www.npmjs.com/package/protractor-html-reporter-2), [jasmine-reporters](https://www.npmjs.com/package/jasmine-reporters), [fs-extra](https://www.npmjs.com/package/fs-extra)

## Author
* **Nipul Jayasekera** - Intern
