#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import re

class conf:
	"""Used to individually run protractor tests on specific browsers"""
	def __init__(self, browser = "", url = "", email = "", path = "", selenium = ""):
		self.file = None
		self.browser = browser
		self.capability = ''
		self.url = url
		self.email = email
		self.path = path
		self.selenium = selenium
		if(self.browser != "" and self.path != ""):
			self.file = open(self.path, "w+")
			if re.search('CHR', self.browser, re.I):
				self.capability = 'chrome'
			elif re.search('FF', self.browser, re.I):
				self.capability = 'firefox'
			elif re.search('IE', self.browser, re.I):
				self.capability = 'internet explorer'
			if re.search('CHR-H', self.browser, re.I):
				self.capability = 'chrome'
			self.write_to_file()

	"""Sets the Selenium Address"""
	def write_selenium(self):
		self.file.write("	framework: 'jasmine2',\n")
	 	self.file.write("	seleniumAddress: '"+self.selenium+"',\n")

	"""Sets Accessible Suites"""
	def write_suites(self):
		self.file.write("	suites: {\n")
		self.file.write("		all: ['tests/spec*.js'],\n")
		self.file.write("		click: ['tests/spec02*.js'],\n")
		self.file.write("		delete_config: ['tests/spec03_systemPageConfigDelete.js', 'tests/spec03_systemPageIntegrationDelete.js'],\n")
		self.file.write("		config: ['tests/spec03_systemPageConfig.js', 'tests/spec03_systemPageIntegration.js'],\n")
		self.file.write("		integrate: ['tests/spec03_systemPageIntegration.js'],\n")
		self.file.write("		config_check: ['tests/spec03_systemPageConfigCheck.js', 'tests/spec03_systemPageIntegrationCheck.js'],\n")
		self.file.write("		audit_log: ['tests/spec03_systemPageAuditLog.js'],\n")
		self.file.write("		menu_test: ['tests/spec_exec.js', 'tests/spec_incident.js', 'tests/spec_group.js', 'tests/spec_deviceSummary.js', 'tests/spec_deviceList.js'],\n")
		self.file.write("		full_config: ['tests/spec_fullConfig.js'],\n")
		self.file.write("		full_audit_log: ['tests/spec_fullAuditLog.js'],\n")
		self.file.write("		specific: ['tests/spec_fullConfig.js', 'tests/spec_fullAuditLog.js'],\n")
		self.file.write("		exec_page: ['tests/spec_exec.js'],\n")
		self.file.write("		incident_page: ['tests/spec_incident.js'],\n")
		self.file.write("		device_page: ['tests/spec_deviceSummary.js', 'tests/spec_deviceList.js', 'tests/spec_deviceAdList.js', 'tests/spec_deviceCommunications.js'],\n")
		self.file.write("		profile_page: ['tests/spec_profile.js'],\n")
		self.file.write("		device_detail: ['tests/spec_individualDevices.js'],\n")
		self.file.write("		group_page: ['tests/spec_group.js'],\n")
		self.file.write("		network_page: ['tests/spec_network.js'],\n")
		self.file.write("		login: ['tests/spec01*.js'],\n")
		self.file.write("		report: ['tests/spec_report.js'],\n")
		self.file.write("		account: ['tests/spec_account_creation.js', 'tests/spec_account_privileges.js'],\n")
		self.file.write("		loop_login: ['tests/spec_loginLogoutLoop.js'],\n")
		self.file.write("	},\n\n")

	"""Timeout options/Required IE Location/Url setup"""
	def write_options(self):
		self.file.write("	allScriptsTimeout: 440000,\n")
		self.file.write("	getPageTimeout: 20000,\n")
		self.file.write("	acceptSslCerts: true,\n")
		self.file.write("	maxSessions: 1,\n\n")
		
		if self.capability == 'chrome':
			self.file.write("	plugins: [{\n")
			self.file.write("			path: './chrome-mouse-tracker.js',\n")
			self.file.write("		}, {\n")
			self.file.write("			package: 'protractor-screenshoter-plugin',\n")
			self.file.write("			screenshotPath: './"+self.browser+"-html-report',\n")
			self.file.write("			screenshotOnExpect: 'failure+success',\n")
			self.file.write("			screenshotOnSpec: 'none',\n")
			#self.file.write("			screenshotOnSpec: 'failure+success',\n")
			self.file.write("			withLogs: 'true',\n")
			#self.file.write("			writeReportFreq: 'asap',\n")
			self.file.write("			writeReportFreq: 'end',\n")
			self.file.write("			verbose: 'info',\n")
			self.file.write("			imageToAscii: 'none',\n")
			self.file.write("			clearFoldersBeforeTest: true\n")
			self.file.write("	}],\n")
		else:
			self.file.write("	plugins: [{\n")
			self.file.write("		package: 'jasmine2-protractor-utils',\n")
			self.file.write("		disableHTMLReport: true,\n")
			self.file.write("		disableScreenshot: false,\n")
			self.file.write("		screenshotPath: './screenshots',\n")
			self.file.write("		screenshotOnExpectFailure: false,\n")
			self.file.write("		screenshotOnSpecFailure: true,\n")
			self.file.write("		clearFoldersBeforeTest: true,\n")
			self.file.write("		htmlReportDir: './"+self.browser+"-html-report',\n")
			self.file.write("	}],\n\n")

		self.file.write("	jasmineNodeOpts: {\n")
		self.file.write("		showColors: true,\n");
		self.file.write("		defaultTimeoutInterval: 600000\n")
		self.file.write("	},\n\n")

		if self.capability == 'internet explorer':
			self.file.write("	localSeleniumStandaloneOpts : {\n")
			self.file.write("		jvmArgs : ['-Dwebdriver.ie.driver=C:/Windows/System32']\n")
			self.file.write("	},\n\n")

		self.file.write("	params: {\n")
		self.file.write("		url: '"+self.url+"',\n")
		self.file.write("		email: '"+self.email+"',\n")
		self.file.write("		version: ''\n")
		self.file.write("	},\n")

	"""Desired Capabilities determined by browser"""
	def write_capability(self):
		self.file.write("	capabilities: {\n")
		self.file.write("		'browserName': '"+self.capability+"',\n")
		
		if re.search('CHR-H', self.browser, re.I):
			self.file.write("		'chromeOptions': {\n")
			self.file.write("			'args': [ \"--headless\", \"--disable-gpu\", \"--window-size=1900,1000\", \"--allow-insecure-localhost\"]\n")
			self.file.write("		},\n")
			self.file.write("		'loggingPrefs': {\n")
			self.file.write("			'driver': 'INFO',\n")
			self.file.write("			'browser': 'INFO'\n")
			self.file.write("		},\n")
			self.file.write("		'acceptInsecureCerts': true\n")
			
		elif re.search('CHR', self.browser, re.I):
			self.file.write("		'version': '67',\n")
			self.file.write("		'loggingPrefs': {\n")
			self.file.write("			'driver': 'INFO',\n")
			self.file.write("			'browser': 'INFO'\n")
			self.file.write("		},\n")
			self.file.write("		'acceptInsecureCerts': true\n")

		elif re.search('FF', self.browser, re.I):
			self.file.write("		'version': '60',\n")
			#self.file.write("		'exclude': ['tests/spec03_systemPageConfig*.js'],\n")
			self.file.write("		'marionette': 'true',\n")
			# self.file.write("		'log': {'level': 'info'},\n")
			self.file.write("		'acceptInsecureCerts': true\n")

		elif re.search('IE', self.browser, re.I):
			self.file.write("		'version': '11',\n")
			self.file.write("		'exclude': [],\n")
			self.file.write("		'enablePersistentHover': false,\n")
			self.file.write("		'requireWindowFocus': false,\n")
			self.file.write("		'ie.ensureCleanSession': true\n")

		self.file.write("	},\n\n")

	"""On Prepare block: instantiates globals, xml output, screenshots on failure, ignoreSynchronization"""
	def write_onPrepare(self):
		self.file.write("	onPrepare: function() {\n\n")
		self.file.write("		console.log('\\n');\n\n")
		# if re.search('CHR-H', self.browser, re.I):
		# 	self.file.write("			var VideoReporter = require('protractor-video-reporter');\n")
		# 	self.file.write("			var path = require('path');\n")
		# 	self.file.write("			jasmine.getEnv().addReporter(new VideoReporter({\n")
		# 	self.file.write("				baseDirectory: path.normalize(path.join(__dirname, '../testresults/videos/')),\n")
		# 	self.file.write("				saveSuccessVideos: false,\n")
		# 	self.file.write("				createSubtitles: true,\n")
		# 	self.file.write("				singleVideo: true,\n")
		# 	self.file.write("				ffmpegCmd: path.normalize('/usr/local/lib/node_modules/ffmpeg'),\n")
		# 	self.file.write("				ffmpegArgs: [\n")
		# 	self.file.write("					'-f', 'avfoundation',\n")
		# 	self.file.write("					'-i', '1',\n")
		# 	self.file.write("					'-pix_fmt', 'yuv420p',\n")
		# 	self.file.write("					'-r', '24',\n")
		# 	self.file.write("					'-video_size', 'woxga',\n")
		# 	self.file.write("					'-q:v', '10',\n")
		# 	self.file.write("				]\n")
		# 	#self.file.write("				debug: true\n")
		# 	self.file.write("			}));\n\n")

		self.file.write("		var jasmineReporters = require('jasmine-reporters');\n")
		self.file.write("		jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({\n")
		self.file.write("			consolidateAll: true,\n")
		self.file.write("			savePath: './',\n")
		self.file.write("			filePrefix: 'xmlresults'\n")
		self.file.write("		}));\n\n")

		self.file.write("		var keys = Object.keys(globals.variables);\n")
		self.file.write("		for(var i = 0; i < keys.length; i++) {\n")
		self.file.write("			global[keys[i]] = globals.variables[keys[i]];\n")
		self.file.write("		}\n\n")

		self.file.write("		global.ignoreSynchronization = function(callback) {\n")
		self.file.write("			browser.ignoreSynchronization = true;\n")
		self.file.write("			browser.sleep(1500);\n")
		self.file.write("			callback();\n")
		self.file.write("			browser.sleep(1500);\n")
		self.file.write("			browser.ignoreSynchronization = false;\n")
		self.file.write("		};\n\n")

		self.file.write("		global.scrollToView = function(elem) {\n")
		self.file.write("			browser.executeScript(\"arguments[0].scrollIntoView();\", elem.getWebElement()).then(function() {\n")
		self.file.write("				return this;\n")
		self.file.write("			});\n")
		self.file.write("		};\n\n")

		self.file.write("		global.scrollAndClick = function(elem) {\n")
		self.file.write("			browser.executeScript(\"arguments[0].scrollIntoView();\", elem.getWebElement()).then(function() {\n")
		self.file.write("				elem.click();\n")
		self.file.write("				return this;\n")
		self.file.write("			});\n")
		self.file.write("		};\n\n")

		self.file.write("		global.presentElement = function(elem, msg = '', callback, failCallback = function empty() { }) {\n")
		self.file.write("			elem.isPresent().then(function(present) {\n")
		self.file.write("				if(present) {\n")
		self.file.write("					callback();\n")
		self.file.write("				}\n")
		self.file.write("				else {\n")
		self.file.write("					if (msg != '') {\n")
		self.file.write("						console.log(msg);\n")
		self.file.write("					}\n")
		self.file.write("					failCallback();\n")
		self.file.write("				}\n")
		self.file.write("			});\n")
		self.file.write("		};\n\n")

		self.file.write("		global.presentElementClick = function(elem, msg = '', callback, failCallback = function empty() { }, xCoord = 0, yCoord = 0) {\n")
		self.file.write("			elem.isPresent().then(function(present) {\n")
		self.file.write("				if(present && xCoord == 0 && yCoord == 0) {\n")
		self.file.write("					scrollAndClick(elem);\n")
		self.file.write("					callback();\n")
		self.file.write("				}\n")
		self.file.write("				else if(present) {\n")
		self.file.write("					browser.actions().mouseMove(elem).mouseMove({x: xCoord, y: yCoord}).click().perform();\n")
		self.file.write("					callback();\n")
		self.file.write("				}\n")
		self.file.write("				else {\n")
		self.file.write("					if (msg != '') {\n")
		self.file.write("						console.log(msg);\n")
		self.file.write("					}\n")
		self.file.write("					failCallback();\n")
		self.file.write("				}\n")
		self.file.write("			});\n")
		self.file.write("		};\n\n")

		self.file.write("		global.presentElementText = function(elem, msg = '', callback, failCallback = function empty() { }) {\n")
		self.file.write("			elem.isPresent().then(function(present) {\n")
		self.file.write("				elem.getText().then(function(text) {\n")
		self.file.write("					if(present && text.length > 0) {\n")
		self.file.write("						callback();\n")
		self.file.write("					}\n")
		self.file.write("					else {\n")
		self.file.write("						if (msg != '') {\n")
		self.file.write("							console.log(msg);\n")
		self.file.write("						}\n")
		self.file.write("						failCallback();\n")
		self.file.write("					}\n")
		self.file.write("				});\n")
		self.file.write("			});\n")
		self.file.write("		};\n\n")

		self.file.write("		global.clearElem = function(elem, length) {\n")
		self.file.write("			length = length || 100;\n")
		self.file.write("			var backspaceSeries = '';\n")
		self.file.write("			for (var i = 0; i < length; i++) {\n")
		self.file.write("				backspaceSeries += protractor.Key.BACK_SPACE;\n")
		self.file.write("			}\n")
		self.file.write("			elem.sendKeys(backspaceSeries);\n")
		self.file.write("		};\n\n")

		self.file.write("		global.stringContains = function(elemString, search) {\n")
		self.file.write("			var regEx = new RegExp(('\\\\b'+search+'\\\\b'), 'i');\n")
		self.file.write("			return regEx.test(elemString);\n")
		self.file.write("		};\n\n")

		self.file.write("		global.selectSideMenu = function(sideText, menuClicks, menuTexts) {\n")
		self.file.write("			let elem = menuClicks.filter(function(elem, index) {\n")
		self.file.write("				return menuTexts.get(index).getText().then(function(text) {\n")
		self.file.write("					return stringContains(text, sideText);\n")
		self.file.write("				});\n")
		self.file.write("			}).first();\n")
		self.file.write("			elem.isPresent().then(function(present) {\n")
		self.file.write("				if(present) {\n")
		self.file.write("					browser.sleep(1000);\n")
		self.file.write("					elem.click();\n")
		self.file.write("				}\n")
		self.file.write("				else {\n")
		self.file.write("					console.log(sideText+' menu tab not found');\n")
		self.file.write("					return function() {fail(sideText+' menu tab not found');};\n")
		self.file.write("				}\n")
		self.file.write("			});\n")
		self.file.write("		};\n\n")

		self.file.write("		global.findDetail = function(allElem, regex, detail) {\n")
		self.file.write("			allElem.each(function(section, index) {\n")
		self.file.write("				presentElementText(section, '', function() {\n")
		self.file.write("					section.getText().then(function(sectText) {\n")
		self.file.write("						if(sectText.includes(detail)) {\n")
		self.file.write("							presentElementText(allElem.get(index + 1), ('Text for '+section+' is not Present'), function() {\n")
		self.file.write("								allElem.get(index + 1).getText().then(function(value) {\n")
		self.file.write("									console.log(sectText+' '+value);\n")
		self.file.write("									expect(value).toMatch(regex);\n")
		self.file.write("									return true;\n")
		self.file.write("								});\n")
		self.file.write("							});\n")
		self.file.write("						}\n")
		self.file.write("					});\n")
		self.file.write("				});\n")
		self.file.write("			});\n")
		self.file.write("		}\n\n")

		self.file.write("		var getVersion = (function() {\n")
		self.file.write("			var executed = false;\n")
		self.file.write("			return function() {\n")
		self.file.write("				if (!executed) {\n")
		self.file.write("					executed = true;\n")
		self.file.write("					browser.sleep(4000);\n")
		self.file.write("					scrollToView(element(by.css('#gnBottomMenu > text:nth-child(17)')));\n")
		self.file.write("					element(by.css('#gnBottomMenu > text:nth-child(17)')).getText().then(function(text) {\n")
		self.file.write("						browser.params.version = text;\n")
		self.file.write("					browser.sleep(2000);\n")
		self.file.write("					});\n")
		self.file.write("				}\n")
		self.file.write("			};\n")
		self.file.write("		})();\n\n")

		if self.capability == 'internet explorer':
			self.file.write("		global.getURL = function() {\n")
			if re.search('localhost', self.selenium, re.I):
				#self.file.write("			browser.driver.manage().window().setSize(1366, 768);\n")
				self.file.write("			browser.driver.manage().window().maximize();\n")
			else:
				#self.file.write("			browser.driver.manage().window().setSize(1900, 1200);\n")
				self.file.write("			browser.driver.manage().window().maximize();\n")
			self.file.write("			browser.ignoreSynchronization = true;\n")
			self.file.write("			browser.get(browser.params.url);\n")
			self.file.write("			browser.findElement(by.css('#overridelink')).click();\n")
			self.file.write("			browser.ignoreSynchronization = false;\n")
			self.file.write("		};\n")
		else:
			self.file.write("		global.getURL = function() {\n")
			if re.search('localhost', self.selenium, re.I):
				self.file.write("			browser.driver.manage().window().setSize(1500, 1000);\n")
				#self.file.write("			browser.driver.manage().window().maximize();\n")
			else:
				#self.file.write("			browser.driver.manage().window().setSize(1900, 1200);\n")
				self.file.write("			browser.driver.manage().window().maximize();\n")
			self.file.write("			browser.ignoreSynchronization = true;\n")
			self.file.write("			browser.get(browser.params.url);\n")
			self.file.write("			browser.ignoreSynchronization = false;\n")
			self.file.write("		};\n\n")

		self.file.write("		global.loginUser = function(username, password) {\n")
		#self.file.write("			browser.sleep(2000);\n")
		#self.file.write("			browser.restart();\n")
		self.file.write("			browser.sleep(2000);\n")
		self.file.write("			browser.call(getURL);\n")
		self.file.write("			browser.sleep(2000);\n")
		self.file.write("			element(by.css(startLogin_css)).click();\n")
		self.file.write("			browser.sleep(1000);\n")
		self.file.write("			element(by.id(skipLoginButtin_id)).isPresent().then(function(present) {\n")
		self.file.write("				if(present) {\n")
		self.file.write("					element(by.id(skipLoginButtin_id)).click();\n")
		self.file.write("					browser.sleep(2000);\n")
		if self.capability != 'firefox':
			self.file.write("					browser.actions().mouseMove(element(by.css('g:nth-child(3) > path'))).mouseMove({x: -10, y: 0}).click().perform();\n")
			#self.file.write("					browser.actions().click().perform();\n")
		else:
			self.file.write("					element(by.css('g:nth-child(3) > svg:nth-child(2) > g:nth-child(1) > text')).click();\n")
		#self.file.write("					element(by.id(usrnmInputSelect_id)).click();\n")
		self.file.write("					browser.sleep(1000);\n")
		self.file.write("					element(by.id(usrnm_id)).sendKeys(username);\n")
		self.file.write("					browser.sleep(1000);\n")
		self.file.write("					browser.actions().mouseMove(element(by.css('g:nth-child(4) > path'))).mouseMove({x: -10, y: 0}).click().perform();\n")
		#self.file.write("					element(by.id(pswrdInputSelect_id)).click();\n")
		self.file.write("					browser.sleep(1000);\n")
		self.file.write("					element(by.id(pswrd_id)).sendKeys(password);\n")
		self.file.write("					browser.sleep(2000);\n")
		self.file.write("					element(by.css(startLogin_css)).click();\n")
		self.file.write("					browser.sleep(2000);\n")
		self.file.write("				}\n")
		self.file.write("			});\n")
		self.file.write("			getVersion();\n")
		self.file.write("		};\n\n")

		self.file.write("		return global.browser.getProcessedConfig().then(function(config) { });\n")
		self.file.write("	},\n\n")

	"""On Complete block: format html results from xml output with screenshots, cleans up global variables"""
	def write_onComplete(self):
		self.file.write("	onComplete: function() {\n")
		self.file.write("		console.log('Tests Completed on '+browser.params.version+'\\n');\n")
		self.file.write("		console.log('Iteration Complete');\n\n")
		if self.capability != 'chrome':
			self.file.write("		var browserName, browserVersion;\n")
			self.file.write("		browser.getCapabilities().then(function(caps) {\n")
			self.file.write("			browserName = caps.get('browserName');\n")
			self.file.write("			browserVersion = caps.get('version');\n")
			self.file.write("			platform = caps.get('platform');\n")
			self.file.write("			var HTMLReport = require('protractor-html-reporter-2');\n")
			self.file.write("			testConfig = {\n")
			self.file.write("				reportTitle: 'Protractor Test Execution Report',\n")
			self.file.write("				outputPath: './"+self.browser+"-html-report/',\n")
			self.file.write("				outputFilename: 'index',\n")
			self.file.write("				screenshotPath: './screenshots',\n")
			self.file.write("				testBrowser: browserName,\n")
			self.file.write("				browserVersion: browserVersion,\n")
			self.file.write("				modifiedSuiteName: false,\n")
			self.file.write("				screenshotsOnlyOnFailure: true,\n")
			self.file.write("				testPlatform: platform\n")
			self.file.write("			};\n")
			self.file.write("			new HTMLReport().from('xmlresults.xml', testConfig);\n")
			self.file.write("		});\n\n")

		self.file.write("		global.getURL = null;\n")
		self.file.write("		global.loginUser = null;\n")
		self.file.write("		global.ignoreSynchronization = null;\n")
		self.file.write("		global.presentElement = null;\n")
		self.file.write("		global.presentElementClick = null;\n")
		self.file.write("		global.presentElementText = null;\n")
		self.file.write("		global.scrollToView = null;\n")
		self.file.write("		global.scrollAndClick = null;\n")
		self.file.write("		global.selectSideMenu = null;\n")
		self.file.write("		global.clearElem = null;\n")
		self.file.write("		global.stringContains = null;\n")
		self.file.write("		global.findDetail = null;\n\n")
		self.file.write("		var keys = Object.keys(globals.variables);\n")
		self.file.write("		for(var i = 0; i < keys.length; i++) {\n")
		self.file.write("			global[keys[i]] = null;\n")
		self.file.write("		}\n")
		self.file.write("		globals = null;\n")
		self.file.write("		keys = null;\n\n")
		self.file.write("		console.log('Cleanup Complete\\n');\n")
		self.file.write("	}\n")

	"""Writes the entire file, with unique values"""
	def write_to_file(self):
		self.file.write("var globals = require('./globals.js');\n")
		self.file.write("exports.config = {\n")
		self.write_selenium()
		self.write_suites()
		self.write_options()
		self.write_capability()
		self.write_onPrepare()
		self.write_onComplete()
		self.file.write("};")

"""Main function that does all the work, if all the information is available"""
def dynamic_conf():
	if len(sys.argv) == 6:
		new_conf = conf(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
		print "\nBrowser: "+new_conf.browser+"\n"

dynamic_conf()